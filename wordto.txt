test=灵感成真

topOne=国家级
topTwo=世界级
topThree=最高级
topFour=顶级
topFive=顶尖
topSix=最高档
topSeven=世界领先
topEight=国家示范
topNine=全国领先
topTen=行业顶尖
topEleven=行业领先
topTwelve=领衔
topThirteen=问鼎
topFourteen=开创之举


firstOne=第一
firstTwo=唯一
firstThree=首个
firstFour=推荐首选
firstFive=首家
firstSix=独家
firstSeven=第一人
firstEight=首席
firstNine=独一无二
firstTen=绝无仅有
firstEleven=前无古人
firstTwelve=100%
firstThirteen=百分百
firstFourteen=史上
firstFifteen=非常一流

goldFirst=金牌
goldTwo=名牌
goldThree=王牌
goldFour=销量冠军
goldFive=第一 (重複)
goldSix=NO.1
goldSeven=Top1
goldEight=领袖品牌
goldNine=领导品牌
goldTen=顶级 (重複)
goldEleven=顶尖 (重複)
goldTwelve=泰斗
goldThirteen=金标
goldFourteen=驰名尖端

mostFirst=最
mostTwo=终极
mostThree=极致
mostFour=万能
mostFive=决对
mostSix=彻底
mostSeven=完全
mostEight=极佳
mostNine=全解决
mostTen=全方位
mostEleven=全面改善
mostTwelve=精确
mostThirteen=准确
mostFourteen=精准
mostFifteen=优秀
mostSixteen=完美
mostSeventeen=永久


authorityOne=老字号
authorityTwo=中国驰名商标
authorityThree=特供
authorityFour=专供
authorityFive=专家推荐
authoritySix=国家免检
authoritySeven=质量免检
authorityEight=无需国家质量检测
authorityNine=免抽检


prizeOne=点击领奖
prizeTwo=恭喜获奖
prizeThree=全民免单
prizeFour=点击有惊喜
prizeFive=点击获取
prizeSix=点击转身
prizeSeven=点击试穿
prizeEight=点击翻身
prizeNine=领奖品
prizeTen=秒杀
prizeEleven=抢爆
prizeTwelve=在不抢就没啦
prizeThirteen=不会再便宜
prizeFourteen=万人疯抢
prizeFifteen=错过就没机会
prizeSixteen=全民疯抢
prizeSeventeen=全民抢购
prizeEighteen=卖疯了
prizeNineteen=抢疯了
