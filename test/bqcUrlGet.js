const puppeteer = require('puppeteer');
const request = require("request-promise");
const cheerio = require("cheerio");
const expect = require('chai').expect;

const cicGA="?utm_source=autotest&utm_medium=CIC"

const bqcUrlAll=[]
const topOnePass=[]
const topOneFail=[]

const topTwoPass=[]
const topTwoFail=[]

const topThreePass=[]
const topThreeFail=[]

const topFourPass=[]
const topFourFail=[]

const topFivePass=[]
const topFiveFail=[]

const topSixPass=[]
const topSixFail=[]

const topSevenPass=[]
const topSevenFail=[]

const topEightPass=[]
const topEightFail=[]

const topNinePass=[]
const topNineFail=[]

const topTenPass=[]
const topTenFail=[]

const topElevenPass=[]
const topElevenFail=[]

const topTwelvePass=[]
const topTwelveFail=[]

const topThirteenPass=[]
const topThirteenFail=[]

const topFourteenPass=[]
const topFourteenFail=[]

const topFifteenPass=[]
const topFifteenFail=[]

const firstOnePass=[]
const firstOneFail=[]

const firstTwoPass=[]
const firstTwoFail=[]

const firstThreePass=[]
const firstThreeFail=[]

const firstFourPass=[]
const firstFourFail=[]

const firstFivePass=[]
const firstFiveFail=[]

const firstSixPass=[]
const firstSixFail=[]

const firstSevenPass=[]
const firstSevenFail=[]

const firstEightPass=[]
const firstEightFail=[]

const firstNinePass=[]
const firstNineFail=[]

const firstTenPass=[]
const firstTenFail=[]

const firstElevenPass=[]
const firstElevenFail=[]

const firstTwelvePass=[]
const firstTwelveFail=[]

const firstThirteenPass=[]
const firstThirteenFail=[]

const firstFourteenPass=[]
const firstFourteenFail=[]

const firstFifteenPass=[]
const firstFifteenFail=[]

const goldFirstPass=[]
const goldFirstFail=[]

const goldTwoPass=[]
const goldTwoFail=[]

const goldThreePass=[]
const goldThreeFail=[]

const goldFourPass=[]
const goldFourFail=[]

const goldFivePass=[]
const goldFiveFail=[]

const goldSixPass=[]
const goldSixFail=[]

const goldSevenPass=[]
const goldSevenFail=[]

const goldEightPass=[]
const goldEightFail=[]

const goldNinePass=[]
const goldNineFail=[]

const goldTenPass=[]
const goldTenFail=[]

const goldElevenPass=[]
const goldElevenFail=[]

const goldTwelvePass=[]
const goldTwelveFail=[]

const goldThirteenPass=[]
const goldThirteenFail=[]

const goldFourteenPass=[]
const goldFourteenFail=[]

const goldFifteenPass=[]
const goldFifteenFail=[]

const mostFirstPass=[]
const mostFirstFail=[]

const mostTwoPass=[]
const mostTwoFail=[]

const mostThreePass=[]
const mostThreeFail=[]

const mostFourPass=[]
const mostFourFail=[]

const mostFivePass=[]
const mostFiveFail=[]

const mostSixPass=[]
const mostSixFail=[]

const mostSevenPass=[]
const mostSevenFail=[]

const mostEightPass=[]
const mostEightFail=[]

const mostNinePass=[]
const mostNineFail=[]

const mostTenPass=[]
const mostTenFail=[]

const mostElevenPass=[]
const mostElevenFail=[]

const mostTwelvePass=[]
const mostTwelveFail=[]

const mostThirteenPass=[]
const mostThirteenFail=[]

const mostFourteenPass=[]
const mostFourteenFail=[]

const mostFifteenPass=[]
const mostFifteenFail=[]

const mostSixteenPass=[]
const mostSixteenFail=[]

const mostSeventeenPass=[]
const mostSeventeenFail=[]

const authorityOnePass=[]
const authorityOneFail=[]

const authorityTwoPass=[]
const authorityTwoFail=[]

const authorityThreePass=[]
const authorityThreeFail=[]

const authorityFourPass=[]
const authorityFourFail=[]

const authorityFivePass=[]
const authorityFiveFail=[]

const authoritySixPass=[]
const authoritySixFail=[]

const authoritySevenPass=[]
const authoritySevenFail=[]

const authorityEightPass=[]
const authorityEightFail=[]

const authorityNinePass=[]
const authorityNineFail=[]

const prizeOnePass=[]
const prizeOneFail=[]

const prizeTwoPass=[]
const prizeTwoFail=[]

const prizeThreePass=[]
const prizeThreeFail=[]

const prizeFourPass=[]
const prizeFourFail=[]

const prizeFivePass=[]
const prizeFiveFail=[]

const prizeSixPass=[]
const prizeSixFail=[]

const prizeSevenPass=[]
const prizeSevenFail=[]

const prizeEightPass=[]
const prizeEightFail=[]

const prizeNinePass=[]
const prizeNineFail=[]

const prizeTenPass=[]
const prizeTenFail=[]

const prizeElevenPass=[]
const prizeElevenFail=[]

const prizeTwelvePass=[]
const prizeTwelveFail=[]

const prizeThirteenPass=[]
const prizeThirteenFail=[]

const prizeFourteenPass=[]
const prizeFourteenFail=[]

const prizeFifteenPass=[]
const prizeFifteenFail=[]

const prizeSixteenPass=[]
const prizeSixteenFail=[]

const prizeSeventeenPass=[]
const prizeSeventeenFail=[]

const prizeEighteenPass=[]
const prizeEighteenFail=[]

const prizeNineteenPass=[]
const prizeNineteenFail=[]

const bqcUrl = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://g5-prod.benq.com.cn/zh-cn/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            let url = $(element).text()
            // if(url.indexOf("specifications")!==-1 && url.indexOf("wireless-full-hd-kit-wdp01")==-1){
            //     eneuSpecUrlAll.push(url)
            // }
            if(url.indexOf("https://")!==-1){
                bqcUrlAll.push(url)
            }
    })
    return bqcUrlAll;
    } catch (error) {
      console.log(error);
    }
};


describe('BQC URL',async()=>{
    let browser
    let page
    //Test Hooks:before, beforeEach, after, afterEach
    //before:每個test case執行之前先做的動作(page/browswer)
    before(async function(){
        browser=await puppeteer.launch({
            executablePath:
            "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
            headless:true,//無介面模式:有無需要開視窗,false要開,true不開
            slowMo:100,// slow down by 100ms
            devtools:false//有無需要開啟開發人員工具
        })
        page=await browser.newPage()
        //設定像素
        await page.setViewport({width:1200,height:1000})

        await page.setDefaultTimeout(20000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
        await page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
    })
    
    //after:每個test case執行之後統一要做的動作(page/browswer)
    after(async function(){
        await browser.close()
    })
    it('BQC Get All Spec URL',async function(){
        const returnedData = await bqcUrl();
        // console.log(returnedData)
        console.log('Total of BQC URL :',returnedData.length)
        console.log("type of Data:",typeof returnedData)
        // console.log("bqcUrlAll",bqcUrlAll)
        const getURL=[]
        for(let i=1600; i<1700; i++){
            const bqcUrlCheck = returnedData[i]
            getURL.push(bqcUrlCheck)
        }
        console.log("Get URL:",getURL)
    })
//   npm run test:url

        // await page.goto("https://www.benq.eu/en-ie/index.html"+cicGA)
        // await page.waitForSelector('#btn_close')
        // await page.click('#btn_close')
    //     for(let i =0;i<2;i++){
    //         //returnedData.length所有URL數量
    //         const bqcUrlCheck = returnedData[i]

    //         const result = await request.get(bqcUrlCheck)
    //         const $ = cheerio.load(result)
    //         const innerHtml = await page.$eval('body', element => element.innerHTML);
    //         if(innerHtml.indexOf("国家级")<0){
    //             topOnePass.push(bqcUrlCheck)
    //             //console.log(`${sbqUrlCheck} is pass`)
    //         }else{
    //             console.log(`Fail URL: ${bqcUrlCheck}`)
    //             topOneFail.push(bqcUrlCheck)
    //         }
    //         if(innerHtml.indexOf("世界级")<0){
    //             topTwoPass.push(bqcUrlCheck)
    //             //console.log(`${sbqUrlCheck} is pass`)
    //         }else{
    //             console.log(`Fail URL: ${bqcUrlCheck}`)
    //             topTwoFail.push(bqcUrlCheck)
    //         }
    //         if(innerHtml.indexOf("最高级")<0){
    //             topThreePass.push(bqcUrlCheck)
    //             //console.log(`${sbqUrlCheck} is pass`)
    //         }else{
    //             console.log(`Fail URL: ${bqcUrlCheck}`)
    //             topThreeFail.push(bqcUrlCheck)
    //         }
    //         if(innerHtml.indexOf("顶级")<0){
    //             topFourPass.push(bqcUrlCheck)
    //             //console.log(`${sbqUrlCheck} is pass`)
    //         }else{
    //             console.log(`Fail URL: ${bqcUrlCheck}`)
    //             topFourFail.push(bqcUrlCheck)
    //         }
    //         if(innerHtml.indexOf("顶尖")<0){
    //             topFivePass.push(bqcUrlCheck)
    //             //console.log(`${sbqUrlCheck} is pass`)
    //         }else{
    //             console.log(`Fail URL: ${bqcUrlCheck}`)
    //             topFiveFail.push(bqcUrlCheck)
    //         }
    //         if(innerHtml.indexOf("最高级")<0){
    //             topSixPass.push(bqcUrlCheck)
    //             //console.log(`${sbqUrlCheck} is pass`)
    //         }else{
    //             console.log(`Fail URL: ${bqcUrlCheck}`)
    //             topSixFail.push(bqcUrlCheck)
    //         }
    //         if(innerHtml.indexOf("最高档")<0){
    //             topSevenPass.push(bqcUrlCheck)
    //             //console.log(`${sbqUrlCheck} is pass`)
    //         }else{
    //             console.log(`Fail URL: ${bqcUrlCheck}`)
    //             topSevenFail.push(bqcUrlCheck)
    //         }
    //         if(innerHtml.indexOf("世界领先")<0){
    //             topEightPass.push(bqcUrlCheck)
    //             //console.log(`${sbqUrlCheck} is pass`)
    //         }else{
    //             console.log(`Fail URL: ${bqcUrlCheck}`)
    //             topEightFail.push(bqcUrlCheck)
    //         }
    //         if(innerHtml.indexOf("国家示范")<0){
    //             topNinePass.push(bqcUrlCheck)
    //             //console.log(`${sbqUrlCheck} is pass`)
    //         }else{
    //             console.log(`Fail URL: ${bqcUrlCheck}`)
    //             topNineFail.push(bqcUrlCheck)
    //         }
    //         if(innerHtml.indexOf("全国领先")<0){
    //             topTenPass.push(bqcUrlCheck)
    //             //console.log(`${sbqUrlCheck} is pass`)
    //         }else{
    //             console.log(`Fail URL: ${bqcUrlCheck}`)
    //             topTenFail.push(bqcUrlCheck)
    //         }
    //         if(innerHtml.indexOf("行业顶尖")<0){
    //             topElevenPass.push(bqcUrlCheck)
    //             //console.log(`${sbqUrlCheck} is pass`)
    //         }else{
    //             console.log(`Fail URL: ${bqcUrlCheck}`)
    //             topElevenFail.push(bqcUrlCheck)
    //         }
    //         if(innerHtml.indexOf("行业领先")<0){
    //             topTwelvePass.push(bqcUrlCheck)
    //             //console.log(`${sbqUrlCheck} is pass`)
    //         }else{
    //             console.log(`Fail URL: ${bqcUrlCheck}`)
    //             topTwelveFail.push(bqcUrlCheck)
    //         }
    //         if(innerHtml.indexOf("领衔")<0){
    //             topThirteenPass.push(bqcUrlCheck)
    //             //console.log(`${sbqUrlCheck} is pass`)
    //         }else{
    //             console.log(`Fail URL: ${bqcUrlCheck}`)
    //             topThirteenFail.push(bqcUrlCheck)
    //         }
    //         if(innerHtml.indexOf("问鼎")<0){
    //             topFourteenPass.push(bqcUrlCheck)
    //             //console.log(`${sbqUrlCheck} is pass`)
    //         }else{
    //             console.log(`Fail URL: ${bqcUrlCheck}`)
    //             topFourteenFail.push(bqcUrlCheck)
    //         }
    //         if(innerHtml.indexOf("开创之举")<0){
    //             topFifteenPass.push(bqcUrlCheck)
    //             //console.log(`${sbqUrlCheck} is pass`)
    //         }else{
    //             console.log(`Fail URL: ${bqcUrlCheck}`)
    //             topFifteenFail.push(bqcUrlCheck)
    //         }
    //     }
    //     // console.log("Total of BQC Pass URL:",bqcUrlPass.length)
    //     // console.log("BQC Pass URL:",bqcUrlPass)
    //     // console.log("Total of BQC Fail Url:",bqcUrlFail.length)
    //     // console.log("BQC Fail Url:",bqcUrlFail)
    //     // expect(bqcUrlFail.length).to.equal(0)
    // })
    // it('国家级',async function(){
    //     console.log("Total of BQC Pass URL-test:",topOnePass.length)
    //     console.log("BQC Pass URL-test:",topOnePass)
    //     console.log("Total of BQC Fail Url-test:",topOneFail.length)
    //     console.log("BQC Fail Url-test:",topOneFail)
    //     expect(topOneFail.length).to.equal(0)
    // })
    // it('世界级',async function(){
    //     console.log("Total of BQC Pass URL-test:",topTwoPass.length)
    //     console.log("BQC Pass URL-test:",topTwoPass)
    //     console.log("Total of BQC Fail Url-test:",topTwoFail.length)
    //     console.log("BQC Fail Url-test:",topTwoFail)
    //     expect(topTwoFail.length).to.equal(0)
    // })
    // it('最高级',async function(){
    //     console.log("Total of BQC Pass URL-test:",topThreePass.length)
    //     console.log("BQC Pass URL-test:",topThreePass)
    //     console.log("Total of BQC Fail Url-test:",topThreeFail.length)
    //     console.log("BQC Fail Url-test:",topThreeFail)
    //     expect(topThreeFail.length).to.equal(0)
    // })
    // it('顶级',async function(){
    //     console.log("Total of BQC Pass URL-test:",topFourPass.length)
    //     console.log("BQC Pass URL-test:",topFourPass)
    //     console.log("Total of BQC Fail Url-test:",topFourFail.length)
    //     console.log("BQC Fail Url-test:",topFourFail)
    //     expect(topFourFail.length).to.equal(0)
    // })
    // it('顶尖',async function(){
    //     console.log("Total of BQC Pass URL-test:",topFivePass.length)
    //     console.log("BQC Pass URL-test:",topFivePass)
    //     console.log("Total of BQC Fail Url-test:",topFiveFail.length)
    //     console.log("BQC Fail Url-test:",topFiveFail)
    //     expect(topFiveFail.length).to.equal(0)
    // })
    // it('最高级',async function(){
    //     console.log("Total of BQC Pass URL-test:",topSixPass.length)
    //     console.log("BQC Pass URL-test:",topSixPass)
    //     console.log("Total of BQC Fail Url-test:",topSixFail.length)
    //     console.log("BQC Fail Url-test:",topSixFail)
    //     expect(topSixFail.length).to.equal(0)
    // })
    // it('最高档',async function(){
    //     console.log("Total of BQC Pass URL-test:",topSevenPass.length)
    //     console.log("BQC Pass URL-test:",topSevenPass)
    //     console.log("Total of BQC Fail Url-test:",topSevenFail.length)
    //     console.log("BQC Fail Url-test:",topSevenFail)
    //     expect(topSevenFail.length).to.equal(0)
    // })
    // it('世界领先',async function(){
    //     console.log("Total of BQC Pass URL-test:",topEightPass.length)
    //     console.log("BQC Pass URL-test:",topEightPass)
    //     console.log("Total of BQC Fail Url-test:",topEightFail.length)
    //     console.log("BQC Fail Url-test:",topEightFail)
    //     expect(topEightFail.length).to.equal(0)
    // })
    // it('国家示范',async function(){
    //     console.log("Total of BQC Pass URL-test:",topNinePass.length)
    //     console.log("BQC Pass URL-test:",topNinePass)
    //     console.log("Total of BQC Fail Url-test:",topNineFail.length)
    //     console.log("BQC Fail Url-test:",topNineFail)
    //     expect(topNineFail.length).to.equal(0)
    // })
    // it('全国领先',async function(){
    //     console.log("Total of BQC Pass URL-test:",topTenPass.length)
    //     console.log("BQC Pass URL-test:",topTenPass)
    //     console.log("Total of BQC Fail Url-test:",topTenFail.length)
    //     console.log("BQC Fail Url-test:",topTenFail)
    //     expect(topTenFail.length).to.equal(0)
    // })
    // it('行业顶尖',async function(){
    //     console.log("Total of BQC Pass URL-test:",topElevenPass.length)
    //     console.log("BQC Pass URL-test:",topElevenPass)
    //     console.log("Total of BQC Fail Url-test:",topElevenFail.length)
    //     console.log("BQC Fail Url-test:",topElevenFail)
    //     expect(topElevenFail.length).to.equal(0)
    // })
    // it('行业领先',async function(){
    //     console.log("Total of BQC Pass URL-test:",topTwelvePass.length)
    //     console.log("BQC Pass URL-test:",topTwelvePass)
    //     console.log("Total of BQC Fail Url-test:",topTwelveFail.length)
    //     console.log("BQC Fail Url-test:",topTwelveFail)
    //     expect(topTwelveFail.length).to.equal(0)
    // })
    // it('领衔',async function(){
    //     console.log("Total of BQC Pass URL-test:",topThirteenPass.length)
    //     console.log("BQC Pass URL-test:",topThirteenPass)
    //     console.log("Total of BQC Fail Url-test:",topThirteenFail.length)
    //     console.log("BQC Fail Url-test:",topThirteenFail)
    //     expect(topThirteenFail.length).to.equal(0)
    // })
    // it('问鼎',async function(){
    //     console.log("Total of BQC Pass URL-test:",topFourteenPass.length)
    //     console.log("BQC Pass URL-test:",topFourteenPass)
    //     console.log("Total of BQC Fail Url-test:",topFourteenFail.length)
    //     console.log("BQC Fail Url-test:",topFourteenFail)
    //     expect(topFourteenFail.length).to.equal(0)
    // })
    // it('开创之举',async function(){
    //     console.log("Total of BQC Pass URL-test:",topFifteenPass.length)
    //     console.log("BQC Pass URL-test:",topFifteenPass)
    //     console.log("Total of BQC Fail Url-test:",topFifteenFail.length)
    //     console.log("BQC Fail Url-test:",topFifteenFail)
    //     expect(topFifteenFail.length).to.equal(0)
    // })
})



//   npm run test:url
