Feature:BQC Word Check
# 一、极限词的替换与极限用语禁用整理
    # 【国家级、世界级、最高级、顶级】
    Scenario: Check
        Given check all URL
    # Scenario: 灵感成真(測試用)
    #     Then '灵感成真'不可出現在網頁中
    Scenario: 国家级
        Then '国家级'不可出現在網頁中
    Scenario: 世界级
        Then '世界级'不可出現在網頁中
    Scenario: 最高级
        Then '最高级'不可出現在網頁中
    Scenario: 顶级
        Then '顶级'不可出現在網頁中
    Scenario: 顶尖
        Then '顶尖'不可出現在網頁中
    # Scenario: 最高级
    #     Then '最高级'不可出現在網頁中
    Scenario: 最高档
        Then '最高档'不可出現在網頁中
    Scenario: 世界领先
        Then '世界领先'不可出現在網頁中
    Scenario: 国家示范
        Then '国家示范'不可出現在網頁中
    Scenario: 全国领先
        Then '全国领先'不可出現在網頁中
    Scenario: 行业顶尖
        Then '行业顶尖'不可出現在網頁中
    Scenario: 行业领先
        Then '行业领先'不可出現在網頁中
    Scenario: 领衔
        Then '领衔'不可出現在網頁中
    Scenario: 问鼎
        Then '问鼎'不可出現在網頁中
    Scenario: 开创之举
        Then '开创之举'不可出現在網頁中
    # 【第一、唯一、首个】
    Scenario: 第一
        Then '第一'不可出現在網頁中
    Scenario: 唯一
        Then '唯一'不可出現在網頁中
    Scenario: 首个
        Then '首个'不可出現在網頁中
    Scenario: 推荐首选
        Then '推荐首选'不可出現在網頁中
    Scenario: 首家
        Then '首家'不可出現在網頁中
    Scenario: 独家
        Then '独家'不可出現在網頁中
    Scenario: 第一人
        Then '第一人'不可出現在網頁中
    Scenario: 首席
        Then '首席'不可出現在網頁中
    Scenario: 独一无二
        Then '独一无二'不可出現在網頁中
    Scenario: 绝无仅有
        Then '绝无仅有'不可出現在網頁中
    Scenario: 前无古人
        Then '前无古人'不可出現在網頁中
    Scenario: 100%
        Then '100%'不可出現在網頁中
    Scenario: 百分百
        Then '百分百'不可出現在網頁中
    Scenario: 史上
        Then '史上'不可出現在網頁中
    Scenario: 非常一流
        Then '非常一流'不可出現在網頁中
#     # 【金牌、名牌】
    Scenario: 金牌
        Then '金牌'不可出現在網頁中
    Scenario: 名牌
        Then '名牌'不可出現在網頁中
    Scenario: 王牌
        Then '王牌'不可出現在網頁中
    Scenario: 销量冠军
        Then '销量冠军'不可出現在網頁中
    # Scenario: 第一
    #     Then '第一'不可出現在網頁中
    Scenario: NO.1
        Then 'NO.1'不可出現在網頁中
    Scenario: Top1
        Then 'Top1'不可出現在網頁中
    Scenario: 领袖品牌
        Then '领袖品牌'不可出現在網頁中
    Scenario: 领导品牌
        Then '领导品牌'不可出現在網頁中
    # Scenario: 顶级
    #     Then '顶级'不可出現在網頁中
    # Scenario: 顶尖
    #     Then '顶尖'不可出現在網頁中
    Scenario: 泰斗
        Then '泰斗'不可出現在網頁中
    Scenario: 金标
        Then '金标'不可出現在網頁中
    Scenario: 金牌
        Then '金牌'不可出現在網頁中
    Scenario: 驰名尖端
        Then '驰名尖端'不可出現在網頁中
#     # 【最，全、精、永久】
    Scenario: 最
        Then '最'不可出現在網頁中
    Scenario: 终极
        Then '终极'不可出現在網頁中
    Scenario: 极致
        Then '极致'不可出現在網頁中
    Scenario: 万能
        Then '万能'不可出現在網頁中
    Scenario: 决对
        Then '决对'不可出現在網頁中
    Scenario: 彻底
        Then '彻底'不可出現在網頁中
    Scenario: 完全
        Then '完全'不可出現在網頁中
    Scenario: 极佳
        Then '极佳'不可出現在網頁中
    Scenario: 全解决
        Then '全解决'不可出現在網頁中
    Scenario: 全方位
        Then '全方位'不可出現在網頁中
    Scenario: 全面改善
        Then '全面改善'不可出現在網頁中
    Scenario: 精确
        Then '精确'不可出現在網頁中
    Scenario: 准确
        Then '准确'不可出現在網頁中
    Scenario: 精准
        Then '精准'不可出現在網頁中
    Scenario: 优秀
        Then '优秀'不可出現在網頁中
    Scenario: 完美
        Then '完美'不可出現在網頁中
    Scenario: 永久
        Then '永久'不可出現在網頁中
# 二、高压线，这些淘宝禁用语别出现
#    【权威】
    Scenario: 老字号
        Then '老字号'不可出現在網頁中
    Scenario: 中国驰名商标
        Then '中国驰名商标'不可出現在網頁中
    Scenario: 特供
        Then '特供'不可出現在網頁中
    Scenario: 专供
        Then '专供'不可出現在網頁中
    Scenario: 专家推荐
        Then '专家推荐'不可出現在網頁中
    Scenario: 国家免检
        Then '国家免检'不可出現在網頁中
    Scenario: 质量免检
        Then '质量免检'不可出現在網頁中
    Scenario: 无需国家质量检测
        Then '无需国家质量检测'不可出現在網頁中
    Scenario: 免抽检
        Then '免抽检'不可出現在網頁中
    # 【中奖】
    Scenario: 点击领奖
        Then '点击领奖'不可出現在網頁中
    Scenario: 恭喜获奖
        Then '恭喜获奖'不可出現在網頁中
    Scenario: 全民免单
        Then '全民免单'不可出現在網頁中
    Scenario: 点击有惊喜
        Then '点击有惊喜'不可出現在網頁中
    Scenario: 点击获取
        Then '点击获取'不可出現在網頁中
    Scenario: 点击转身
        Then '点击转身'不可出現在網頁中
    Scenario: 点击试穿
        Then '点击试穿'不可出現在網頁中
    Scenario: 点击翻身
        Then '点击翻身'不可出現在網頁中
    Scenario: 领奖品
        Then '领奖品'不可出現在網頁中
    Scenario: 秒杀
        Then '秒杀'不可出現在網頁中
    Scenario: 抢爆
        Then '抢爆'不可出現在網頁中
    Scenario: 在不抢就没啦
        Then '在不抢就没啦'不可出現在網頁中
    Scenario: 不会再便宜
        Then '不会再便宜'不可出現在網頁中
    Scenario: 万人疯抢
        Then '万人疯抢'不可出現在網頁中
    Scenario: 错过就没机会
        Then '错过就没机会'不可出現在網頁中
    Scenario: 全民疯抢
        Then '全民疯抢'不可出現在網頁中
    Scenario: 全民抢购
        Then '全民抢购'不可出現在網頁中
    Scenario: 卖疯了
        Then '卖疯了'不可出現在網頁中
    Scenario: 抢疯了
        Then '抢疯了'不可出現在網頁中