const { Given, When, Then, Before, After } = require('@cucumber/cucumber')
const { expect } = require('chai')
const fs = require('fs');
const puppeteer = require('puppeteer')
const request = require("request-promise");
const cheerio = require("cheerio");
const cicGA="?utm_source=autotest&utm_medium=CIC"

const bqcUrlAll = 
[ 
    'https://g5-prod.benq.com.cn/zh-cn/monitor/gaming/ex3210u.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/gaming/ex2710q.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/gaming/ex3415r.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/gaming/ex2710s.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/designer/pd3220u.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/business.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/entertainment/ew3880r-38-inch.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/entertainment/ew2780u.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/entertainment/ew27801.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/entertainment/ew2480-24-inch1.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/entertainment/ew27801/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/designer/pd2725u.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/designer/pd3220u/question.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/gaming/ex2710s/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/gaming/ex2510s-25-inch/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/designer/pd2725u/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/gaming/ex2710q/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/gaming/ex3415r/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/gaming/ex3210u/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/entertainment/ew2880u.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/entertainment.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/entertainment/ew2780q/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/entertainment/ew2780q/question1.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/entertainment/ew2780u/question1.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/entertainment/ew2480-24-inch1/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/entertainment/ew3270u.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/entertainment/ew3280u.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/entertainment/ew2780u/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/business/bl2480t.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/business/bl2480t/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/designer/pd2500q.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/designer/pd2700q.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/designer/pd3205u/question.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/business.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/screenbar-lamp/wit-screenbar.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/entertainment/ew3280u/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/screenbar-lamp/wit-screenbar-plus.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/screenbar-lamp/screenbar-halo.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/designer/pd3205u.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/designer/pd2705u.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/screenbar-lamp/wit-screenbar-lite.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/entertainment/ew3270u/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/designer/pd2705q/question.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/entertainment/ew2880u/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china/anhui.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/designer/pd2705u/question.html',
    'https://g5-prod.benq.com.cn/zh-cn/knowledge-center/knowledge/one-cable-to-daisy-chain-all-the-monitors.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/designer/pd2705u/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/entertainment/ew3880r-38-inch/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/designer/pd2700u.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/designer/pd3200q.html',
    'https://g5-prod.benq.com.cn/zh-cn/knowledge-center/knowledge/a-monitor-that-matches-the-colors-of-your-mac.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/designer/pd3200u.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/aqcolor-expert/designer.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/designer/pd2705q.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/designer-expert/home/bert-monroy.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/laser-tv-projector/i985l.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/designer-expert/home/nemanja-sekulic.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/interactive-classroom.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/designer-expert/home/sun-chun-xing.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/designer-expert/home/mike-hermes.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/business/ed18a/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/designer-expert/home/robert-hranitzky.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/designer-expert/home/martin-pertiniak.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/laser-tv-projector/i980l.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/designer.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/stylish/gw2480t.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/cineprime-home-cinema/w2700.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/designer-expert/home/martin-benes.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/inatallation.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/interactive-classroom/lx82ustd/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/interactive-classroom/dx809st/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/interactive-classroom/lh890ustd/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/gaming-projector/th685.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/smart-projector-e-series/e520/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/interactive-classroom/dx861ust/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/designer-expert/home/hugo-guerra.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/smart-projector-e-series.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/home-series/i750.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/i750.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/gaming-projector/tk800m.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/portable/gs2/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shanghai/xuhui.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/portable/gv1/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/home-series/gk100/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/interactive-classroom/lx833std/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/inatallation/lu960ust/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/inatallation/lu935st/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/inatallation/lk953st/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/inatallation/lu9255/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/inatallation/lu9245/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/business/ed18b/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/business/ed18d/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/business/mh560/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/business/eh7941/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/business/mx560/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/business/au716n/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/business/th671st1/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/gaming-projector/tk700.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/laser-tv-projector/i985l/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/laser-tv-projector/i967l.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/cineprime-home-cinema/w1800.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/laser-tv-projector/i980l/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/cineprime-home-cinema/w1130.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/laser-tv-projector.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/laser-tv-projector/i962l.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/entertainment/ew2880u/question.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/entertainment/ew3280u/question.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/stylish/gw2480t/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/gaming-projector/tk850/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/cineprime-home-cinema/w5700/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/cineprime-home-cinema/w5700.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/gaming-projector/rk9000/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/gaming-projector/tk850.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/stylish.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/smart-projector-e-series/e320/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/smart-projector-e-series/e592/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/smart-projector-e-series/e590/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/smart-projector-e-series/e582/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/smart-projector-e-series/e580/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/smart-projector-e-series/e540/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/smart-projector-e-series/e530/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/cineprime-home-cinema/w1130/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/cineprime-home-cinema/w1800/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/aqcolor.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/photographer/sw271c.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/business/th671st1.html',
    'https://g5-prod.benq.com.cn/zh-cn/search-result.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/photographer/sw321c.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/photographer/sw240/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/tk800m/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/tk800m/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/photographer.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/th671st/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/photographer/sw270c.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/tk810/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/mindduo-2-plus/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/mindduo/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/photographer/sw270c/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/photographer/sh240/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/photographer/sw271c/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/photographer/sw321c/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/photographer/sh240.html',
    'https://g5-prod.benq.com.cn/zh-cn/index.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/sh240.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/sw270c.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/sw240.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/sw271c.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/sw2700pt.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/sw321c.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/wit-screenbar-plus.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/pianolight.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/wit-genie.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/floorlamp.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/wit-intelligent.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/mindduo-2-plus.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/wit-mindduo-s.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/mindduo.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/portable.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/designer-expert/home/adam-softley.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/gaming-projector/tk700st.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/lk953st/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/gaming-projector/th585.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/i980l/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/gaming-projector/th685/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/screenbar-lamp.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/home-series.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/business/au716n.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/dx861ust1/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/dx861ust1/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/accessory/wdc10c.html',
    'https://g5-prod.benq.com.cn/zh-cn/knowledge-center/knowledge/what-makes-a-professional-monitor-mac-ready.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/dx809st1/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/dx809st1/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/business/ed18d.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china.html',
    'https://g5-prod.benq.com.cn/zh-cn/knowledge-center.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/accessory/wdc10/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hebei/shijiazhuang.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/smart-projector-e-series/e520.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/gaming-projector/rk9000.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/cineprime-home-cinema/w2700/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/w1800/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/w1800/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/accessory/wdc10.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/interactive-classroom/dx861ust.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/lu935st1/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/interactive-classroom/lx82ustd.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/interactive-classroom/dx809st.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/interactive-classroom/lx833std.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/lu935st1/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/interactive-classroom/lh890ustd.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/inatallation/lu960ust.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/inatallation/lu935st.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/inatallation/lk953st.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/inatallation/lu9255.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/inatallation/lu9245.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/cineprime-home-cinema.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/business/ed18a.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/business/ed18b.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/business/mh560.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/business/eh7941.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/business/mx560.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/smart-projector-e-series/e592.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/smart-projector-e-series/e590.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/smart-projector-e-series/e582.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/smart-projector-e-series/e580.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/smart-projector-e-series/e540.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/smart-projector-e-series/e530.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/lu9255/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/lu9255/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/smart-projector-e-series/e320.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/home-series/i707.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/home-series/i707/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/home-series/i750/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/gaming-projector/tk800m/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/gaming-projector/tk700/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/gaming-projector/tk700st/question1.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/gaming-projector/tk700st/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/gaming-projector/th585/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/accessory/wit-floor-stand-extension.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/accessory/wit-desk-clamp.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/photographer/sw240.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/th671st/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ew2480-24-inch/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/mx5601/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/mx5601/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/w2700/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/designer/pd2700q/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/designer/pd2500q/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/tk7001/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/tk7001/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/lu960ust/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/lu960ust/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/lu960ust.html',
    'https://g5-prod.benq.com.cn/zh-cn/support.html',
    'https://g5-prod.benq.com.cn/zh-cn/about-benq/thebrand.html',
    'https://g5-prod.benq.com.cn/zh-cn/about-benq/csr.html',
    'https://g5-prod.benq.com.cn/zh-cn/about-benq/group.html',
    'https://g5-prod.benq.com.cn/zh-cn/speaker/electrostatic-bluetooth-speaker/trevolo-u/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/mindduo/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/mindduo-2-plus/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/parent-child-lamp/mindduo-2-plus.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/parent-child-lamp/mindduo-2-plus/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/parent-child-lamp/wit-mindduo-s.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/parent-child-lamp/wit-mindduo-s/qalist.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting.html',
    'https://g5-prod.benq.com.cn/zh-cn/speaker/electrostatic-bluetooth-speaker.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/accessory/wdc10c/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/lh890ustd/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/mw632st/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/mw632st/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/lu9245/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2700u.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/lu9245/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/dx861ust1.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/mx631st/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/mx631st/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/ms630st/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/ms630st/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/lk953st/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/wdc10/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/wdc10/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/MH560/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/gk100/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/MH560/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/gk100/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/gs2/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/gs2/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/gv1/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/gv1/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2700u/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2700u/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/W1130/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/W1130/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/tk700st/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/tk700st/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/th685/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/th685/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/th585/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/th585/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/i750/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/i750/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/i707/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/i707/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/e592/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/e592/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/e590/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd3220u/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/e590/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd3220u/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/e582/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/e582/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/e580/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd3200u/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2725u-2021new/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2725u-2021new/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd3200u/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/e540/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/e540/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd3200q/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/e530/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd3200q/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd3205u-32-inch/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd3205u-32-inch/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/e520/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/e520/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2705u/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2705u/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/e320/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/e320/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/sw2700pt/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/sw2700pt/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/pianolight/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/pianolight/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/i985l/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/screenbar-halo/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/sw320/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/screenbar-halo/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/i985l/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/sw320/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/sw271/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/sw271/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/i980l/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/desklamp.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/v7050i/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/wit-genie/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/wit-genie/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/v7050i/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/floorlamp/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/floorlamp/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/sw321c/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/sw321c/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/wit-intelligent/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/wit-intelligent/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/sw271c/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/sw271c/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/v7000i/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/sw270c/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/v7000i/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/wit-mindduo-s/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/wit-mindduo-s/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/sw270c/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/tk810/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/sh240/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/sh240/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/w5700/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/sw240/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/sw240/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/w5700/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ex3210u/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ex3210u/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/w2700/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2705q/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2705q/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2700q/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2700q/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ex3210r/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ex3210r/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2500q/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2500q/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ew3880r-38-inch/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ew3880r-38-inch/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/speaker/trevolo-u.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ex3415r/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ex3415r/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/bl2780t/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/bl2780t/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ew2780/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ew2780/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/gw2780/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/gw2780/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/parent-child-lamp.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ew2480-24-inch/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/parent-child-lamp/mindduo.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/bl2480t/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/bl2480t/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/gw2485tc/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/gw2485tc/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/gw2480t/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/gw2480t/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/gw2480/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/gw2480/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/gw2381/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/gw2381/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ew3280u/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ew3280u/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ex3203r/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ex3203r/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ew3270u/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ew3270u/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ex2710q-mobiuz/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ex2710q-mobiuz/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ex2710s/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ex2710s/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ew2780q/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ew2780q/manual.html',
      'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ew2780u/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ew2780u/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ex2780q/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ex2780q/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ex2510s-25-inch/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ex2510s-25-inch/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ex2510-25-inch/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ex2510-25-inch/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/wit-screenbar-plus/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/wit-screenbar-plus/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/wit-screenbar-lite/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/wit-screenbar-lite/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ew2880u/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ew2880u/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/speaker/trevolo-u/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/el2870u/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/el2870u/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/wit-screenbar/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/wit-screenbar/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ex2710/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ex2710/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/e580/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/tk850/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/tk850/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/knowledge-center/knowledge/one-set-to-control-two-systems-with-kvm.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/v7050i.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/e-reading-lamp/wit-intelligent.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/pianolight/pianolight.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/designer/pd3220u/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd3205u-32-inch.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/lh890ustd/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/portable/gv1.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/portable/gs2.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/e-reading-lamp/wit-genie.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/designer/pd3200q/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/designer/pd3200u/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/designer/pd2705q/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/designer/pd2700u/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor/designer/pd3205u/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/parent-child-lamp/mindduo/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/laser-tv-projector/i962l/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/laser-tv-projector/i967l/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/speaker/electrostatic-bluetooth-speaker/trevolo-u.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/e530/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2705u.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd3200q.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd3220u.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2725u-2021new.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2705q.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2500q.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/home-series/gk100.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/accessory.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/lu9245.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/screenbar-halo.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/wit-screenbar-lite.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/wit-screenbar.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china/chongqing.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china/zhejiang.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china/yunnan.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china/tianjin.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china/sichuan.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china/shanghai.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/registration-warranty/warranty-information.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/registration-warranty.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/manuals.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/accessory.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china/hubei.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china/henan.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china/hebei.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china/shaanxi.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china/hainan.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china/guizhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china/guangxi.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china/guangdong.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china/shanxi.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china/shandong.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/speaker/trevolo-u/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china/neimenggu.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china/liaoning.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china/jiangxi.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china/jiangsu.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china/hunan.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/e-reading-lamp.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/cineprime-home-cinema/w1700/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/lu935st1.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/gaming-projector.html',
    'https://g5-prod.benq.com.cn/zh-cn/policy/user-terms.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/v7000i.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china/fujian.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china/beijing.html',
    'https://g5-prod.benq.com.cn/zh-cn/policy/privacy-policy.html',
    'https://g5-prod.benq.com.cn/zh-cn/policy/disclaimer.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/pianolight.html',
      'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/lh890ustd.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/dx809st1.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/desklamp/floorlamp/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/pianolight/pianolight/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/wdc10.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/parent-child-lamp/wit-mindduo-s/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/e-reading-lamp/wit-genie/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/accessory/wit-desk-clamp/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/MH560.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/mx5601.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/accessory/wit-floor-stand-extension/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china/ningxia.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china/xizang.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china/xingjiang.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china/qinghai.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china/jilin.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china/heilongjiang.html',
    'https://g5-prod.benq.com.cn/zh-cn/repair-service/china/gansu.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/screenbar-lamp/wit-screenbar/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/screenbar-lamp/wit-screenbar-lite/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/screenbar-lamp/screenbar-halo/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/screenbar-lamp/wit-screenbar-plus/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/W1130.html',
    'https://g5-prod.benq.com.cn/zh-cn/about-benq.html',
    'https://g5-prod.benq.com.cn/zh-cn/policy.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/tk7001.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/desklamp/floorlamp.html',
    'https://g5-prod.benq.com.cn/zh-cn/lighting/e-reading-lamp/wit-intelligent/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/registration-warranty/warranty-information/lighting.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/registration-warranty/warranty-information/projector.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/registration-warranty/warranty-information/ifp.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/registration-warranty/warranty-information/signage.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/registration-warranty/warranty-information/monitor.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/contact-us.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/registration-warranty/warranty-information/speaker.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector-test/meeting-room-test/e320-test.html',
    'https://g5-prod.benq.com.cn/zh-cn/speaker.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chain-store.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/online-store.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/projector-faq-kn-00007.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/projector-faq-kn-00006.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-k-00173.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00056.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00055.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/neimenggu/hulunbeier.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/neimenggu/huhehaote.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/neimenggu/bayannaoer.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/neimenggu/alashan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/neimenggu/xilinguole.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/neimenggu/eerduosi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/neimenggu/wuhai.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/neimenggu/xingan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/neimenggu/baotou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/neimenggu/wulanchabu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/neimenggu/tongliao.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/neimenggu/chifeng.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shanghai/baoshan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shanghai/songjiang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shanghai/hongkou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shanghai/changning.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shanghai/putuo.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shanghai/minhang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shanghai/yangpu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shanghai/qingpu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shanghai/fengxian.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shanghai/jingan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shanghai/pudong.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shanghai/huangpu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shanghai/chongming.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shanghai/jiading.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shanghai/jinshan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jiangsu/wuxi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jiangsu/changzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jiangsu/suzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jiangsu/nantong.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jiangsu/nanjing.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jiangsu/xuzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/beijing/haidian.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/beijing/changping.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/beijing/xicheng.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/beijing/fangshan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/beijing/huairou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/beijing/chaoyang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/beijing/shunyi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/beijing/yanqing.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/beijing/pinggu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/beijing/tongzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/beijing/daxing.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/beijing/dongcheng.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/beijing/fengtai.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/beijing/shijingshan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/beijing/miyun.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/beijing/mentougou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/tianjin/jinghai.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/tianjin/baodi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/tianjin/hexi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/tianjin/jizhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/tianjin/nankai.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/tianjin/dongli.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/tianjin/hedong.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/tianjin/wuqing.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/tianjin/ninghe.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/tianjin/beichen.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/tianjin/hebei.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/tianjin/binhai.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/tianjin/xiqing.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/tianjin/heping.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/tianjin/hongqiao.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/tianjin/jinnan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/heilongjiang/daqing.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/heilongjiang/jixi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/heilongjiang/heihe.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/heilongjiang/suihua.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/heilongjiang/jiamusi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/heilongjiang/qiqihaer.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/heilongjiang/shuangyashan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/heilongjiang/mudanjiang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/heilongjiang/hegang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/heilongjiang/yichun.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/heilongjiang/haerbin.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/heilongjiang/daxinganling.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/heilongjiang/qitaihe.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shanxi/taiyuan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shanxi/jincheng.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shanxi/yangquan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shanxi/jinzhong.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shanxi/changzhi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shanxi/datong.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shanxi/yuncheng.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shanxi/linfen.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shanxi/xinzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shanxi/shuozhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shanxi/lvliang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hebei/cangzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hebei/baoding.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hebei/tangshan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hebei/langfang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hebei/qinhuangdao.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hebei/zhangjiakou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hebei/hengshui.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hebei/xingtai.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hebei/handan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hebei/chengde.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/liaoning/dandong.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/liaoning/huludao.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/liaoning/yingkou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/liaoning/fuxin.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/liaoning/chaoyang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/liaoning/dalian.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/liaoning/anshan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/liaoning/liaoyang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/liaoning/jinzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/liaoning/fushun.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/liaoning/benxi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/liaoning/tieling.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/liaoning/shenyang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/liaoning/panjin.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jilin/siping.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jilin/songyuan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jilin/changchun.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jilin/baicheng.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jilin/baishan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jilin/tonghua.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jilin/liaoyuan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jilin/yanbian.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jilin/jilin.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/taiwan/jilong.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/taiwan/taibei.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/taiwan/taoyuan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/taiwan/penghu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/taiwan/nantou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/taiwan/hualian.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/taiwan/yilan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/taiwan/taidong.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/taiwan/pingdong.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/taiwan/yunlin.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/taiwan/gaoxiong.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/taiwan/xinbei.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/taiwan/zhanghua.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/taiwan/miaoli.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/taiwan/xinzhu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/taiwan/taizhong.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/taiwan/jiayi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/taiwan/tainan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/qinghai/huangnan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/qinghai/guoluo.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/qinghai/haixi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/qinghai/haibei.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/qinghai/yushu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/qinghai/hainan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/qinghai/haidong.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/qinghai/xining.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jiangxi/jiujiang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jiangxi/shangrao.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jiangxi/jingdezhen.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jiangxi/fuzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jiangxi/yichun.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jiangxi/yingtan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jiangxi/nanchang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jiangxi/xinyu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jiangxi/pingxiang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jiangxi/ganzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jiangxi/jian.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hubei/wuhan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hubei/huangshi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hubei/suizhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hubei/xiaogan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hubei/xiangyang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hubei/tianmen.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hubei/jingmen.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hubei/ezhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hubei/jingzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hubei/qianjiang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hubei/yichang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hubei/enshi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hubei/xianning.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hubei/shennongjia.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hubei/xiantao.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hubei/huanggang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hubei/shiyan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/yunnan/baoshan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/yunnan/qujing.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/yunnan/nujiang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/yunnan/lijiang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/yunnan/lincang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/yunnan/puer.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/yunnan/diqing.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/yunnan/wenshan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/yunnan/yuxi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/yunnan/dehong.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/yunnan/zhaotong.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/yunnan/xishuangbanna.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/yunnan/dali.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/yunnan/chuxiong.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/yunnan/honghe.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/yunnan/kunming.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangxi/chongzuo.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangxi/baise.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangxi/fangchenggang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangxi/qinzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangxi/yulin.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangxi/hechi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangxi/guigang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangxi/beihai.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangxi/nanning.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangxi/guilin.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangxi/hezhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangxi/wuzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangxi/liuzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangxi/laibin.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/henan/kaifeng.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/henan/luohe.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/henan/zhumadian.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/henan/zhoukou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/henan/zhengzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/henan/xinxiang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/henan/jiyuan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/henan/anyang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/henan/sanmenxia.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/henan/luoyang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/henan/shangqiu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/henan/pingdingshan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/henan/xuchang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/henan/xinyang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/henan/puyang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/henan/jiaozuo.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/henan/nanyang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/henan/hebi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/sichuan/chengdu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/sichuan/ganzi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/sichuan/yaan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/sichuan/leshan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/sichuan/bazhong.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/sichuan/luzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/sichuan/ziyang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/sichuan/yibin.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/sichuan/zigong.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/sichuan/aba.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/sichuan/meishan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/sichuan/guangyuan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/sichuan/liangshan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/sichuan/suining.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/sichuan/deyang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/sichuan/nanchong.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/sichuan/guangan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/sichuan/mianyang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/sichuan/panzhihua.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/sichuan/dazhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/sichuan/neijiang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jiangsu/yancheng.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jiangsu/lianyungang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jiangsu/taizhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jiangsu/huaian.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jiangsu/suqian.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jiangsu/zhenjiang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jiangsu/yangzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xianggang/youjianwangqu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xianggang/jiulong.html',
   'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xianggang/shatianqu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xianggang/xigongqu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xianggang/yuanlangqu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xianggang/dapuqu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xianggang/zhongxiqu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xianggang/guantangqu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xianggang/kuiqingqu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xianggang/beiqu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xianggang/quanwanqu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xianggang/lidaoqu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xianggang/wanziqu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xianggang/tunmenqu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xianggang/nanqu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xianggang/huangdaxianqu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xianggang/shenshuibuqu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xianggang/dongqu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/ningxia/yinchuan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/ningxia/shizuishan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/ningxia/wuzhong.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/ningxia/guyuan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/ningxia/zhongwei.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/anhui/chuzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/anhui/tongling.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/anhui/huainan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/anhui/xuancheng.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/anhui/suzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/anhui/huaibei.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/anhui/hefei.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/anhui/wuhu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/anhui/bozhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/anhui/huangshan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/anhui/maanshan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/anhui/fuyang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/anhui/chizhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/anhui/bengbu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/anhui/anqing.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/anhui/liuan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xinjiang/yili.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xinjiang/kelamayi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xinjiang/shuanghe.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xinjiang/shihezi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xinjiang/tumushuke.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xinjiang/tulufan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xinjiang/kezhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xinjiang/kunyu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xinjiang/hetian.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xinjiang/kashi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xinjiang/hami.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xinjiang/kekedala.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xinjiang/bazhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xinjiang/akesu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xinjiang/huyanghe.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xinjiang/tacheng.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xinjiang/bozhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xinjiang/wujiaqu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xinjiang/tiemenguan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xinjiang/changji.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xinjiang/beitun.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xinjiang/aletai.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xinjiang/alaer.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xinjiang/wulumuqi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shandong/linyi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shandong/taian.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shandong/liaocheng.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shandong/dezhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shandong/qingdao.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shandong/jining.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shandong/heze.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shandong/binzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shandong/weihai.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shandong/zaozhuang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shandong/zibo.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shandong/dongying.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shandong/jinan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shandong/yantai.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shandong/rizhao.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shandong/weifang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xizang/linzhi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xizang/rikaze.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xizang/shannan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xizang/ali.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xizang/naqu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xizang/changdu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xizang/lasa.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/gansu/jiuquan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/gansu/tianshui.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/gansu/baiyin.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/gansu/wuwei.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/gansu/qingyang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/gansu/dingxi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/gansu/zhangye.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/gansu/gannan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/gansu/jiayuguan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/gansu/pingliang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/gansu/jinchang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/gansu/linxia.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/gansu/longnan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/gansu/lanzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/shizhu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/tongnan.html',
     'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/zhongxian.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/wuxi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/youyang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/dadukou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/fengdu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/fengjie.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/pengshui.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/dianjiang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/rongchang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/wushan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/yunyang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/hechuan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/nanchuan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/shapingba.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/bishan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/nanan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/qianjiang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/yongchuan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/tongliang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/banan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/jiulongpo.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/jiangbei.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/qijiang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/dazu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/yuzhong.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/wanzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/liangping.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/fuling.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/changshou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/kaizhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/yubei.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/wulong.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/chengkou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/xiushan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/jiangjin.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing/beibei.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/fujian/sanming.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/fujian/quanzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/fujian/zhangzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/fujian/xiamen.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/fujian/nanping.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/fujian/fuzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/fujian/ningde.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/fujian/longyan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/fujian/putian.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hainan/changjiang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hainan/baisha.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hainan/wuzhishan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hainan/qionghai.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hainan/baoting.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hainan/sansha.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hainan/sanya.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hainan/qiongzhong.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hainan/dongfang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hainan/danzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hainan/chengmai.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hainan/lingao.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hainan/dingan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hainan/tunchang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hainan/lingshui.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hainan/ledong.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hainan/wanning.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hainan/haikou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hainan/wenchang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/aomen/aomenbandao.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/aomen/luhuan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/aomen/dangzi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/aomen/ludangcheng.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hunan/zhangjiajie.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hunan/changsha.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hunan/huaihua.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hunan/zhuzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hunan/shaoyang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hunan/yiyang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hunan/yongzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hunan/hengyang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hunan/yueyang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hunan/loudi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hunan/changde.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hunan/chenzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hunan/xiangxi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hunan/xiangtan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/zhejiang/ningbo.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/zhejiang/jiaxing.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/zhejiang/shaoxing.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/zhejiang/zhoushan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/zhejiang/hangzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/zhejiang/taizhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/zhejiang/wenzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/zhejiang/huzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/zhejiang/jinhua.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/zhejiang/quzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/zhejiang/lishui.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shaanxi/tongchuan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shaanxi/ankang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shaanxi/yulin.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shaanxi/hanzhong.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shaanxi/yanan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shaanxi/weinan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shaanxi/xian.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shaanxi/xianyang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shaanxi/baoji.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shaanxi/shangluo.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guizhou/zunyi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guizhou/tongren.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guizhou/anshun.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guizhou/qiannan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guizhou/bijie.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guizhou/guiyang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guizhou/liupanshui.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guizhou/qianxinan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guizhou/qiandongnan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangdong/maoming.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangdong/shanwei.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangdong/foshan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangdong/zhuhai.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangdong/jiangmen.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangdong/shantou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangdong/huizhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangdong/chaozhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangdong/zhaoqing.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangdong/meizhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangdong/yangjiang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangdong/jieyang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangdong/yunfu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangdong/guangzhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangdong/zhanjiang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangdong/zhongshan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangdong/heyuan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangdong/shaoguan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangdong/dongguan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangdong/qingyuan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangdong/shenzhen.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/taiwan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/qinghai.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jiangxi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hubei.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/neimenggu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/yunnan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/henan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shanghai.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/sichuan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jiangsu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/ningxia.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/beijing.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/anhui.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xinjiang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shandong.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xizang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/gansu.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/chongqing.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/fujian.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hainan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/aomen.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/tianjin.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/heilongjiang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hunan.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/hebei.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/zhejiang.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shaanxi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guizhou.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/liaoning.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangdong.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/jilin.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/guangxi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/shanxi.html',
    'https://g5-prod.benq.com.cn/zh-cn/where-to-buy/china/xianggang.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ex3415r.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ex2710s.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ex2710q-mobiuz.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ex2510s-25-inch.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ew2780u.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ew2780q.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/bl2780t.html',
    'https://g5-prod.benq.com.cn/zh-cn/news/products/mobiuz-gaming-monitor-series-launch.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor-test/designer-test/pd2500q/reviews.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor-test/designer-test/pd2500q/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor-test/designer-test/pd2500q.html',
    'https://g5-prod.benq.com.cn/zh-cn/configuration.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor-test/video-enjoyment-test/ew3280u-test/question.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor-test/video-enjoyment-test/ew3280u-test/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor-test/video-enjoyment-test/ex3203r-test/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor-test/video-enjoyment-test.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor-test/designer-test/pd2720u-test/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor-test/designer-test/pd2700q-test/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor-test/designer-test.html',
    'https://g5-prod.benq.com.cn/zh-cn/monitor-test.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector-test/meeting-room-test/mx631st-test/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector-test/meeting-room-test/mh750-test/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector-test/meeting-room-test.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector-test/portable-test/gv30-test/question.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector-test/portable-test/gv30-test/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector-test/portable-test/gs2-test/question.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector-test/portable-test/gs2-test/specifications.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector-test/portable-test.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector-test.html',
    'https://g5-prod.benq.com.cn/zh-cn/job-ref-macau.html',
    'https://g5-prod.benq.com.cn/zh-cn/job-ref-government.html',
    'https://g5-prod.benq.com.cn/zh-cn/job-ref-fandb.html',
    'https://g5-prod.benq.com.cn/zh-cn/job-ref-commercial.html',
    'https://g5-prod.benq.com.cn/zh-cn/job-ref-sen.html',
    'https://g5-prod.benq.com.cn/zh-cn/job-ref-kindergarten.html',
    'https://g5-prod.benq.com.cn/zh-cn/job-ref-secondaryschool.html',
    'https://g5-prod.benq.com.cn/zh-cn/job-ref-primaryschool.html',
    'https://g5-prod.benq.com.cn/zh-cn/job-ref-university.html',
    'https://g5-prod.benq.com.cn/zh-cn/newsletter/unsubscribe.html',
    'https://g5-prod.benq.com.cn/zh-cn/newsletter/subscribe.html',
    'https://g5-prod.benq.com.cn/zh-cn/newsletter.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/benq-netvigator-promotion.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/digitaleducation/quote-thank-you.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/digitaleducation/thank-you.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/digitaleducation/casestudy/friesenheim-test.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/digitaleducation/casestudy/iggs.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/digitaleducation/casestudy/gymnasium-luebz.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/digitaleducation/casestudy/british-international-school-riyadh.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/digitaleducation/casestudy/woodmansterne-school.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/digitaleducation/casestudy/friesenheim.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/digitaleducation/casestudy.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/digitaleducation/what-why-ifp/five-ways-interactive-flat-panels-improve-engagement-infographic.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/digitaleducation/what-why-ifp/nanosilver-power.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/digitaleducation/what-why-ifp/iwb-ifp-comparison.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/digitaleducation/what-why-ifp/benq-interactive-displays-minimize-the-spread-of-pathogens.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/digitaleducation/what-why-ifp/five-ways-interactive-flat-panels-improve-engagement.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/digitaleducation/what-why-ifp/top-10-advantages-of-benq-ifp.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/digitaleducation/what-why-ifp.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/digitaleducation/ifp/solution.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/digitaleducation/ifp/rp704k.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/digitaleducation/ifp/rp6501k.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/digitaleducation/ifp/rp7501k.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/digitaleducation/ifp/rp8601k.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/digitaleducation/ifp.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/digitaleducation/trend/silver-ions-pioneering-antimicrobial-defense-in-classrooms.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/digitaleducation/trend/empower-innovation-and-productivity.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/digitaleducation/trend/4-reasons-edtech-is-a-must.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/digitaleducation/trend/distance-teaching-and-learning.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/digitaleducation/trend/education-digital-transformation.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/digitaleducation/trend.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/digitaleducation.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/wit-jp/products/expert-02.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/wit-jp/products/expert-01.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/wit-jp/products.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign/wit-jp.html',
    'https://g5-prod.benq.com.cn/zh-cn/campaign.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/tabletpc-faq-00011.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/tabletpc-faq-00010.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/tabletpc-faq-00009.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/tabletpc-faq-00008.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/tabletpc-faq-00007.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/tabletpc-faq-00006.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/tabletpc-faq-00005.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/tabletpc-faq-00004.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/tabletpc-faq-00003.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/tabletpc-faq-00002.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/tabletpc-faq-00001.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/projector-faq-00032.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/projector-faq-00031.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/projector-faq-00030.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/projector-faq-00027.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/projector-faq-00026.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/projector-faq-00025.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/projector-faq-00024.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/projector-faq-00023.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/projector-faq-00022.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/projector-faq-00021.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/projector-faq-00020.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/projector-faq-00019.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/projector-faq-00018.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/projector-faq-00017.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/projector-faq-00016.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/projector-faq-00015.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/projector-faq-00014.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/projector-faq-00013.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/projector-faq-00012.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/projector-faq-00011.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/projector-faq-00010.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/projector-faq-00009.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/projector-faq-00008.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/projector-faq-00007.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/projector-faq-00006.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/projector-faq-00005.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/projector-faq-00004.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/projector-faq-00003.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/projector-faq-00002.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/projector-faq-00001.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/nreader-faq-00018.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/nreader-faq-00017.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/nreader-faq-00013.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/nreader-faq-00012.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/nreader-faq-00011.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/nreader-faq-00010.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/nreader-faq-00009.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/nreader-faq-00008.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/nreader-faq-00007.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/nreader-faq-00006.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/nreader-faq-00005.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/nreader-faq-00004.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/nreader-faq-00003.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/nreader-faq-00002.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/nreader-faq-00001.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00065.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00064.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00063.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00062.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00061.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00060.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00059.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00058.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00057.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00056.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00055.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00054.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00053.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00052.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00051.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00050.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00049.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00048.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00047.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00046.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00045.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00044.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00043.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00042.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00041.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00040.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00039.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00038.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00037.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00036.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00035.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00034.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00033.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00032.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00031.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00030.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00029.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00028.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00027.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00026.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00025.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00024.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00023.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00022.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00021.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00020.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00019.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00018.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00017.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00016.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00015.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00014.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00013.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00012.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00011.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00010.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00009.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00008.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00007.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00006.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00005.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00004.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00003.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00002.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/notebook-faq-00001.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/monitor-faq-00019.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/monitor-faq-00018.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/monitor-faq-00017.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/monitor-faq-00015.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/monitor-faq-00014.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/monitor-faq-00013.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/monitor-faq-00012.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/monitor-faq-00011.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/monitor-faq-00010.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/monitor-faq-00009.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/monitor-faq-00008.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/monitor-faq-00007.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/monitor-faq-00006.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/monitor-faq-00005.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/monitor-faq-00004.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/monitor-faq-00003.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/monitor-faq-00002.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/monitor-faq-00001.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00063.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00062.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00061.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00060.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00059.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00058.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00057.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00054.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00053.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00052.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00051.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00050.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00049.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00048.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00047.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00046.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00045.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00044.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00043.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00042.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00041.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00040.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00039.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00038.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00037.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00036.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00023.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00022.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00021.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00020.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00002.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/dsc-faq-00001.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/androidsmartdevice-faq-00012.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/androidsmartdevice-faq-00011.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/androidsmartdevice-faq-00010.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/androidsmartdevice-faq-00009.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/androidsmartdevice-faq-00008.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/androidsmartdevice-faq-00007.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/androidsmartdevice-faq-00006.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/androidsmartdevice-faq-00005.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/androidsmartdevice-faq-00004.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/androidsmartdevice-faq-00003.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/androidsmartdevice-faq-00002.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others/androidsmartdevice-faq-00001.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/others.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-kn-00005.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-kn-00004.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-k-00024.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-k-00033.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-k-00034.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-k-00028.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-k-00027.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-k-00026.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-k-00023.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-k-00012.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-k-00009.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-k-00008.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-k-00005.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-k-00004.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-k-00001.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-k-00036.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/projector-faq-kn-00009.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/projector-faq-kn-00010.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-k-00032.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-k-00031.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-k-00030.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-k-00029.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/projector-faq-kn-00008.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-kn-00001.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/projector-faq-k-00038.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00017.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00016.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00059.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00058.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00057.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00050.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00054.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-k-00020.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00012.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00011.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-k-00051.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00015.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-k-00162.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-k-00161.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-k-00017.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-k-00045.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-k-00040.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-k-00039.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-k-00033.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-k-00023.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-k-00021.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-k-00015.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-k-00018.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-k-00016.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-k-00013.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-k-00012.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-k-00056.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00003.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00012.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00014.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00013.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00011.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00010.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00008.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00009.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00006.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00007.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00005.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00004.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-k-00159.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-k-00157.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-k-00156.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-k-00154.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-k-00153.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-k-00151.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-k-00145.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-k-00148.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-k-00150.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-k-00147.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-k-00146.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-k-00149.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-k-00143.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-k-00142.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-k-00141.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-k-00139.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-k-00138.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-k-00137.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00049.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00051.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-k-00068.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00047.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00002.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00014.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00004.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00003.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00002.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00001.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00001.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-k-00055.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-k-00054.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-k-00053.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-00029.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-00028.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/nreader-faq-00016.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/nreader-faq-00014.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/notebook-faq-00066.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/dsc-faq-00056.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/action-cam-faq-00003.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/action-cam-faq-00002.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/action-cam-faq-00001.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/faq/product.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/manuals/others/monitor-others-001.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/manuals/others.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/manuals/datasheet/projector-datasheet-001.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/manuals/datasheet/signage-datasheet-006.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/manuals/datasheet/signage-datasheet-005.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/manuals/datasheet/signage-datasheet-004.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/manuals/datasheet/signage-datasheet-003.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/manuals/datasheet/signage-datasheet-002.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/manuals/datasheet/signage-datasheet-001.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/manuals/datasheet/notebook-datasheet-003.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/manuals/datasheet/notebook-datasheet-002.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/manuals/datasheet/notebook-datasheet-001.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/manuals/datasheet.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/software/gs1-airpin-pc-software-001.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/software.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-037.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-036.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-035.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-034.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/signage-bios-003.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/signage-bios-002.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/signage-bios-001.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-033.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-032.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-031.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-030.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/nreader-bios-007.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/nreader-bios-006.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/nreader-bios-005.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/nreader-bios-004.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/nreader-bios-003.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/nreader-bios-002.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/nreader-bios-001.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-029.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-028.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-027.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-026.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-025.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-024.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-023.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-022.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-021.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-020.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-019.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-018.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-017.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-016.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-015.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-014.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-013.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-012.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-011.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-010.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-009.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-008.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-007.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-006.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-005.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-004.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-003.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-002.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/notebook-bios-001.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/projector-bios-001.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/aiopc-bios-004.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/aiopc-bios-003.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/aiopc-bios-002.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios/aiopc-bios-001.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/software-driver/bios.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/speaker.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ew2480-24-inch.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2720u/warranty.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2720u/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2720u/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2720u/video.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2720u/faq.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2720u.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ew2780.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ew3280u.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ex2780q.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/gw2381.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/gw2780.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ex3203r.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ew3270u.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2700q.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/sw271.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/sw320.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/el2870u.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/bl2480t.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/gw2480.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd3200u.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/w1800i/warranty.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/w1800i/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/w1800i/video.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/w1800i/faq.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/w1800i/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/w1800i.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/tk700sti/warranty.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/tk700sti/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/tk700sti/video.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/tk700sti/faq.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/tk700sti/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/tk700sti.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/tk850.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/tk800m.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/th585.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/th671st.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/tk810.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/th685.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/w5700.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/w2700.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector.html',
    'https://g5-prod.benq.com.cn/zh-cn/news/event/benq-netvigator-promotion.html',
    'https://g5-prod.benq.com.cn/zh-cn/news/event/1465804423-25-1022.html',
    'https://g5-prod.benq.com.cn/zh-cn/news/event.html',
    'https://g5-prod.benq.com.cn/zh-cn/news/products/1486458204-23-1024.html',
    'https://g5-prod.benq.com.cn/zh-cn/news/products/1463542590-23-1019.html',
    'https://g5-prod.benq.com.cn/zh-cn/news/products/1452665342-23-1010.html',
    'https://g5-prod.benq.com.cn/zh-cn/news/products/1443774558-23-1002.html',
    'https://g5-prod.benq.com.cn/zh-cn/news/products/1430808704-23-992.html',
    'https://g5-prod.benq.com.cn/zh-cn/news/products/1430105065-23-991.html',
    'https://g5-prod.benq.com.cn/zh-cn/news/products/1426816990-23-984.html',
    'https://g5-prod.benq.com.cn/zh-cn/news/products/1418954505-23-977.html',
    'https://g5-prod.benq.com.cn/zh-cn/news/products/1415865107-23-970.html',
    'https://g5-prod.benq.com.cn/zh-cn/news/products/1415863608-23-967.html',
    'https://g5-prod.benq.com.cn/zh-cn/news/products/1412047156-23-963.html',
    'https://g5-prod.benq.com.cn/zh-cn/news/products/1410749641-23-962.html',
    'https://g5-prod.benq.com.cn/zh-cn/news/products/1403242820-23-958.html',
    'https://g5-prod.benq.com.cn/zh-cn/news/products/1396886400-23-954.html',
    'https://g5-prod.benq.com.cn/zh-cn/news/products.html',
    'https://g5-prod.benq.com.cn/zh-cn/news/corporate/1449727710-20-1005.html',
    'https://g5-prod.benq.com.cn/zh-cn/news/corporate.html',
    'https://g5-prod.benq.com.cn/zh-cn/news.html',
    'https://g5-prod.benq.com.cn/zh-cn/recycle.html',
    'https://g5-prod.benq.com.cn/zh-cn/404.html',
    'https://g5-prod.benq.com.cn/zh-cn/sitemap.html',
    'https://g5-prod.benq.com.cn/zh-cn/template/template/pagetest.html',
    'https://g5-prod.benq.com.cn/zh-cn/template/template/template.html',
    'https://g5-prod.benq.com.cn/zh-cn/template/template.html',
    'https://g5-prod.benq.com.cn/zh-cn/template.html',
    // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages/menu/virtual-patent-marking.html',
    // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages/menu/welcome.html',
    // // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages/menu/inside-studio.html',(連不上)
    // // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages/menu/eye-care-solution.html',(連不上)
    // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages/menu.html',
    // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages/l-a-b-color-space.html',
    // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages/u-v-chromaticity.html',
    // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages/xy-chromaticity-diagram.html',
    // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages/life-style.html',
    // // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages/monitor/insidestudio.html',(連不上)
    // // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages/monitor/pv270-downloads.html', 404
    // // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages/monitor/pv3200pt-downloads.html', 404
    // // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages/monitor/sw2700pt-downloads.html', 404
    // // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages/monitor/sw320-downloads.html', 404
    // // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages/monitor/pv270-palette-master-software.html', 404
    // // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages/monitor/pd2710qc-compatibility-list.html', 404是檔案
    // // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages/monitor/eye-care.html', (連不上)
    // // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages/monitor/shading-hood.html',(youtube)
    // // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages/monitor/color-display-cone.html',(youtube)
    // // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages/monitor/gamutduo.html',(youtube)
    // // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages/monitor/dualview-mode.html',(youtube)
    // // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages/monitor/kvm-switch.html',(youtube)
    // // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages/monitor/hotkey-puck.html',(youtube)
    // // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages/monitor/animation-mode.html',(youtube)
    // // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages/monitor/cad-cam-mode.html',(youtube)
    // // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages/monitor/ew2775zh-pdf.html',(檔案)
    // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages/monitor.html',
    // // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages/projector/dlp-projection-technology.html',(連不上)
    // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages/projector.html',
    // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages/home.html',
    // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages/test-redirect.html',
    // 'https://g5-prod.benq.com.cn/zh-cn/redirect-pages.html',
    'https://g5-prod.benq.com.cn/zh-cn/e-sports.html',
    'https://g5-prod.benq.com.cn/zh-cn/business-display.html',
    'https://g5-prod.benq.com.cn/zh-cn/life-style/led-lighting/compare.html',
    'https://g5-prod.benq.com.cn/zh-cn/life-style/led-lighting/wit/wit.html',
    'https://g5-prod.benq.com.cn/zh-cn/life-style/led-lighting/wit.html',
    'https://g5-prod.benq.com.cn/zh-cn/life-style/led-lighting.html',
    'https://g5-prod.benq.com.cn/zh-cn/life-style.html',
    'https://g5-prod.benq.com.cn/zh-cn/projector/gaming-projector/th585/support.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2725u-old/warranty.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2725u-old/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2725u-old/faq.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2725u-old/video.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2725u-old/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2725u-old.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2710qc/warranty.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2710qc/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2710qc/faq.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2710qc/video.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2710qc/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/palette-master-element/warranty.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/palette-master-element/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/palette-master-element/faq.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/palette-master-element/video.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/palette-master-element/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pv270/warranty.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pv270/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pv270/faq.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pv270/video.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pv270/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/paper-color-sync/warranty.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/paper-color-sync/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/paper-color-sync/faq.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/paper-color-sync/video.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/paper-color-sync/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/wit-mindduo1/warranty.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/wit-mindduo1/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/wit-mindduo1/faq.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/wit-mindduo1/video.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/wit-mindduo1/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/lighting/wit-mindduo1.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/sw271c1/warranty.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/sw271c1/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/sw271c1/faq.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/sw271c1/video.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/sw271c1/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/sw271c1.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/LU9245/warranty.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/LU9245/software-driver.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/LU9245/faq.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/LU9245/video.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/LU9245/manual.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/LU9245.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ew3880r-38-inch.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ex3210u.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/palette-master-element.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ex2710.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd3420q.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/w1130.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ex3210r.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/paper-color-sync.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ex2510-25-inch.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/gw2480t.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pd2710qc.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/pv270.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/e580.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/e530.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/e520.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/e320.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/e592.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/e582.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/e590.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/gw2485tc.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/i707.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/tk700st.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/monitor/ew2880u.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/w1800.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/gs2.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/gk100.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/i980l.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/i985l.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/e540.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/mw632st.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/lu9255.html',
    'https://g5-prod.benq.com.cn/zh-cn/support/downloads-faq/products/projector/lk953st.html'
  ]

//測試用
// const testPass=[]
// const testFail=[]

const topOnePass=[]
const topOneFail=[]

const topTwoPass=[]
const topTwoFail=[]

const topThreePass=[]
const topThreeFail=[]

const topFourPass=[]
const topFourFail=[]

const topFivePass=[]
const topFiveFail=[]

const topSixPass=[]
const topSixFail=[]

const topSevenPass=[]
const topSevenFail=[]

const topEightPass=[]
const topEightFail=[]

const topNinePass=[]
const topNineFail=[]

const topTenPass=[]
const topTenFail=[]

const topElevenPass=[]
const topElevenFail=[]

const topTwelvePass=[]
const topTwelveFail=[]

const topThirteenPass=[]
const topThirteenFail=[]

const topFourteenPass=[]
const topFourteenFail=[]

const firstOnePass=[]
const firstOneFail=[]

const firstTwoPass=[]
const firstTwoFail=[]

const firstThreePass=[]
const firstThreeFail=[]

const firstFourPass=[]
const firstFourFail=[]

const firstFivePass=[]
const firstFiveFail=[]

const firstSixPass=[]
const firstSixFail=[]

const firstSevenPass=[]
const firstSevenFail=[]

const firstEightPass=[]
const firstEightFail=[]

const firstNinePass=[]
const firstNineFail=[]

const firstTenPass=[]
const firstTenFail=[]

const firstElevenPass=[]
const firstElevenFail=[]

const firstTwelvePass=[]
const firstTwelveFail=[]

const firstThirteenPass=[]
const firstThirteenFail=[]

const firstFourteenPass=[]
const firstFourteenFail=[]

const firstFifteenPass=[]
const firstFifteenFail=[]

const goldFirstPass=[]
const goldFirstFail=[]

const goldTwoPass=[]
const goldTwoFail=[]

const goldThreePass=[]
const goldThreeFail=[]

const goldFourPass=[]
const goldFourFail=[]

// const goldFivePass=[]
// const goldFiveFail=[]

const goldSixPass=[]
const goldSixFail=[]

const goldSevenPass=[]
const goldSevenFail=[]

const goldEightPass=[]
const goldEightFail=[]

const goldNinePass=[]
const goldNineFail=[]

// const goldTenPass=[]
// const goldTenFail=[]

// const goldElevenPass=[]
// const goldElevenFail=[]

const goldTwelvePass=[]
const goldTwelveFail=[]

const goldThirteenPass=[]
const goldThirteenFail=[]

const goldFourteenPass=[]
const goldFourteenFail=[]

const mostFirstPass=[]
const mostFirstFail=[]

const mostTwoPass=[]
const mostTwoFail=[]

const mostThreePass=[]
const mostThreeFail=[]

const mostFourPass=[]
const mostFourFail=[]

const mostFivePass=[]
const mostFiveFail=[]

const mostSixPass=[]
const mostSixFail=[]

const mostSevenPass=[]
const mostSevenFail=[]

const mostEightPass=[]
const mostEightFail=[]

const mostNinePass=[]
const mostNineFail=[]

const mostTenPass=[]
const mostTenFail=[]

const mostElevenPass=[]
const mostElevenFail=[]

const mostTwelvePass=[]
const mostTwelveFail=[]

const mostThirteenPass=[]
const mostThirteenFail=[]

const mostFourteenPass=[]
const mostFourteenFail=[]

const mostFifteenPass=[]
const mostFifteenFail=[]

const mostSixteenPass=[]
const mostSixteenFail=[]

const mostSeventeenPass=[]
const mostSeventeenFail=[]

const authorityOnePass=[]
const authorityOneFail=[]

const authorityTwoPass=[]
const authorityTwoFail=[]

const authorityThreePass=[]
const authorityThreeFail=[]

const authorityFourPass=[]
const authorityFourFail=[]

const authorityFivePass=[]
const authorityFiveFail=[]

const authoritySixPass=[]
const authoritySixFail=[]

const authoritySevenPass=[]
const authoritySevenFail=[]

const authorityEightPass=[]
const authorityEightFail=[]

const authorityNinePass=[]
const authorityNineFail=[]

const prizeOnePass=[]
const prizeOneFail=[]

const prizeTwoPass=[]
const prizeTwoFail=[]

const prizeThreePass=[]
const prizeThreeFail=[]

const prizeFourPass=[]
const prizeFourFail=[]

const prizeFivePass=[]
const prizeFiveFail=[]

const prizeSixPass=[]
const prizeSixFail=[]

const prizeSevenPass=[]
const prizeSevenFail=[]

const prizeEightPass=[]
const prizeEightFail=[]

const prizeNinePass=[]
const prizeNineFail=[]

const prizeTenPass=[]
const prizeTenFail=[]

const prizeElevenPass=[]
const prizeElevenFail=[]

const prizeTwelvePass=[]
const prizeTwelveFail=[]

const prizeThirteenPass=[]
const prizeThirteenFail=[]

const prizeFourteenPass=[]
const prizeFourteenFail=[]

const prizeFifteenPass=[]
const prizeFifteenFail=[]

const prizeSixteenPass=[]
const prizeSixteenFail=[]

const prizeSeventeenPass=[]
const prizeSeventeenFail=[]

const prizeEighteenPass=[]
const prizeEighteenFail=[]

const prizeNineteenPass=[]
const prizeNineteenFail=[]


Before({timeout: 24 * 5000},async function () {
    this.browser = await puppeteer.launch({ 
        executablePath:
        "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
        // MAC路徑:
        // executablePath:
        // '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
        headless:true,//有無需要開視窗,false要開,true不開
        slowMo:100,// slow down by 100ms
        devtools:false//有無需要開啟開發人員工具
    })
    this.page = await this.browser.newPage()
    await this.page.setViewport({width:1200,height:1000})
    await this.page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
    await this.page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
})

After({timeout: 12 * 5000},async function () {
    await this.browser.close()
})

const urlAll=[]
const wordAll=[]

Given("check all URL",{timeout: 120000 * 5000},async function(){
    for(let i =0; i<bqcUrlAll.length; i++){
        //測試用
        // await this.page.goto("https://g5-prod.benq.com.cn/zh-cn/business/monitor/pd2700q1/specifications.html" +cicGA)
        // const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // console.log("灵感成真index",innerHtml.indexOf("灵感成真"))

        //bqcUrlAll.length所有URL數量
        const bqcUrlCheck = bqcUrlAll[i]
        const word=[]
        // await this.page.goto(bqcUrlCheck+cicGA)
        await this.page.goto(bqcUrlCheck)
        await this.page.waitForSelector('html')
        // const innerHtml = await page.$eval('body', element => element.innerHTML);
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // console.log(bqcUrlCheck,"灵感成真index",innerHtml.indexOf("灵感成真"))

        //測試用
        // if(innerHtml.indexOf("灵感成真")<0){
        //     testPass.push("https://g5-prod.benq.com.cn/zh-cn/business/monitor/pd2700q1/specifications.html")
        //     //console.log(`${bqcUrlCheck} is pass`)
        // }else{
        //     console.log(`Fail URL:"https://g5-prod.benq.com.cn/zh-cn/business/monitor/pd2700q1/specifications.html"`)
        //     testFail.push("https://g5-prod.benq.com.cn/zh-cn/business/monitor/pd2700q1/specifications.html")
        // }  
        //測試用
        // if(innerHtml.indexOf("灵感成真")<0){
        //     testPass.push(bqcUrlCheck)
        //     //console.log(`${bqcUrlCheck} is pass`)
        // }else{
        //     console.log(`Fail URL: ${bqcUrlCheck}`)
        //     testFail.push(bqcUrlCheck)
        // }        
        if(innerHtml.indexOf("国家级")<0){
            topOnePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("国家级")
            //console.log(`Fail URL(国家级): ${bqcUrlCheck}`)
            topOneFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("世界级")<0){
            topTwoPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("世界级")
            //console.log(`Fail URL(世界级): ${bqcUrlCheck}`)
            topTwoFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("最高级")<0){
            topThreePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("最高级")
            //console.log(`Fail URL(最高级): ${bqcUrlCheck}`)
            topThreeFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("顶级")<0){
            topFourPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("顶级")
            //console.log(`Fail URL(顶级): ${bqcUrlCheck}`)
            topFourFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("顶尖")<0){
            topFivePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("顶尖")
            //console.log(`Fail URL(顶尖): ${bqcUrlCheck}`)
            topFiveFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("最高档")<0){
            topSixPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("最高档")
            //console.log(`Fail URL(最高档): ${bqcUrlCheck}`)
            topSixFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("世界领先")<0){
            topSevenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("世界领先")
            //console.log(`Fail URL(世界领先): ${bqcUrlCheck}`)
            topSevenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("国家示范")<0){
            topEightPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("国家示范")
            //console.log(`Fail URL(国家示范): ${bqcUrlCheck}`)
            topEightFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("全国领先")<0){
            topNinePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("全国领先")
            //console.log(`Fail URL(全国领先): ${bqcUrlCheck}`)
            topNineFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("行业顶尖")<0){
            topTenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("行业顶尖")
            //console.log(`Fail URL(行业顶尖): ${bqcUrlCheck}`)
            topTenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("行业领先")<0){
            topElevenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("行业领先")
            //console.log(`Fail URL(行业领先): ${bqcUrlCheck}`)
            topElevenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("领衔")<0){
            topTwelvePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("领衔")
            //console.log(`Fail URL(领衔): ${bqcUrlCheck}`)
            topTwelveFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("问鼎")<0){
            topThirteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("问鼎")
            //console.log(`Fail URL(问鼎): ${bqcUrlCheck}`)
            topThirteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("开创之举")<0){
            topFourteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("开创之举")
            //console.log(`Fail URL(开创之举): ${bqcUrlCheck}`)
            topFourteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("第一")<0){
            firstOnePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("第一")
            //console.log(`Fail URL(第一): ${bqcUrlCheck}`)
            firstOneFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("唯一")<0){
            firstTwoPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("唯一")
            //console.log(`Fail URL(唯一): ${bqcUrlCheck}`)
            firstTwoFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("首个")<0){
            firstThreePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("首个")
            //console.log(`Fail URL(首个): ${bqcUrlCheck}`)
            firstThreeFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("推荐首选")<0){
            firstFourPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("推荐首选")
            //console.log(`Fail URL(推荐首选): ${bqcUrlCheck}`)
            firstFourFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("首家")<0){
            firstFivePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("首家")
            //console.log(`Fail URL(首家): ${bqcUrlCheck}`)
            firstFiveFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("独家")<0){
            firstSixPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("独家")
            //console.log(`Fail URL(独家): ${bqcUrlCheck}`)
            firstSixFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("第一人")<0){
            firstSevenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("第一人")
            //console.log(`Fail URL(第一人): ${bqcUrlCheck}`)
            firstSevenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("首席")<0){
            firstEightPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("首席")
            //console.log(`Fail URL(首席): ${bqcUrlCheck}`)
            firstEightFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("独一无二")<0){
            firstNinePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("独一无二")
            //console.log(`Fail URL(独一无二): ${bqcUrlCheck}`)
            firstNineFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("绝无仅有")<0){
            firstTenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("绝无仅有")
            //console.log(`Fail URL(绝无仅有): ${bqcUrlCheck}`)
            firstTenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("前无古人")<0){
            firstElevenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("前无古人")
            //console.log(`Fail URL(前无古人): ${bqcUrlCheck}`)
            firstElevenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("100%")<0){
            firstTwelvePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("100%")
            //console.log(`Fail URL(100%): ${bqcUrlCheck}`)
            firstTwelveFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("百分百")<0){
            firstThirteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("百分百")
            //console.log(`Fail URL(百分百): ${bqcUrlCheck}`)
            firstThirteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("史上")<0){
            firstFourteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("史上")
            //console.log(`Fail URL(史上): ${bqcUrlCheck}`)
            firstFourteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("非常一流")<0){
            firstFifteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("非常一流")
            //console.log(`Fail URL(非常一流): ${bqcUrlCheck}`)
            firstFifteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("金牌")<0){
            goldFirstPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("金牌")
            //console.log(`Fail URL(金牌): ${bqcUrlCheck}`)
            goldFirstFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("名牌")<0){
            goldTwoPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("名牌")
            //console.log(`Fail URL(名牌): ${bqcUrlCheck}`)
            goldTwoFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("王牌")<0){
            goldThreePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("王牌")
            //console.log(`Fail URL(王牌): ${bqcUrlCheck}`)
            goldThreeFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("销量冠军")<0){
            goldFourPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("销量冠军")
            //console.log(`Fail URL(销量冠军): ${bqcUrlCheck}`)
            goldFourFail.push(bqcUrlCheck)
        }
        // if(innerHtml.indexOf("第一")<0){
        //     goldFivePass.push(bqcUrlCheck)
        //     //console.log(`${bqcUrlCheck} is pass`)
        // }else{
        //     console.log(`Fail URL(第一): ${bqcUrlCheck}`)
        //     goldFiveFail.push(bqcUrlCheck)
        // }
        if(innerHtml.indexOf("NO.1")<0){
            goldSixPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("NO.1")
            //console.log(`Fail URL(NO.1): ${bqcUrlCheck}`)
            goldSixFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("Top1")<0){
            goldSevenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("Top1")
            //console.log(`Fail URL(Top1): ${bqcUrlCheck}`)
            goldSevenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("领袖品牌")<0){
            goldEightPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("领袖品牌")
            //console.log(`Fail URL(领袖品牌): ${bqcUrlCheck}`)
            goldEightFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("领导品牌")<0){
            goldNinePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("领导品牌")
            //console.log(`Fail URL(领导品牌): ${bqcUrlCheck}`)
            goldNineFail.push(bqcUrlCheck)
        }
        // if(innerHtml.indexOf("顶级")<0){
        //     goldTenPass.push(bqcUrlCheck)
        //     //console.log(`${bqcUrlCheck} is pass`)
        // }else{
            // word.push("")
        //     console.log(`Fail URL(顶级): ${bqcUrlCheck}`)
        //     goldTenFail.push(bqcUrlCheck)
        // }
        // if(innerHtml.indexOf("顶尖")<0){
        //     goldElevenPass.push(bqcUrlCheck)
        //     //console.log(`${bqcUrlCheck} is pass`)
        // }else{
            // word.push("")
        //     console.log(`Fail URL(顶尖): ${bqcUrlCheck}`)
        //     goldElevenFail.push(bqcUrlCheck)
        // }
        if(innerHtml.indexOf("泰斗")<0){
            goldTwelvePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("泰斗")
            //console.log(`Fail URL(泰斗): ${bqcUrlCheck}`)
            goldTwelveFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("金标")<0){
            goldThirteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("金标")
            //console.log(`Fail URL(金标): ${bqcUrlCheck}`)
            goldThirteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("驰名尖端")<0){
            goldFourteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("驰名尖端")
            //console.log(`Fail URL(驰名尖端): ${bqcUrlCheck}`)
            goldFourteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("最")<0){
            mostFirstPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("最")
            //console.log(`Fail URL(最): ${bqcUrlCheck}`)
            mostFirstFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("终极")<0){
            mostTwoPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("终极")
            //console.log(`Fail URL(终极): ${bqcUrlCheck}`)
            mostTwoFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("极致")<0){
            mostThreePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("极致")
            //console.log(`Fail URL(极致): ${bqcUrlCheck}`)
            mostThreeFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("万能")<0){
            mostFourPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("万能")
            //console.log(`Fail URL(万能): ${bqcUrlCheck}`)
            mostFourFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("决对")<0){
            mostFivePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("决对")
            //console.log(`Fail URL(决对): ${bqcUrlCheck}`)
            mostFiveFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("彻底")<0){
            mostSixPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("彻底")
            //console.log(`Fail URL(彻底): ${bqcUrlCheck}`)
            mostSixFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("完全")<0){
            mostSevenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("完全")
            //console.log(`Fail URL(完全): ${bqcUrlCheck}`)
            mostSevenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("极佳")<0){
            mostEightPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("极佳")
            //console.log(`Fail URL(极佳): ${bqcUrlCheck}`)
            mostEightFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("全解决")<0){
            mostNinePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("全解决")
            //console.log(`Fail URL(全解决): ${bqcUrlCheck}`)
            mostNineFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("全方位")<0){
            mostTenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("全方位")
            //console.log(`Fail URL(全方位): ${bqcUrlCheck}`)
            mostTenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("全面改善")<0){
            mostElevenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("全面改善")
            //console.log(`Fail URL(全面改善): ${bqcUrlCheck}`)
            mostElevenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("精确")<0){
            mostTwelvePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("精确")
            //console.log(`Fail URL(精确): ${bqcUrlCheck}`)
            mostTwelveFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("准确")<0){
            mostThirteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("准确")
            //console.log(`Fail URL(准确): ${bqcUrlCheck}`)
            mostThirteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("精准")<0){
            mostFourteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("精准")
            //console.log(`Fail URL(精准): ${bqcUrlCheck}`)
            mostFourteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("优秀")<0){
            mostFifteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("优秀")
            //console.log(`Fail URL(优秀): ${bqcUrlCheck}`)
            mostFifteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("完美")<0){
            mostSixteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("完美")
            //console.log(`Fail URL(完美): ${bqcUrlCheck}`)
            mostSixteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("永久")<0){
            mostSeventeenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("永久")
            //console.log(`Fail URL(永久): ${bqcUrlCheck}`)
            mostSeventeenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("老字号")<0){
            authorityOnePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("老字号")
            //console.log(`Fail URL(老字号): ${bqcUrlCheck}`)
            authorityOneFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("中国驰名商标")<0){
            authorityTwoPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("中国驰名商标")
            //console.log(`Fail URL(中国驰名商标): ${bqcUrlCheck}`)
            authorityTwoFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("特供")<0){
            authorityThreePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("特供")
            //console.log(`Fail URL(特供): ${bqcUrlCheck}`)
            authorityThreeFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("专供")<0){
            authorityFourPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("专供")
            //console.log(`Fail URL(专供): ${bqcUrlCheck}`)
            authorityFourFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("专家推荐")<0){
            authorityFivePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("专家推荐")
            //console.log(`Fail URL(专家推荐): ${bqcUrlCheck}`)
            authorityFiveFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("国家免检")<0){
            authoritySixPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("国家免检")
            //console.log(`Fail URL(国家免检): ${bqcUrlCheck}`)
            authoritySixFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("质量免检")<0){
            authoritySevenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("质量免检")
            //console.log(`Fail URL(质量免检): ${bqcUrlCheck}`)
            authoritySevenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("无需国家质量检测")<0){
            authorityEightPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("无需国家质量检测")
            //console.log(`Fail URL(无需国家质量检测): ${bqcUrlCheck}`)
            authorityEightFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("免抽检")<0){
            authorityNinePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("免抽检")
            //console.log(`Fail URL(免抽检): ${bqcUrlCheck}`)
            authorityNineFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("点击领奖")<0){
            prizeOnePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("点击领奖")
            //console.log(`Fail URL(点击领奖): ${bqcUrlCheck}`)
            prizeOneFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("恭喜获奖")<0){
            prizeTwoPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("恭喜获奖")
            //console.log(`Fail URL(恭喜获奖): ${bqcUrlCheck}`)
            prizeTwoFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("全民免单")<0){
            prizeThreePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("全民免单")
            //console.log(`Fail URL(全民免单): ${bqcUrlCheck}`)
            prizeThreeFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("点击有惊喜")<0){
            prizeFourPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("点击有惊喜")
            //console.log(`Fail URL(点击有惊喜): ${bqcUrlCheck}`)
            prizeFourFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("点击获取")<0){
            prizeFivePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("点击获取")
            //console.log(`Fail URL(点击获取): ${bqcUrlCheck}`)
            prizeFiveFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("点击转身")<0){
            prizeSixPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("点击转身")
            //console.log(`Fail URL(点击转身): ${bqcUrlCheck}`)
            prizeSixFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("点击试穿")<0){
            prizeSevenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("点击试穿")
            //console.log(`Fail URL(点击试穿): ${bqcUrlCheck}`)
            prizeSevenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("点击翻身")<0){
            prizeEightPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("点击翻身")
            //console.log(`Fail URL(点击翻身): ${bqcUrlCheck}`)
            prizeEightFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("领奖品")<0){
            prizeNinePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("领奖品")
            //console.log(`Fail URL(领奖品): ${bqcUrlCheck}`)
            prizeNineFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("秒杀")<0){
            prizeTenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("秒杀")
            //console.log(`Fail URL(秒杀): ${bqcUrlCheck}`)
            prizeTenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("抢爆")<0){
            prizeElevenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("抢爆")
            //console.log(`Fail URL(抢爆): ${bqcUrlCheck}`)
            prizeElevenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("在不抢就没啦")<0){
            prizeTwelvePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("在不抢就没啦")
            //console.log(`Fail URL(在不抢就没啦): ${bqcUrlCheck}`)
            prizeTwelveFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("不会再便宜")<0){
            prizeThirteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("不会再便宜")
            //console.log(`Fail URL(不会再便宜): ${bqcUrlCheck}`)
            prizeThirteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("万人疯抢")<0){
            prizeFourteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("万人疯抢")
            //console.log(`Fail URL(万人疯抢): ${bqcUrlCheck}`)
            prizeFourteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("错过就没机会")<0){
            prizeFifteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("过就没机会")
            //console.log(`Fail URL(错过就没机会): ${bqcUrlCheck}`)
            prizeFifteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("全民疯抢")<0){
            prizeSixteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("全民疯抢")
            //console.log(`Fail URL(全民疯抢): ${bqcUrlCheck}`)
            prizeSixteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("全民抢购")<0){
            prizeSeventeenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("全民抢购")
            //console.log(`Fail URL(全民抢购): ${bqcUrlCheck}`)
            prizeSeventeenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("卖疯了")<0){
            prizeEighteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("卖疯了")
            //console.log(`Fail URL(卖疯了): ${bqcUrlCheck}`)
            prizeEighteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("抢疯了")<0){
            prizeNineteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            word.push("抢疯了")
            //console.log(`Fail URL(抢疯了): ${bqcUrlCheck}`)
            prizeNineteenFail.push(bqcUrlCheck)
        }
        urlAll.push(bqcUrlCheck)
        wordAll.push(word)
        console.log(`${bqcUrlCheck}`,word)
    }
    //貼在excel上
    //每個URL各自出現哪些詞
    console.log("All test url Result:")
    for(let i=0; i<urlAll.length ; i++){
        const testUrl = urlAll[i]
        console.log(testUrl)
    }
    console.log("All test word Result:")
    for(let i=0; i<wordAll.length ; i++){
        const testWord = wordAll[i]
        console.log(testWord)
    }
    // console.log("url:",urlAll)
    // console.log("word:",wordAll)
})

Then("'国家级'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '国家级'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",topOnePass.length)
    //console.log("BQC Pass URL-test:",topOnePass)
    console.log("Total of BQC Fail Url-test:",topOneFail.length)
    console.log("BQC Fail Url-test:",topOneFail)
    if(topOneFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${topOneFail} `)
    }
    //expect(topOneFail.length).to.equal(0)
})

Then("'世界级'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '世界级'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",topTwoPass.length)
    //console.log("BQC Pass URL-test:",topTwoPass)
    console.log("Total of BQC Fail Url-test:",topTwoFail.length)
    console.log("BQC Fail Url-test:",topTwoFail)
    if(topTwoFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${topTwoFail} `)
    }
    //expect(topTwoFail.length).to.equal(0)
})

Then("'最高级'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '最高级'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",topThreePass.length)
    // console.log("BQC Pass URL-test:",topThreePass)
    console.log("Total of BQC Fail Url-test:",topThreeFail.length)
    console.log("BQC Fail Url-test:",topThreeFail)
    if(topThreeFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${topThreeFail} `)
    }
    //expect(topThreeFail.length).to.equal(0)
})

Then("'顶级'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '顶级'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",topFourPass.length)
    // console.log("BQC Pass URL-test:",topFourPass)
    console.log("Total of BQC Fail Url-test:",topFourFail.length)
    console.log("BQC Fail Url-test:",topFourFail)
    if(topFourFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${topFourFail} `)
    }
    //expect(topFourFail.length).to.equal(0)
})

Then("'顶尖'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '顶尖'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",topFivePass.length)
    // console.log("BQC Pass URL-test:",topFivePass)
    console.log("Total of BQC Fail Url-test:",topFiveFail.length)
    console.log("BQC Fail Url-test:",topFiveFail)
    if(topFiveFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${topFiveFail} `)
    }
    //expect(topFiveFail.length).to.equal(0)
})

Then("'最高档'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '最高档'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",topSixPass.length)
    // console.log("BQC Pass URL-test:",topSixPass)
    console.log("Total of BQC Fail Url-test:",topSixFail.length)
    console.log("BQC Fail Url-test:",topSixFail)
    if(topSixFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${topSixFail} `)
    }
    //expect(topSixFail.length).to.equal(0)
})

Then("'世界领先'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '世界领先'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",topSevenPass.length)
    // console.log("BQC Pass URL-test:",topSevenPass)
    console.log("Total of BQC Fail Url-test:",topSevenFail.length)
    console.log("BQC Fail Url-test:",topSevenFail)
    if(topSevenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${topSevenFail} `)
    }
    //expect(topSevenFail.length).to.equal(0)
})

Then("'国家示范'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '国家示范'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",topEightPass.length)
    // console.log("BQC Pass URL-test:",topEightPass)
    console.log("Total of BQC Fail Url-test:",topEightFail.length)
    console.log("BQC Fail Url-test:",topEightFail)
    if(topEightFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${topEightFail} `)
    }
    //expect(topEightFail.length).to.equal(0)
})

Then("'全国领先'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '全国领先'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",topNinePass.length)
    // console.log("BQC Pass URL-test:",topNinePass)
    console.log("Total of BQC Fail Url-test:",topNineFail.length)
    console.log("BQC Fail Url-test:",topNineFail)
    if(topNineFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${topNineFail} `)
    }
    //expect(topNineFail.length).to.equal(0)
})

Then("'行业顶尖'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '行业顶尖'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",topTenPass.length)
    // console.log("BQC Pass URL-test:",topTenPass)
    console.log("Total of BQC Fail Url-test:",topTenFail.length)
    console.log("BQC Fail Url-test:",topTenFail)
    if(topTenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${topTenFail} `)
    }
    //expect(topTenFail.length).to.equal(0)
})

Then("'行业领先'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '行业领先'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",topElevenPass.length)
    // console.log("BQC Pass URL-test:",topElevenPass)
    console.log("Total of BQC Fail Url-test:",topElevenFail.length)
    console.log("BQC Fail Url-test:",topElevenFail)
    if(topElevenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${topElevenFail} `)
    }
    //expect(topElevenFail.length).to.equal(0)
})

Then("'领衔'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '领衔'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",topTwelvePass.length)
    // console.log("BQC Pass URL-test:",topTwelvePass)
    console.log("Total of BQC Fail Url-test:",topTwelveFail.length)
    console.log("BQC Fail Url-test:",topTwelveFail)
    if(topTwelveFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${topTwelveFail} `)
    }
    //expect(topTwelveFail.length).to.equal(0)
})

Then("'问鼎'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '问鼎'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",topThirteenPass.length)
    // console.log("BQC Pass URL-test:",topThirteenPass)
    console.log("Total of BQC Fail Url-test:",topThirteenFail.length)
    console.log("BQC Fail Url-test:",topThirteenFail)
    if(topThirteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${topThirteenFail} `)
    }
    //expect(topThirteenFail.length).to.equal(0)
})

Then("'开创之举'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '开创之举'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",topFourteenPass.length)
    // console.log("BQC Pass URL-test:",topFourteenPass)
    console.log("Total of BQC Fail Url-test:",topFourteenFail.length)
    console.log("BQC Fail Url-test:",topFourteenFail)
    if(topFourteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${topFourteenFail} `)
    }
    //expect(topFourteenFail.length).to.equal(0)
})

Then("'第一'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '第一'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",firstOnePass.length)
    // console.log("BQC Pass URL-test:",firstOnePass)
    console.log("Total of BQC Fail Url-test:",firstOneFail.length)
    console.log("BQC Fail Url-test:",firstOneFail)
    if(firstOneFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${firstOneFail} `)
    }
    //expect(firstOneFail.length).to.equal(0)
})

Then("'唯一'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '唯一'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",firstTwoPass.length)
    // console.log("BQC Pass URL-test:",firstTwoPass)
    console.log("Total of BQC Fail Url-test:",firstTwoFail.length)
    console.log("BQC Fail Url-test:",firstTwoFail)
    if(firstTwoFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${firstTwoFail} `)
    }
    //expect(firstTwoFail.length).to.equal(0)
})

Then("'首个'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '首个'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",firstThreePass.length)
    // console.log("BQC Pass URL-test:",firstThreePass)
    console.log("Total of BQC Fail Url-test:",firstThreeFail.length)
    console.log("BQC Fail Url-test:",firstThreeFail)
    if(firstThreeFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${firstThreeFail} `)
    }
    //expect(firstThreeFail.length).to.equal(0)
})

Then("'推荐首选'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '推荐首选'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",firstFourPass.length)
    // console.log("BQC Pass URL-test:",firstFourPass)
    console.log("Total of BQC Fail Url-test:",firstFourFail.length)
    console.log("BQC Fail Url-test:",firstFourFail)
    if(firstFourFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${firstFourFail} `)
    }
    //expect(firstFourFail.length).to.equal(0)
})

Then("'首家'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '首家'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",firstFivePass.length)
    // console.log("BQC Pass URL-test:",firstFivePass)
    console.log("Total of BQC Fail Url-test:",firstFiveFail.length)
    console.log("BQC Fail Url-test:",firstFiveFail)
    if(firstFiveFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${firstFiveFail} `)
    }
    //expect(firstFiveFail.length).to.equal(0)
})

Then("'独家'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '独家'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",firstSixPass.length)
    // console.log("BQC Pass URL-test:",firstSixPass)
    console.log("Total of BQC Fail Url-test:",firstSixFail.length)
    console.log("BQC Fail Url-test:",firstSixFail)
    if(firstSixFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${firstSixFail} `)
    }
    //expect(firstSixFail.length).to.equal(0)
})

Then("'第一人'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '第一人'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",firstSevenPass.length)
    // console.log("BQC Pass URL-test:",firstSevenPass)
    console.log("Total of BQC Fail Url-test:",firstSevenFail.length)
    console.log("BQC Fail Url-test:",firstSevenFail)
    if(firstSevenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${firstSevenFail} `)
    }
    //expect(firstSevenFail.length).to.equal(0)
})

Then("'首席'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '首席'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",firstEightPass.length)
    // console.log("BQC Pass URL-test:",firstEightPass)
    console.log("Total of BQC Fail Url-test:",firstEightFail.length)
    console.log("BQC Fail Url-test:",firstEightFail)
    if(firstEightFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${firstEightFail} `)
    }
    //expect(firstEightFail.length).to.equal(0)
})

Then("'独一无二'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '独一无二'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",firstNinePass.length)
    // console.log("BQC Pass URL-test:",firstNinePass)
    console.log("Total of BQC Fail Url-test:",firstNineFail.length)
    console.log("BQC Fail Url-test:",firstNineFail)
    if(firstNineFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${firstNineFail} `)
    }
    //expect(firstNineFail.length).to.equal(0)
})

Then("'绝无仅有'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '绝无仅有'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",firstTenPass.length)
    // console.log("BQC Pass URL-test:",firstTenPass)
    console.log("Total of BQC Fail Url-test:",firstTenFail.length)
    console.log("BQC Fail Url-test:",firstTenFail)
    if(firstTenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${firstTenFail} `)
    }
    //expect(firstTenFail.length).to.equal(0)
})

Then("'前无古人'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '前无古人'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",firstElevenPass.length)
    // console.log("BQC Pass URL-test:",firstElevenPass)
    console.log("Total of BQC Fail Url-test:",firstElevenFail.length)
    console.log("BQC Fail Url-test:",firstElevenFail)
    if(firstElevenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${firstElevenFail} `)
    }
    //expect(firstElevenFail.length).to.equal(0)
})

Then("'100%'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '100%'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",firstTwelvePass.length)
    // console.log("BQC Pass URL-test:",firstTwelvePass)
    console.log("Total of BQC Fail Url-test:",firstTwelveFail.length)
    console.log("BQC Fail Url-test:",firstTwelveFail)
    if(firstTwelveFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${firstTwelveFail} `)
    }
    //expect(firstTwelveFail.length).to.equal(0)
})

Then("'百分百'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '百分百'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",firstThirteenPass.length)
    // console.log("BQC Pass URL-test:",firstThirteenPass)
    console.log("Total of BQC Fail Url-test:",firstThirteenFail.length)
    console.log("BQC Fail Url-test:",firstThirteenFail)
    if(firstThirteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${firstThirteenFail} `)
    }
    //expect(firstThirteenFail.length).to.equal(0)
})

Then("'史上'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '史上'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",firstFourteenPass.length)
    // console.log("BQC Pass URL-test:",firstFourteenPass)
    console.log("Total of BQC Fail Url-test:",firstFourteenFail.length)
    console.log("BQC Fail Url-test:",firstFourteenFail)
    if(firstFourteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${firstFourteenFail} `)
    }
    //expect(firstFourteenFail.length).to.equal(0)
})

Then("'非常一流'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '非常一流'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",firstFifteenPass.length)
    // console.log("BQC Pass URL-test:",firstFifteenPass)
    console.log("Total of BQC Fail Url-test:",firstFifteenFail.length)
    console.log("BQC Fail Url-test:",firstFifteenFail)
    if(firstFifteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${firstFifteenFail} `)
    }
    //expect(firstFifteenFail.length).to.equal(0)
})

Then("'金牌'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '金牌'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",goldFirstPass.length)
    // console.log("BQC Pass URL-test:",goldFirstPass)
    console.log("Total of BQC Fail Url-test:",goldFirstFail.length)
    console.log("BQC Fail Url-test:",goldFirstFail)
    if(goldFirstFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${goldFirstFail} `)
    }
    //expect(goldFirstFail.length).to.equal(0)
})

Then("'名牌'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '名牌'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",goldTwoPass.length)
    // console.log("BQC Pass URL-test:",goldTwoPass)
    console.log("Total of BQC Fail Url-test:",goldTwoFail.length)
    console.log("BQC Fail Url-test:",goldTwoFail)
    if(goldTwoFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${goldTwoFail} `)
    }
    //expect(goldTwoFail.length).to.equal(0)
})

Then("'王牌'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '王牌'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",goldThreePass.length)
    // console.log("BQC Pass URL-test:",goldThreePass)
    console.log("Total of BQC Fail Url-test:",goldThreeFail.length)
    console.log("BQC Fail Url-test:",goldThreeFail)
    if(goldThreeFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${goldThreeFail} `)
    }
    //expect(goldThreeFail.length).to.equal(0)
})

Then("'销量冠军'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '销量冠军'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",goldFourPass.length)
    // console.log("BQC Pass URL-test:",goldFourPass)
    console.log("Total of BQC Fail Url-test:",goldFourFail.length)
    console.log("BQC Fail Url-test:",goldFourFail)
    if(goldFourFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${goldFourFail} `)
    }
    //expect(goldFourFail.length).to.equal(0)
})

// Then("'第一'不可出現在網頁中",{timeout: 24 * 5000},async function(){
//     console.log("Total of BQC Pass URL-test:",goldFivePass.length)
//     // console.log("BQC Pass URL-test:",goldFivePass)
//     console.log("Total of BQC Fail Url-test:",goldFiveFail.length)
//     console.log("BQC Fail Url-test:",goldFiveFail)
//     const word = '第一'
//     if(goldFiveFail.length > 0 ){
//         throw new Error(` ${word}有出現在以下這些URL: ${goldFiveFail} `)
//     }
//     //expect(goldFiveFail.length).to.equal(0)
// })

Then("'NO.1'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = 'NO.1'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",goldSixPass.length)
    // console.log("BQC Pass URL-test:",goldSixPass)
    console.log("Total of BQC Fail Url-test:",goldSixFail.length)
    console.log("BQC Fail Url-test:",goldSixFail)
    if(goldSixFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${goldSixFail} `)
    }
    //expect(goldSixFail.length).to.equal(0)
})

Then("'Top1'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = 'Top1'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",goldSevenPass.length)
    // console.log("BQC Pass URL-test:",goldSevenPass)
    console.log("Total of BQC Fail Url-test:",goldSevenFail.length)
    console.log("BQC Fail Url-test:",goldSevenFail)
    if(goldSevenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${goldSevenFail} `)
    }
    //expect(goldSevenFail.length).to.equal(0)
})

Then("'领袖品牌'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '领袖品牌'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",goldEightPass.length)
    // console.log("BQC Pass URL-test:",goldEightPass)
    console.log("Total of BQC Fail Url-test:",goldEightFail.length)
    console.log("BQC Fail Url-test:",goldEightFail)
    if(goldEightFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${goldEightFail} `)
    }
    //expect(goldFirstFail.length).to.equal(0)
})

Then("'领导品牌'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '领导品牌'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",goldNinePass.length)
    // console.log("BQC Pass URL-test:",goldNinePass)
    console.log("Total of BQC Fail Url-test:",goldNineFail.length)
    console.log("BQC Fail Url-test:",goldNineFail)
    if(goldNineFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${goldNineFail} `)
    }
    //expect(goldNineFail.length).to.equal(0)
})

// Then("'顶级'不可出現在網頁中",{timeout: 24 * 5000},async function(){
//     console.log("Total of BQC Pass URL-test:",goldTenPass.length)
//     // console.log("BQC Pass URL-test:",goldTenPass)
//     console.log("Total of BQC Fail Url-test:",goldTenFail.length)
//     console.log("BQC Fail Url-test:",goldTenFail)
//     const word = '顶级'
//     if(goldTenFail.length > 0 ){
//         throw new Error(` ${word}有出現在以下這些URL: ${goldTenFail} `)
//     }
//     //expect(goldTenFail.length).to.equal(0)
// })

// Then("'顶尖'不可出現在網頁中",{timeout: 24 * 5000},async function(){
//     console.log("Total of BQC Pass URL-test:",goldElevenPass.length)
//     // console.log("BQC Pass URL-test:",goldElevenPass)
//     console.log("Total of BQC Fail Url-test:",goldElevenFail.length)
//     console.log("BQC Fail Url-test:",goldElevenFail)
//     const word = '顶尖'
//     if(goldElevenFail.length > 0 ){
//         throw new Error(` ${word}有出現在以下這些URL: ${goldElevenFail} `)
//     }
//     //expect(goldElevenFail.length).to.equal(0)
// })

Then("'泰斗'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '泰斗'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",goldTwelvePass.length)
    // console.log("BQC Pass URL-test:",goldTwelvePass)
    console.log("Total of BQC Fail Url-test:",goldTwelveFail.length)
    console.log("BQC Fail Url-test:",goldTwelveFail)
    if(goldTwelveFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${goldTwelveFail} `)
    }
    //expect(goldTwelveFail.length).to.equal(0)
})

Then("'金标'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '金标'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",goldThirteenPass.length)
    // console.log("BQC Pass URL-test:",goldThirteenPass)
    console.log("Total of BQC Fail Url-test:",goldThirteenFail.length)
    console.log("BQC Fail Url-test:",goldThirteenFail)
    if(goldThirteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${goldThirteenFail} `)
    }
    //expect(goldThirteenFail.length).to.equal(0)
})

Then("'驰名尖端'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '驰名尖端'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",goldFourteenPass.length)
    // console.log("BQC Pass URL-test:",goldFourteenPass)
    console.log("Total of BQC Fail Url-test:",goldFourteenFail.length)
    console.log("BQC Fail Url-test:",goldFourteenFail)
    if(goldFourteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${goldFourteenFail} `)
    }
    //expect(goldFourteenFail.length).to.equal(0)
})

Then("'最'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '最'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",mostFirstPass.length)
    // console.log("BQC Pass URL-test:",mostFirstPass)
    console.log("Total of BQC Fail Url-test:",mostFirstFail.length)
    console.log("BQC Fail Url-test:",mostFirstFail)
    if(mostFirstFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostFirstFail} `)
    }
    //expect(mostFirstFail.length).to.equal(0)
})

Then("'终极'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '终极'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",mostTwoPass.length)
    // console.log("BQC Pass URL-test:",mostTwoPass)
    console.log("Total of BQC Fail Url-test:",mostTwoFail.length)
    console.log("BQC Fail Url-test:",mostTwoFail)
    if(mostTwoFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostTwoFail} `)
    }
    //expect(mostTwoFail.length).to.equal(0)
})

Then("'极致'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '极致'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",mostThreePass.length)
    // console.log("BQC Pass URL-test:",mostThreePass)
    console.log("Total of BQC Fail Url-test:",mostThreeFail.length)
    console.log("BQC Fail Url-test:",mostThreeFail)
    if(mostThreeFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostThreeFail} `)
    }
    //expect(mostThreeFail.length).to.equal(0)
})

Then("'万能'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '万能'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",mostFourPass.length)
    // console.log("BQC Pass URL-test:",mostFourPass)
    console.log("Total of BQC Fail Url-test:",mostFourFail.length)
    console.log("BQC Fail Url-test:",mostFourFail)
    if(mostFourFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostFourFail} `)
    }
    //expect(mostFourFail.length).to.equal(0)
})

Then("'决对'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '决对'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",mostFivePass.length)
    // console.log("BQC Pass URL-test:",mostFivePass)
    console.log("Total of BQC Fail Url-test:",mostFiveFail.length)
    console.log("BQC Fail Url-test:",mostFiveFail)
    if(mostFiveFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostFiveFail} `)
    }
    //expect(mostFiveFail.length).to.equal(0)
})

Then("'彻底'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '彻底'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",mostSixPass.length)
    // console.log("BQC Pass URL-test:",mostSixPass)
    console.log("Total of BQC Fail Url-test:",mostSixFail.length)
    console.log("BQC Fail Url-test:",mostSixFail)
    if(mostSixFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostSixFail} `)
    }
    //expect(mostSixFail.length).to.equal(0)
})

Then("'完全'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '完全'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",mostSevenPass.length)
    // console.log("BQC Pass URL-test:",mostSevenPass)
    console.log("Total of BQC Fail Url-test:",mostSevenFail.length)
    console.log("BQC Fail Url-test:",mostSevenFail)
    if(mostSevenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostSevenFail} `)
    }
    //expect(mostSevenFail.length).to.equal(0)
})

Then("'极佳'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '极佳'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",mostEightPass.length)
    // console.log("BQC Pass URL-test:",mostEightPass)
    console.log("Total of BQC Fail Url-test:",mostEightFail.length)
    console.log("BQC Fail Url-test:",mostEightFail)
    if(mostEightFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostEightFail} `)
    }
    //expect(mostEightFail.length).to.equal(0)
})

Then("'全解决'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '全解决'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",mostNinePass.length)
    // console.log("BQC Pass URL-test:",mostNinePass)
    console.log("Total of BQC Fail Url-test:",mostNineFail.length)
    console.log("BQC Fail Url-test:",mostNineFail)
    if(mostNineFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostNineFail} `)
    }
    //expect(mostNineFail.length).to.equal(0)
})

Then("'全方位'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '全方位'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",mostTenPass.length)
    // console.log("BQC Pass URL-test:",mostTenPass)
    console.log("Total of BQC Fail Url-test:",mostTenFail.length)
    console.log("BQC Fail Url-test:",mostTenFail)
    if(mostTenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostTenFail} `)
    }
    //expect(mostTenFail.length).to.equal(0)
})

Then("'全面改善'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '全面改善'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",mostElevenPass.length)
    // console.log("BQC Pass URL-test:",mostElevenPass)
    console.log("Total of BQC Fail Url-test:",mostElevenFail.length)
    console.log("BQC Fail Url-test:",mostElevenFail)
    if(mostElevenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostElevenFail} `)
    }
    //expect(mostElevenFail.length).to.equal(0)
})

Then("'精确'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '精确'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",mostTwelvePass.length)
    // console.log("BQC Pass URL-test:",mostTwelvePass)
    console.log("Total of BQC Fail Url-test:",mostTwelveFail.length)
    console.log("BQC Fail Url-test:",mostTwelveFail)
    if(mostTwelveFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostTwelveFail} `)
    }
    //expect(mostTwelveFail.length).to.equal(0)
})

Then("'准确'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '准确'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",mostThirteenPass.length)
    // console.log("BQC Pass URL-test:",mostThirteenPass)
    console.log("Total of BQC Fail Url-test:",mostThirteenFail.length)
    console.log("BQC Fail Url-test:",mostThirteenFail)
    if(mostThirteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostThirteenFail} `)
    }
    //expect(mostThirteenFail.length).to.equal(0)
})

Then("'精准'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '精准'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",mostFourteenPass.length)
    // console.log("BQC Pass URL-test:",mostFourteenPass)
    console.log("Total of BQC Fail Url-test:",mostFourteenFail.length)
    console.log("BQC Fail Url-test:",mostFourteenFail)
    if(mostFourteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostFourteenFail} `)
    }
    //expect(mostFourteenFail.length).to.equal(0)
})

Then("'优秀'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '优秀'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",mostFifteenPass.length)
    // console.log("BQC Pass URL-test:",mostFifteenPass)
    console.log("Total of BQC Fail Url-test:",mostFifteenFail.length)
    console.log("BQC Fail Url-test:",mostFifteenFail)
    if(mostFifteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostFifteenFail} `)
    }
    //expect(mostFifteenFail.length).to.equal(0)
})

Then("'完美'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '完美'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",mostSixteenPass.length)
    // console.log("BQC Pass URL-test:",mostSixteenPass)
    console.log("Total of BQC Fail Url-test:",mostSixteenFail.length)
    console.log("BQC Fail Url-test:",mostSixteenFail)
    if(mostSixteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostSixteenFail} `)
    }
    //expect(mostSixteenFail.length).to.equal(0)
})

Then("'永久'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '永久'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",mostSeventeenPass.length)
    // console.log("BQC Pass URL-test:",mostSeventeenPass)
    console.log("Total of BQC Fail Url-test:",mostSeventeenFail.length)
    console.log("BQC Fail Url-test:",mostSeventeenFail)
    if(mostSeventeenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostSeventeenFail} `)
    }
    //expect(mostSeventeenFail.length).to.equal(0)
})

Then("'老字号'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '老字号'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",authorityOnePass.length)
    // console.log("BQC Pass URL-test:",authorityOnePass)
    console.log("Total of BQC Fail Url-test:",authorityOneFail.length)
    console.log("BQC Fail Url-test:",authorityOneFail)
    if(authorityOneFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${authorityOneFail} `)
    }
    //expect(authorityOneFail.length).to.equal(0)
})

Then("'中国驰名商标'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '中国驰名商标'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",authorityTwoPass.length)
    // console.log("BQC Pass URL-test:",authorityTwoPass)
    console.log("Total of BQC Fail Url-test:",authorityTwoFail.length)
    console.log("BQC Fail Url-test:",authorityTwoFail)
    if(authorityTwoFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${authorityTwoFail} `)
    }
    //expect(authorityTwoFail.length).to.equal(0)
})

Then("'特供'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '特供'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",authorityThreePass.length)
    // console.log("BQC Pass URL-test:",authorityThreePass)
    console.log("Total of BQC Fail Url-test:",authorityThreeFail.length)
    console.log("BQC Fail Url-test:",authorityThreeFail)
    if(authorityThreeFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${authorityThreeFail} `)
    }
    //expect(authorityThreeFail.length).to.equal(0)
})

Then("'专供'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '专供'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",authorityFourPass.length)
    // console.log("BQC Pass URL-test:",authorityFourPass)
    console.log("Total of BQC Fail Url-test:",authorityFourFail.length)
    console.log("BQC Fail Url-test:",authorityFourFail)
    if(authorityFourFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${authorityFourFail} `)
    }
    //expect(authorityFourFail.length).to.equal(0)
})

Then("'专家推荐'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '专家推荐'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",authorityFivePass.length)
    // console.log("BQC Pass URL-test:",authorityFivePass)
    console.log("Total of BQC Fail Url-test:",authorityFiveFail.length)
    console.log("BQC Fail Url-test:",authorityFiveFail)
    if(authorityFiveFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${authorityFiveFail} `)
    }
    //expect(authorityFiveFail.length).to.equal(0)
})

Then("'国家免检'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '国家免检'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",authoritySixPass.length)
    // console.log("BQC Pass URL-test:",authoritySixPass)
    console.log("Total of BQC Fail Url-test:",authoritySixFail.length)
    console.log("BQC Fail Url-test:",authoritySixFail)
    if(authoritySixFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${authoritySixFail} `)
    }
    //expect(authoritySixFail.length).to.equal(0)
})

Then("'质量免检'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '质量免检'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",authoritySevenPass.length)
    // console.log("BQC Pass URL-test:",authoritySevenPass)
    console.log("Total of BQC Fail Url-test:",authoritySevenFail.length)
    console.log("BQC Fail Url-test:",authoritySevenFail)
    if(authoritySevenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${authoritySevenFail} `)
    }
    //expect(authoritySevenFail.length).to.equal(0)
})

Then("'无需国家质量检测'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '无需国家质量检测'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",authorityEightPass.length)
    // console.log("BQC Pass URL-test:",authorityEightPass)
    console.log("Total of BQC Fail Url-test:",authorityEightFail.length)
    console.log("BQC Fail Url-test:",authorityEightFail)
    if(authorityEightFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${authorityEightFail} `)
    }
    //expect(authorityEightFail.length).to.equal(0)
})

Then("'免抽检'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '免抽检'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",authorityNinePass.length)
    // console.log("BQC Pass URL-test:",authorityNinePass)
    console.log("Total of BQC Fail Url-test:",authorityNineFail.length)
    console.log("BQC Fail Url-test:",authorityNineFail)
    if(authorityNineFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${authorityNineFail} `)
    }
    //expect(authorityNineFail.length).to.equal(0)
})

Then("'点击领奖'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '点击领奖'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",prizeOnePass.length)
    // console.log("BQC Pass URL-test:",prizeOnePass)
    console.log("Total of BQC Fail Url-test:",prizeOneFail.length)
    console.log("BQC Fail Url-test:",prizeOneFail)
    if(prizeOneFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeOneFail} `)
    }
    //expect(prizeOneFail.length).to.equal(0)
})

Then("'恭喜获奖'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '恭喜获奖'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",prizeTwoPass.length)
    // console.log("BQC Pass URL-test:",prizeTwoPass)
    console.log("Total of BQC Fail Url-test:",prizeTwoFail.length)
    console.log("BQC Fail Url-test:",prizeTwoFail)
    if(prizeTwoFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeTwoFail} `)
    }
    //expect(prizeTwoFail.length).to.equal(0)
})

Then("'全民免单'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '全民免单'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",prizeThreePass.length)
    // console.log("BQC Pass URL-test:",prizeThreePass)
    console.log("Total of BQC Fail Url-test:",prizeThreeFail.length)
    console.log("BQC Fail Url-test:",prizeThreeFail)
    if(prizeThreeFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeThreeFail} `)
    }
    //expect(prizeThreeFail.length).to.equal(0)
})

Then("'点击有惊喜'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '点击有惊喜'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",prizeFourPass.length)
    // console.log("BQC Pass URL-test:",prizeFourPass)
    console.log("Total of BQC Fail Url-test:",prizeFourFail.length)
    console.log("BQC Fail Url-test:",prizeFourFail)
    if(prizeFourFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeFourFail} `)
    }
    //expect(prizeFourFail.length).to.equal(0)
})

Then("'点击获取'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '点击获取'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",prizeFivePass.length)
    // console.log("BQC Pass URL-test:",prizeFivePass)
    console.log("Total of BQC Fail Url-test:",prizeFiveFail.length)
    console.log("BQC Fail Url-test:",prizeFiveFail)
    if(prizeFiveFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeFiveFail} `)
    }
    //expect(prizeFiveFail.length).to.equal(0)
})

Then("'点击转身'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '点击转身'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",prizeSixPass.length)
    // console.log("BQC Pass URL-test:",prizeSixPass)
    console.log("Total of BQC Fail Url-test:",prizeSixFail.length)
    console.log("BQC Fail Url-test:",prizeSixFail)
    if(prizeSixFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeSixFail} `)
    }
    //expect(prizeSixFail.length).to.equal(0)
})

Then("'点击试穿'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '点击试穿'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",prizeSevenPass.length)
    // console.log("BQC Pass URL-test:",prizeSevenPass)
    console.log("Total of BQC Fail Url-test:",prizeSevenFail.length)
    console.log("BQC Fail Url-test:",prizeSevenFail)
    if(prizeSevenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeSevenFail} `)
    }
    //expect(prizeSevenFail.length).to.equal(0)
})

Then("'点击翻身'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '点击翻身'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",prizeEightPass.length)
    // console.log("BQC Pass URL-test:",prizeEightPass)
    console.log("Total of BQC Fail Url-test:",prizeEightFail.length)
    console.log("BQC Fail Url-test:",prizeEightFail)
    if(prizeEightFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeEightFail} `)
    }
    //expect(prizeEightFail.length).to.equal(0)
})

Then("'领奖品'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '领奖品'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",prizeNinePass.length)
    // console.log("BQC Pass URL-test:",prizeNinePass)
    console.log("Total of BQC Fail Url-test:",prizeNineFail.length)
    console.log("BQC Fail Url-test:",prizeNineFail)
    if(prizeNineFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeNineFail} `)
    }
    //expect(prizeNineFail.length).to.equal(0)
})

Then("'秒杀'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '秒杀'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",prizeTenPass.length)
    // console.log("BQC Pass URL-test:",prizeTenPass)
    console.log("Total of BQC Fail Url-test:",prizeTenFail.length)
    console.log("BQC Fail Url-test:",prizeTenFail)
    if(prizeTenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeTenFail} `)
    }
    //expect(prizeTenFail.length).to.equal(0)
})

Then("'抢爆'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '抢爆'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",prizeElevenPass.length)
    // console.log("BQC Pass URL-test:",prizeElevenPass)
    console.log("Total of BQC Fail Url-test:",prizeElevenFail.length)
    console.log("BQC Fail Url-test:",prizeElevenFail)
    if(prizeElevenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeElevenFail} `)
    }
    //expect(prizeElevenFail.length).to.equal(0)
})

Then("'在不抢就没啦'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '在不抢就没啦'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",prizeTwelvePass.length)
    // console.log("BQC Pass URL-test:",prizeTwelvePass)
    console.log("Total of BQC Fail Url-test:",prizeTwelveFail.length)
    console.log("BQC Fail Url-test:",prizeTwelveFail)
    if(prizeTwelveFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeTwelveFail} `)
    }
    //expect(prizeTwelveFail.length).to.equal(0)
})

Then("'不会再便宜'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '不会再便宜'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",prizeThirteenPass.length)
    // console.log("BQC Pass URL-test:",prizeThirteenPass)
    console.log("Total of BQC Fail Url-test:",prizeThirteenFail.length)
    console.log("BQC Fail Url-test:",prizeThirteenFail)
    if(prizeThirteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeThirteenFail} `)
    }
    //expect(prizeThirteenFail.length).to.equal(0)
})

Then("'万人疯抢'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '万人疯抢'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",prizeFourteenPass.length)
    // console.log("BQC Pass URL-test:",prizeFourteenPass)
    console.log("Total of BQC Fail Url-test:",prizeFourteenFail.length)
    console.log("BQC Fail Url-test:",prizeFourteenFail)
    if(prizeFourteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeFourteenFail} `)
    }
    //expect(prizeFourteenFail.length).to.equal(0)
})

Then("'错过就没机会'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '错过就没机会'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",prizeFifteenPass.length)
    // console.log("BQC Pass URL-test:",prizeFifteenPass)
    console.log("Total of BQC Fail Url-test:",prizeFifteenFail.length)
    console.log("BQC Fail Url-test:",prizeFifteenFail)
    if(prizeFifteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeFifteenFail} `)
    }
    //expect(prizeFifteenFail.length).to.equal(0)
})

Then("'全民疯抢'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '全民疯抢'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",prizeSixteenPass.length)
    // console.log("BQC Pass URL-test:",prizeSixteenPass)
    console.log("Total of BQC Fail Url-test:",prizeSixteenFail.length)
    console.log("BQC Fail Url-test:",prizeSixteenFail)
    if(prizeSixteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeSixteenFail} `)
    }
    //expect(prizeSixteenFail.length).to.equal(0)
})

Then("'全民抢购'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '全民抢购'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",prizeSeventeenPass.length)
    // console.log("BQC Pass URL-test:",prizeSeventeenPass)
    console.log("Total of BQC Fail Url-test:",prizeSeventeenFail.length)
    console.log("BQC Fail Url-test:",prizeSeventeenFail)
    if(prizeSeventeenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeSeventeenFail} `)
    }
    //expect(prizeSeventeenFail.length).to.equal(0)
})

Then("'卖疯了'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '卖疯了'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",prizeEighteenPass.length)
    // console.log("BQC Pass URL-test:",prizeEighteenPass)
    console.log("Total of BQC Fail Url-test:",prizeEighteenFail.length)
    console.log("BQC Fail Url-test:",prizeEighteenFail)
    if(prizeEighteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeEighteenFail} `)
    }
    //expect(prizeEighteenFail.length).to.equal(0)
})

Then("'抢疯了'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    const word = '抢疯了'
    console.log(word)
    console.log("Total of BQC Pass URL-test:",prizeNineteenPass.length)
    // console.log("BQC Pass URL-test:",prizeNineteenPass)
    console.log("Total of BQC Fail Url-test:",prizeNineteenFail.length)
    console.log("BQC Fail Url-test:",prizeNineteenFail)
    if(prizeNineteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeNineteenFail} `)
    }
    //expect(prizeNineteenFail.length).to.equal(0)
})

// //測試用
// Then("'灵感成真'不可出現在網頁中",{timeout: 24 * 5000},async function(){
//     console.log("Total of BQC Pass URL-test(test):",testPass.length)
//     //console.log("BQC Pass URL-test:",testPass)
//     console.log("Total of BQC Fail Url-test(test):",testFail.length)
//     console.log("BQC Fail Url-test(test):",testFail)
//     const word = '灵感成真'
//     if(testFail.length > 0 ){
//         throw new Error(` ${word}有出現在以下這些URL: ${testFail} `)
//     }
//     //expect(testFail.length).to.equal(0)
// })