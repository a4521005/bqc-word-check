const { Given, When, Then, Before, After } = require('@cucumber/cucumber')
const { expect } = require('chai')
const fs = require('fs');
const puppeteer = require('puppeteer')
const request = require("request-promise");
const cheerio = require("cheerio");
const cicGA="?utm_source=autotest&utm_medium=CIC"

Before({timeout: 24 * 5000},async function () {
    this.browser = await puppeteer.launch({ 
        executablePath:
        "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
        headless:false,//有無需要開視窗,false要開,true不開
        slowMo:100,// slow down by 100ms
        devtools:false//有無需要開啟開發人員工具
    })
    this.page = await this.browser.newPage()
    await this.page.setViewport({width:1200,height:1000})
    await this.page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
    await this.page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
})

After({timeout: 12 * 5000},async function () {
    await this.browser.close()
})

const bqcUrlAll=[]
const bqcUrl = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://g5-prod.benq.com.cn/zh-cn/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            let url = $(element).text()
            // if(url.indexOf("specifications")!==-1 && url.indexOf("wireless-full-hd-kit-wdp01")==-1){
            //     eneuSpecUrlAll.push(url)
            // }
            if(url.indexOf("https://")>0){
                bqcUrlAll.push(url)
            }
    })
    return bqcUrlAll;
    } catch (error) {
      console.log(error);
    }
  };


