const { Given, When, Then, Before, After } = require('@cucumber/cucumber')
const { expect } = require('chai')
const fs = require('fs');
const puppeteer = require('puppeteer')
const request = require("request-promise");
const cheerio = require("cheerio");
const cicGA="?utm_source=autotest&utm_medium=CIC"

const bqcUrlAll = 
[
    "https://g5-prod.benq.com.cn/zh-cn/business/monitor/bl2480t1.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/monitor/bl2780t.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/monitor/sw270c.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/monitor/pd3200u.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/monitor/pd2700q1.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/monitor/pd2700q1/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/monitor/pd2700q1/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/monitor/pd2725U1.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/monitor.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/monitor/pd2725U1/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/monitor/pd2725U1/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/monitor/PD2725U.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/monitor/pd2725U.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/monitor/pd2725U/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/monitor/pd2725U/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/monitor/pd3220u.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/monitor/bl2480t.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/monitor/pd2700q/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/monitor/pd2700q.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/corporate-display.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/software.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/compare.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/st6502.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/compare.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/rp8602.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/rp7502-corporate.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/rp6502.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/cp8601k.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/rm5502k.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/corporate.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/double-sided.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/smart-signage.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/interactive.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/pantone-validated-signage.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/bar-type.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/st6502/retail.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/st6502/corporate.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/il5501.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/il43011.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/st7502.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/st8602.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/pl5502.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/pl4901.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/pl5501.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/sl4302k.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/sl5502k.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/sl6502k.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/sl7502k.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/bh3501t.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/bh2401t.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/bh3801d.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/bh3801n.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/bh2801n.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/bh3801.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/bh3501.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/bh2801.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/bh2401.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/account-management-system.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/re8601.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/re7501.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/re6501.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/x-sign-20.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/x-sign-20/restaurants.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/st8602/retail.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/education.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/specification/monitor-b2bfaq-k-00029.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/how-to-find-the-product-model.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/download-search-result.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/specification/specification.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/software/compare.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/contact-us.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/projector.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/office.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/software.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/office/china/china-shanghai.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/office/asia-pacific/taiwan.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/office/china.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/office/latin-america.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/office/europe.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/office/asia-pacific.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/office/asia-pacific/hong-kong.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/office/headquarters.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/office/america.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/treVolo/AU3500.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/monitor/pd2700u/download.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/monitor/pd2700u.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/cp6501k.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/il5501/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/il43011/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/st8602/corporate.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/st7502/corporate.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/st7502/retail.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/sl8502k.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/monitor/pd2500q.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/monitor/pd2700q/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/treVolo/_.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/explanation/publicdisplay-b2bfaq-k-00006.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00011.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00030.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00015.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/publicdisplay-b2bfaq-k-00120.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-kn-00001.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-kn-00011.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-kn-00006.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-kn-00010.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-kn-00002.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00007.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00016.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00003.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00031.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00012.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/publicdisplay-b2bfaq-k-00117.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/publicdisplay-b2bfaq-k-00126.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/publicdisplay-b2bfaq-k-00013.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-000121.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-kn-00012.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00111.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-kn-00003.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00008.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00017.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00027.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00004.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-kn-00008.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-kn-00009.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00112.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00010.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00005.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00018.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00014.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00009.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/ifp/re8601/download.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/ifp/rp860k-corporate/download.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/ifp/rp8602/download.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/ifp/rm8602k/download.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/ifp/re7501/download.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/ifp/re6501/download.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/ifp/rp704k-corporate/download.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/ifp/cp6501k/download.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/ifp/rp6502/download.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/ifp/rp7502/download.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/ifp/qtouch/download.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/ifp/partner-software/download.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/ifp/display-management/download.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/ifp/rp7501k-corporate/download.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/ifp/rp6501k-corporate/download.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/ifp/ezwrite-60.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/ifp/rm7502k/download.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/ifp/rm6502k/download.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/ifp/rm5502k/download.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/ifp/cp8601k/download.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/specification/publicdisplay-b2bfaq-k-00119.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/specification/publicdisplay-b2bfaq-k-00118.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/specification/publicdisplay-b2bfaq-k-00049.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/specification/publicdisplay-b2bfaq-k-00048.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/specification/publicdisplay-b2bfaq-k-00047.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/specification/publicdisplay-b2bfaq-k-00046.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/publicdisplay-b2bfaq-k-00001.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/publicdisplay-b2bfaq-k-00063.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/publicdisplay-b2bfaq-k-00061.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/publicdisplay-b2bfaq-k-00053.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/projector-b2bfaq-k-00116.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/projector-b2bfaq-k-00110.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/projector-b2bfaq-k-00097.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/projector-b2bfaq-k-00084.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/projector-b2bfaq-k-00082.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/projector-b2bfaq-k-00078.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/projector-b2bfaq-k-00081.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/projector-b2bfaq-k-00080.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/projector-b2bfaq-k-00051.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/explanation/monitor-faq-kn-00003.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/explanation/monitor-faq-kn-00002.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/explanation/monitor-b2bfaq-k-00068.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/explanation/monitor-b2bfaq-k-00056.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/projector-b2bfaq-k-00185.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/projector-b2bfaq-k-00177.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/projector-b2bfaq-k-00176.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/publicdisplay-b2bfaq-k-00110.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/publicdisplay-b2bfaq-k-00109.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/projector-b2bfaq-k-00168.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/projector-b2bfaq-k-00162.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/projector-b2bfaq-k-00161.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/projector-b2bfaq-k-00159.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/projector-b2bfaq-k-00151.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/projector-b2bfaq-k-00054.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/publicdisplay-b2bfaq-k-00108.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/publicdisplay-b2bfaq-k-00107.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/monitor-b2bfaq-k-00014.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/monitor-b2bfaq-k-00004.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/publicdisplay-b2bfaq-k-00002.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/publicdisplay-b2bfaq-k-00039.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/publicdisplay-b2bfaq-k-00066.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/publicdisplay-b2bfaq-k-00062.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/publicdisplay-b2bfaq-k-00064.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/publicdisplay-b2bfaq-k-00028.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/projector-b2bfaq-k-00115.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00129.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/monitor-b2bfaq-k-00070.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/monitor-b2bfaq-k-00071.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/projector-b2bfaq-k-00184.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/projector-b2bfaq-k-00183.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/projector-b2bfaq-k-00182.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00125.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00122.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00121.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00124.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00123.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00114.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00113.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/monitor-b2bfaq-k-00052.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/monitor-b2bfaq-k-00051.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/monitor-b2bfaq-k-00050.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/projector-b2bfaq-k-00169.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/projector-b2bfaq-k-00152.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/projector-b2bfaq-k-00150.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00106.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00102.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00103.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00098.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00097.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00096.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00075.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00074.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00084.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00080.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00073.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00059.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00057.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00055.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/projector-b2bfaq-k-00112.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00101.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00100.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00099.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00095.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00094.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00078.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00093.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00092.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00090.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00089.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00088.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00087.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00086.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00085.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00083.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00082.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00081.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00079.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00077.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00076.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00070.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00068.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00067.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00060.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00058.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00056.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00054.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00052.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00050.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00045.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00040.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00044.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00038.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00037.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00036.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00025.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00026.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00033.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00034.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00035.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00024.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00023.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00022.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00021.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00020.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00019.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00105.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00104.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00091.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00069.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00041.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00032.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00051.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00072.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00043.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/publicdisplay-b2bfaq-k-00042.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/projector-b2bfaq-k-00058.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/sl4302k/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/treVolo.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/signage/dvy32/download.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/signage/dvy32/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/signage/dvy32/video.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/signage/dvy31/qa_msm_moved.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/signage/dvy31/download_msm_moved.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/signage/dvy31/video_msm_moved.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/signage/dvy31/video.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/signage/dvy31/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/signage/dvy31/download.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/projector/lk936st.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/projector/lk936st/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/projector/lk936st/download.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/projector/lk936st/video.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/projector/lk936st1.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/projector-b2bfaq-k-00003.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/projector-b2bfaq-k-00002.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/signage/st4301k-refurbished/video.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/signage/st4301k-refurbished/download.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/signage/st4301k-refurbished/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/signage/st4301k-refurbished.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/signage/dey21/video.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/signage/dey21/download.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/signage/dey21/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/signage.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/ifp/tty21/video.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/ifp/tty21/download.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/ifp/tty21/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/ifp/tmy21/video.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/ifp/tmy21/download.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/ifp/tmy21/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/ifp/ie1004/video.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/ifp/ie1004/download.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/ifp/ie1004/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/products/ifp.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/publicdisplay-b2bfaq-k-00065.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/projector-faq-kn-000601.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/projector-b2bfaq-k-00107.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/temperature-screening-solution.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/sl8502k/corporate0.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/display-management1/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/display-management1.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/ezwrite-6/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/ezwrite-6.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/ezwrite-6/features.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/ezwrite-6/education.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/ezwrite-6/corporate.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/ezwrite-6/function.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-templates/service.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-templates/retail.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-templates/education.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-templates/corporate.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-templates/_restaurant.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-templates/home.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-templates.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign_openapi/documentation/models/model_datalink.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign_openapi/documentation/models/model_schedule_event.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign_openapi/documentation/models/model_schedule.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign_openapi/documentation/models/model_channel.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign_openapi/documentation/models/model_equipment.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign_openapi/documentation/models/model_mda_schedule_screen.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign_openapi/documentation/models/model_archive.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign_openapi/documentation/models/model_user.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign_openapi/documentation/models.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign_openapi/documentation/general_status_code.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign_openapi/documentation/apis/datalink.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign_openapi/documentation/apis/mda.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign_openapi/documentation/apis/schedule.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign_openapi/documentation/apis/channel.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign_openapi/documentation/apis/equipment.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign_openapi/documentation/apis/archive.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign_openapi/documentation/apis/user.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign_openapi/documentation/apis.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign_openapi/documentation/prerequisite.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign_openapi/documentation.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign_openapi/authentication.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign_openapi/getting_started.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign_openapi.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/release-version.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/user-manual-search-result.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/gpl-lgpl.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/api-premium/creating-an-api-template-using-the-designed-template.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/api-premium.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/interactive-premium/setting-the-device-group-control.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/interactive-premium/viewing-an-interactive-report.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/interactive-premium/enabling-interactive-statistics.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/interactive-premium/viewing-the-structure-of-an-interactive-template.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/interactive-premium/creating-interactive-content-from-designed-templates-with-area-link.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/interactive-premium/creating-interactive-content-with-interactive-template.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/interactive-premium.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/video-wall/fine-tuning-the-video-wall-from-x-sign-designer.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/video-wall/creating-the-video-wall-content-from-x-sign-designer.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/video-wall/setting-up-the-environment-for-video-wall.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/video-wall.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/report/downloading-player-content-report.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/report/getting-the-activity-report-of-x-sign-manager.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/report.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/local-delivery/setting-a-schedule-for-local-delivery-mode.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/local-delivery/searching-for-devices-in-local-delivery-mode.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/local-delivery/playing-content-with-local-delivery.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/local-delivery.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/player/usb-autoplay.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/player/changing-picture-settings-in-x-sign-player-sl4302k-l5502k-sl6502k-sl7502k-sl8502k.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/player/playing-sealed-files-on-x-sign-player.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/player/activating-an-interactive-function-in-standalone-mode.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/player/playing-files-with-usb.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/player/switching-the-mode-of-player.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/player.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/designer/changing-your-language-in-x-sign-designer.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/designer/opening-an-x-sign-1x-project-file.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/designer/sealing-a-file-in-x-sign-designer.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/designer/changing-the-playing-order-of-pages.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/designer/setting-background-music-in-x-sign-designer.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/designer/creating-a-tv-input-source-widget-in-x-sign-designer.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/designer/creating-an-api-template-using-the-demo-kit.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/designer/setting-a-timer-widget.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/designer/adding-a-data-connection.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/designer/generating-a-qr-code.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/designer/rotating-an-image-or-video.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/designer/creating-an-image-or-video-slideshow.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/designer/setting-a-weather-widget.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/designer/creating-a-clock-widget-in-x-sign-designer.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/designer/creating-content-from-designed-templates.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/designer/creating-content-in-x-sign-designer.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/designer/uploading-your-content-from-designer.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/designer/creating-a-new-project-file-in-x-sign-designer.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/designer.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/manager/using-designer-express.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/manager/player-report-proof-of-play.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/manager/updating-device-status-immediately.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/manager/getting-the-system-notification.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/manager/setting-verification-06.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/manager/creating-an-api-table-in-x-sign-manager.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/manager/managing-the-calling-system.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/manager/updating-device-player-remotely.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/manager/controlling-the-device-remotely.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/manager/pushing-real-time-message-to-your-benq-smart-signage-remotely.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/manager/editing-the-device-management-structure.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/manager/scheduling-the-content-by-device.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/manager/scheduling-the-content-by-channel.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/manager/creating-the-channel.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/manager/pairing-the-device-with-x-sign-manager.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/manager.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/licence/checking-the-license-upgrade-notice.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/licence/mapping-the-license.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/licence/importing-the-licenses-to-x-sign-manager-02.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/licence/purchasing-the-license-from-x-sign-manager.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/licence.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/account/authorizing-other-users-as-admins.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/account/setting-the-permission-for-users.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/account/accessing-your-account.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/account/registering-an-account-for-x-sign-manager-admin.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260/account.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/x-sign-260.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/quick-guide.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual/x-sign-20/release-information.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/user-manual.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/instashare-2/education.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/instashare-2.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/instashare-2/features.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/instashare-2/corporate.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/ezwrite-60.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/ezwrite-60/privacy-policy.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/ezwrite-60/features.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/campaign/smart-video-conferencing-bundle/registration-form.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/campaign/smart-video-conferencing-bundle/form.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/campaign/smart-video-conferencing-bundle/thank-you.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/campaign/smart-video-conferencing-bundle.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/campaign/edtech-calculator.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/campaign/distance-learning.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/campaign/corporate-solutions-webinars.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/campaign/corporate-signage-bundle/registration-form.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/campaign/corporate-signage-bundle/form.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/campaign/corporate-signage-bundle/thank-you.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/campaign/corporate-signage-bundle.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/campaign/collaboration-meeting-room-solution/e-brochure.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/campaign/collaboration-meeting-room-solution/resources.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/campaign/classroom-technology-solution-revamp/tap-n-teach-draft.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/campaign/classroom-technology-solution-revamp/tap-n-teach.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/campaign/classroom-technology-solution-revamp/new-banners.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/campaign/classroom-technology-solution-revamp/blog.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/campaign/classroom-technology-solution-revamp/contact-us.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/campaign/classroom-technology-solution-revamp/download.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/campaign/classroom-technology-solution-revamp/product.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/campaign/classroom-technology-solution-revamp/contact-us-test.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/campaign/classroom-technology-solution-revamp.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/campaign/benqboard-the-most-highly-decorated-interactive-touch-display.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/troubleshooting/projector-faq-kn-00029.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/specification/monitor-faq-kn-00005.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/projector-b2bfaq-k-00091.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/monitor-b2bfaq-k-00047.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/support/faqs/application/projector-faq-kn-00060.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/projector/lh710.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/monitor/bl2480t1/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/monitor/bl2480t1/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/projector/lx770.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/projector/mh7501/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/projector/mh7501/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/projector/lh770.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/projector/qcast-app.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/software/g-suite.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/x-sign-20/corporate.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/x-sign-20/login.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/x-sign-20/function.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/partner-software.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/stretch.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/pl4901/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/pl4901/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/pl5501/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/pl5501/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/pl5502/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/pl5502/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/accessory.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/st8602/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/st8602/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/st7502/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/st7502/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/bh3501t/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/bh3501t/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/bh2401t/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/bh2401t/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/st6502/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/st6502/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/bh3801n/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/bh3801n/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/bh3801/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/bh3801/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/bh3801d/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/bh3801d/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/bh2801n/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/bh2801n/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/stretch-display-series.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/device-management-solution/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/device-management-solution.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/sl4302k/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/sl5502k/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/sl5502k/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/sl6502k/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/sl6502k/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/sl7502k/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/sl7502k/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/sl8502k/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/sl8502k/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/bh2401/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/bh2401/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/bh3501/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/bh3501/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/bh2801/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/vr-demo-kit-app/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/vr-demo-kit-app/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/vr-demo-kit-app.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/display-management/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/display-management/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/signage/display-management.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/re8601/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/re7501/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/re6501/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/instashare2-old/function.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/instashare2-old.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/ezwrite-5/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/ezwrite-5/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/ezwrite-5.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/partner-software.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/rp7502-corporate/specifications.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/rp7502-corporate/qa.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/webinars.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/accessory.html",
    "https://g5-prod.benq.com.cn/zh-cn/business/ifp/software.html"
]

//測試用
// const testPass=[]
// const testFail=[]

const topOnePass=[]
const topOneFail=[]

const topTwoPass=[]
const topTwoFail=[]

const topThreePass=[]
const topThreeFail=[]

const topFourPass=[]
const topFourFail=[]

const topFivePass=[]
const topFiveFail=[]

const topSixPass=[]
const topSixFail=[]

const topSevenPass=[]
const topSevenFail=[]

const topEightPass=[]
const topEightFail=[]

const topNinePass=[]
const topNineFail=[]

const topTenPass=[]
const topTenFail=[]

const topElevenPass=[]
const topElevenFail=[]

const topTwelvePass=[]
const topTwelveFail=[]

const topThirteenPass=[]
const topThirteenFail=[]

const topFourteenPass=[]
const topFourteenFail=[]

const firstOnePass=[]
const firstOneFail=[]

const firstTwoPass=[]
const firstTwoFail=[]

const firstThreePass=[]
const firstThreeFail=[]

const firstFourPass=[]
const firstFourFail=[]

const firstFivePass=[]
const firstFiveFail=[]

const firstSixPass=[]
const firstSixFail=[]

const firstSevenPass=[]
const firstSevenFail=[]

const firstEightPass=[]
const firstEightFail=[]

const firstNinePass=[]
const firstNineFail=[]

const firstTenPass=[]
const firstTenFail=[]

const firstElevenPass=[]
const firstElevenFail=[]

const firstTwelvePass=[]
const firstTwelveFail=[]

const firstThirteenPass=[]
const firstThirteenFail=[]

const firstFourteenPass=[]
const firstFourteenFail=[]

const firstFifteenPass=[]
const firstFifteenFail=[]

const goldFirstPass=[]
const goldFirstFail=[]

const goldTwoPass=[]
const goldTwoFail=[]

const goldThreePass=[]
const goldThreeFail=[]

const goldFourPass=[]
const goldFourFail=[]

// const goldFivePass=[]
// const goldFiveFail=[]

const goldSixPass=[]
const goldSixFail=[]

const goldSevenPass=[]
const goldSevenFail=[]

const goldEightPass=[]
const goldEightFail=[]

const goldNinePass=[]
const goldNineFail=[]

// const goldTenPass=[]
// const goldTenFail=[]

// const goldElevenPass=[]
// const goldElevenFail=[]

const goldTwelvePass=[]
const goldTwelveFail=[]

const goldThirteenPass=[]
const goldThirteenFail=[]

const goldFourteenPass=[]
const goldFourteenFail=[]

const mostFirstPass=[]
const mostFirstFail=[]

const mostTwoPass=[]
const mostTwoFail=[]

const mostThreePass=[]
const mostThreeFail=[]

const mostFourPass=[]
const mostFourFail=[]

const mostFivePass=[]
const mostFiveFail=[]

const mostSixPass=[]
const mostSixFail=[]

const mostSevenPass=[]
const mostSevenFail=[]

const mostEightPass=[]
const mostEightFail=[]

const mostNinePass=[]
const mostNineFail=[]

const mostTenPass=[]
const mostTenFail=[]

const mostElevenPass=[]
const mostElevenFail=[]

const mostTwelvePass=[]
const mostTwelveFail=[]

const mostThirteenPass=[]
const mostThirteenFail=[]

const mostFourteenPass=[]
const mostFourteenFail=[]

const mostFifteenPass=[]
const mostFifteenFail=[]

const mostSixteenPass=[]
const mostSixteenFail=[]

const mostSeventeenPass=[]
const mostSeventeenFail=[]

const authorityOnePass=[]
const authorityOneFail=[]

const authorityTwoPass=[]
const authorityTwoFail=[]

const authorityThreePass=[]
const authorityThreeFail=[]

const authorityFourPass=[]
const authorityFourFail=[]

const authorityFivePass=[]
const authorityFiveFail=[]

const authoritySixPass=[]
const authoritySixFail=[]

const authoritySevenPass=[]
const authoritySevenFail=[]

const authorityEightPass=[]
const authorityEightFail=[]

const authorityNinePass=[]
const authorityNineFail=[]

const prizeOnePass=[]
const prizeOneFail=[]

const prizeTwoPass=[]
const prizeTwoFail=[]

const prizeThreePass=[]
const prizeThreeFail=[]

const prizeFourPass=[]
const prizeFourFail=[]

const prizeFivePass=[]
const prizeFiveFail=[]

const prizeSixPass=[]
const prizeSixFail=[]

const prizeSevenPass=[]
const prizeSevenFail=[]

const prizeEightPass=[]
const prizeEightFail=[]

const prizeNinePass=[]
const prizeNineFail=[]

const prizeTenPass=[]
const prizeTenFail=[]

const prizeElevenPass=[]
const prizeElevenFail=[]

const prizeTwelvePass=[]
const prizeTwelveFail=[]

const prizeThirteenPass=[]
const prizeThirteenFail=[]

const prizeFourteenPass=[]
const prizeFourteenFail=[]

const prizeFifteenPass=[]
const prizeFifteenFail=[]

const prizeSixteenPass=[]
const prizeSixteenFail=[]

const prizeSeventeenPass=[]
const prizeSeventeenFail=[]

const prizeEighteenPass=[]
const prizeEighteenFail=[]

const prizeNineteenPass=[]
const prizeNineteenFail=[]


Before({timeout: 24 * 5000},async function () {
    this.browser = await puppeteer.launch({ 
        executablePath:
        "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
        headless:true,//有無需要開視窗,false要開,true不開
        slowMo:100,// slow down by 100ms
        devtools:false//有無需要開啟開發人員工具
    })
    this.page = await this.browser.newPage()
    await this.page.setViewport({width:1200,height:1000})
    await this.page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
    await this.page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
})

After({timeout: 12 * 5000},async function () {
    await this.browser.close()
})

Given("check all URL",{timeout: 120000 * 5000},async function(){
    for(let i =0; i<10; i++){
        //測試用
        // await this.page.goto("https://g5-prod.benq.com.cn/zh-cn/business/monitor/pd2700q1/specifications.html" +cicGA)
        // const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // console.log("灵感成真index",innerHtml.indexOf("灵感成真"))

        //bqcUrlAll.length所有URL數量
        const bqcUrlCheck = bqcUrlAll[i]
        await this.page.goto(bqcUrlCheck +cicGA)
        await this.page.waitForSelector('html')
        // const innerHtml = await page.$eval('body', element => element.innerHTML);
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // console.log(bqcUrlCheck,"灵感成真index",innerHtml.indexOf("灵感成真"))

        //測試用
        // if(innerHtml.indexOf("灵感成真")<0){
        //     testPass.push("https://g5-prod.benq.com.cn/zh-cn/business/monitor/pd2700q1/specifications.html")
        //     //console.log(`${bqcUrlCheck} is pass`)
        // }else{
        //     console.log(`Fail URL:"https://g5-prod.benq.com.cn/zh-cn/business/monitor/pd2700q1/specifications.html"`)
        //     testFail.push("https://g5-prod.benq.com.cn/zh-cn/business/monitor/pd2700q1/specifications.html")
        // }  
        //測試用
        // if(innerHtml.indexOf("灵感成真")<0){
        //     testPass.push(bqcUrlCheck)
        //     //console.log(`${bqcUrlCheck} is pass`)
        // }else{
        //     console.log(`Fail URL: ${bqcUrlCheck}`)
        //     testFail.push(bqcUrlCheck)
        // }        
        if(innerHtml.indexOf("国家级")<0){
            topOnePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(国家级): ${bqcUrlCheck}`)
            topOneFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("世界级")<0){
            topTwoPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(世界级): ${bqcUrlCheck}`)
            topTwoFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("最高级")<0){
            topThreePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(最高级): ${bqcUrlCheck}`)
            topThreeFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("顶级")<0){
            topFourPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(顶级): ${bqcUrlCheck}`)
            topFourFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("顶尖")<0){
            topFivePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(顶尖): ${bqcUrlCheck}`)
            topFiveFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("最高档")<0){
            topSixPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(最高档): ${bqcUrlCheck}`)
            topSixFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("世界领先")<0){
            topSevenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(世界领先): ${bqcUrlCheck}`)
            topSevenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("国家示范")<0){
            topEightPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(国家示范): ${bqcUrlCheck}`)
            topEightFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("全国领先")<0){
            topNinePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(全国领先): ${bqcUrlCheck}`)
            topNineFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("行业顶尖")<0){
            topTenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(行业顶尖): ${bqcUrlCheck}`)
            topTenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("行业领先")<0){
            topElevenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(行业领先): ${bqcUrlCheck}`)
            topElevenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("领衔")<0){
            topTwelvePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(领衔): ${bqcUrlCheck}`)
            topTwelveFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("问鼎")<0){
            topThirteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(问鼎): ${bqcUrlCheck}`)
            topThirteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("开创之举")<0){
            topFourteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(开创之举): ${bqcUrlCheck}`)
            topFourteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("第一")<0){
            firstOnePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(第一): ${bqcUrlCheck}`)
            firstOneFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("唯一")<0){
            firstTwoPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(唯一): ${bqcUrlCheck}`)
            firstTwoFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("首个")<0){
            firstThreePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(首个): ${bqcUrlCheck}`)
            firstThreeFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("推荐首选")<0){
            firstFourPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(推荐首选): ${bqcUrlCheck}`)
            firstFourFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("首家")<0){
            firstFivePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(首家): ${bqcUrlCheck}`)
            firstFiveFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("独家")<0){
            firstSixPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(独家): ${bqcUrlCheck}`)
            firstSixFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("第一人")<0){
            firstSevenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(第一人): ${bqcUrlCheck}`)
            firstSevenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("首席")<0){
            firstEightPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(首席): ${bqcUrlCheck}`)
            firstEightFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("独一无二")<0){
            firstNinePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(独一无二): ${bqcUrlCheck}`)
            firstNineFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("绝无仅有")<0){
            firstTenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(绝无仅有): ${bqcUrlCheck}`)
            firstTenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("前无古人")<0){
            firstElevenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(前无古人): ${bqcUrlCheck}`)
            firstElevenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("100%")<0){
            firstTwelvePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(100%): ${bqcUrlCheck}`)
            firstTwelveFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("百分百")<0){
            firstThirteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(百分百): ${bqcUrlCheck}`)
            firstThirteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("史上")<0){
            firstFourteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(史上): ${bqcUrlCheck}`)
            firstFourteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("非常一流")<0){
            firstFifteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(非常一流): ${bqcUrlCheck}`)
            firstFifteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("金牌")<0){
            goldFirstPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(金牌): ${bqcUrlCheck}`)
            goldFirstFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("名牌")<0){
            goldTwoPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(名牌): ${bqcUrlCheck}`)
            goldTwoFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("王牌")<0){
            goldThreePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(王牌): ${bqcUrlCheck}`)
            goldThreeFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("销量冠军")<0){
            goldFourPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(销量冠军): ${bqcUrlCheck}`)
            goldFourFail.push(bqcUrlCheck)
        }
        // if(innerHtml.indexOf("第一")<0){
        //     goldFivePass.push(bqcUrlCheck)
        //     //console.log(`${bqcUrlCheck} is pass`)
        // }else{
        //     console.log(`Fail URL(第一): ${bqcUrlCheck}`)
        //     goldFiveFail.push(bqcUrlCheck)
        // }
        if(innerHtml.indexOf("NO.1")<0){
            goldSixPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(NO.1): ${bqcUrlCheck}`)
            goldSixFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("Top1")<0){
            goldSevenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(Top1): ${bqcUrlCheck}`)
            goldSevenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("领袖品牌")<0){
            goldEightPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(领袖品牌): ${bqcUrlCheck}`)
            goldEightFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("领导品牌")<0){
            goldNinePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(领导品牌): ${bqcUrlCheck}`)
            goldNineFail.push(bqcUrlCheck)
        }
        // if(innerHtml.indexOf("顶级")<0){
        //     goldTenPass.push(bqcUrlCheck)
        //     //console.log(`${bqcUrlCheck} is pass`)
        // }else{
        //     console.log(`Fail URL(顶级): ${bqcUrlCheck}`)
        //     goldTenFail.push(bqcUrlCheck)
        // }
        // if(innerHtml.indexOf("顶尖")<0){
        //     goldElevenPass.push(bqcUrlCheck)
        //     //console.log(`${bqcUrlCheck} is pass`)
        // }else{
        //     console.log(`Fail URL(顶尖): ${bqcUrlCheck}`)
        //     goldElevenFail.push(bqcUrlCheck)
        // }
        if(innerHtml.indexOf("泰斗")<0){
            goldTwelvePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(泰斗): ${bqcUrlCheck}`)
            goldTwelveFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("金标")<0){
            goldThirteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(金标): ${bqcUrlCheck}`)
            goldThirteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("驰名尖端")<0){
            goldFourteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(驰名尖端): ${bqcUrlCheck}`)
            goldFourteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("最")<0){
            mostFirstPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(最): ${bqcUrlCheck}`)
            mostFirstFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("终极")<0){
            mostTwoPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(终极): ${bqcUrlCheck}`)
            mostTwoFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("极致")<0){
            mostThreePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(极致): ${bqcUrlCheck}`)
            mostThreeFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("万能")<0){
            mostFourPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(万能): ${bqcUrlCheck}`)
            mostFourFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("决对")<0){
            mostFivePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(决对): ${bqcUrlCheck}`)
            mostFiveFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("彻底")<0){
            mostSixPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(彻底): ${bqcUrlCheck}`)
            mostSixFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("完全")<0){
            mostSevenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(完全): ${bqcUrlCheck}`)
            mostSevenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("极佳")<0){
            mostEightPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(极佳): ${bqcUrlCheck}`)
            mostEightFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("全解决")<0){
            mostNinePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(全解决): ${bqcUrlCheck}`)
            mostNineFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("全方位")<0){
            mostTenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(全方位): ${bqcUrlCheck}`)
            mostTenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("全面改善")<0){
            mostElevenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(全面改善): ${bqcUrlCheck}`)
            mostElevenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("精确")<0){
            mostTwelvePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(精确): ${bqcUrlCheck}`)
            mostTwelveFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("准确")<0){
            mostThirteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(准确): ${bqcUrlCheck}`)
            mostThirteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("精准")<0){
            mostFourteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(精准): ${bqcUrlCheck}`)
            mostFourteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("优秀")<0){
            mostFifteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(优秀): ${bqcUrlCheck}`)
            mostFifteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("完美")<0){
            mostSixteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(完美): ${bqcUrlCheck}`)
            mostSixteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("永久")<0){
            mostSeventeenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(永久): ${bqcUrlCheck}`)
            mostSeventeenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("老字号")<0){
            authorityOnePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(老字号): ${bqcUrlCheck}`)
            authorityOneFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("中国驰名商标")<0){
            authorityTwoPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(中国驰名商标): ${bqcUrlCheck}`)
            authorityTwoFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("特供")<0){
            authorityThreePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(特供): ${bqcUrlCheck}`)
            authorityThreeFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("专供")<0){
            authorityFourPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(专供): ${bqcUrlCheck}`)
            authorityFourFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("专家推荐")<0){
            authorityFivePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(专家推荐): ${bqcUrlCheck}`)
            authorityFiveFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("国家免检")<0){
            authoritySixPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(国家免检): ${bqcUrlCheck}`)
            authoritySixFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("质量免检")<0){
            authoritySevenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(质量免检): ${bqcUrlCheck}`)
            authoritySevenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("无需国家质量检测")<0){
            authorityEightPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(无需国家质量检测): ${bqcUrlCheck}`)
            authorityEightFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("免抽检")<0){
            authorityNinePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(免抽检): ${bqcUrlCheck}`)
            authorityNineFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("点击领奖")<0){
            prizeOnePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(点击领奖): ${bqcUrlCheck}`)
            prizeOneFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("恭喜获奖")<0){
            prizeTwoPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(恭喜获奖): ${bqcUrlCheck}`)
            prizeTwoFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("全民免单")<0){
            prizeThreePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(全民免单): ${bqcUrlCheck}`)
            prizeThreeFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("点击有惊喜")<0){
            prizeFourPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(点击有惊喜): ${bqcUrlCheck}`)
            prizeFourFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("点击获取")<0){
            prizeFivePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(点击获取): ${bqcUrlCheck}`)
            prizeFiveFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("点击转身")<0){
            prizeSixPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(点击转身): ${bqcUrlCheck}`)
            prizeSixFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("点击试穿")<0){
            prizeSevenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(点击试穿): ${bqcUrlCheck}`)
            prizeSevenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("点击翻身")<0){
            prizeEightPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(点击翻身): ${bqcUrlCheck}`)
            prizeEightFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("领奖品")<0){
            prizeNinePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(领奖品): ${bqcUrlCheck}`)
            prizeNineFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("秒杀")<0){
            prizeTenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(秒杀): ${bqcUrlCheck}`)
            prizeTenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("抢爆")<0){
            prizeElevenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(抢爆): ${bqcUrlCheck}`)
            prizeElevenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("在不抢就没啦")<0){
            prizeTwelvePass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(在不抢就没啦): ${bqcUrlCheck}`)
            prizeTwelveFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("不会再便宜")<0){
            prizeThirteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(不会再便宜): ${bqcUrlCheck}`)
            prizeThirteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("万人疯抢")<0){
            prizeFourteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(万人疯抢): ${bqcUrlCheck}`)
            prizeFourteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("错过就没机会")<0){
            prizeFifteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(错过就没机会): ${bqcUrlCheck}`)
            prizeFifteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("全民疯抢")<0){
            prizeSixteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(全民疯抢): ${bqcUrlCheck}`)
            prizeSixteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("全民抢购")<0){
            prizeSeventeenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(全民抢购): ${bqcUrlCheck}`)
            prizeSeventeenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("卖疯了")<0){
            prizeEighteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(卖疯了): ${bqcUrlCheck}`)
            prizeEighteenFail.push(bqcUrlCheck)
        }
        if(innerHtml.indexOf("抢疯了")<0){
            prizeNineteenPass.push(bqcUrlCheck)
            //console.log(`${bqcUrlCheck} is pass`)
        }else{
            console.log(`Fail URL(抢疯了): ${bqcUrlCheck}`)
            prizeNineteenFail.push(bqcUrlCheck)
        }
    }
})

Then("'国家级'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",topOnePass.length)
    //console.log("BQC Pass URL-test:",topOnePass)
    console.log("Total of BQC Fail Url-test:",topOneFail.length)
    console.log("BQC Fail Url-test:",topOneFail)
    const word = '国家级'
    if(topOneFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${topOneFail} `)
    }
    //expect(topOneFail.length).to.equal(0)
})

Then("'世界级'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",topTwoPass.length)
    //console.log("BQC Pass URL-test:",topTwoPass)
    console.log("Total of BQC Fail Url-test:",topTwoFail.length)
    console.log("BQC Fail Url-test:",topTwoFail)
    const word = '世界级'
    if(topTwoFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${topTwoFail} `)
    }
    //expect(topTwoFail.length).to.equal(0)
})

Then("'最高级'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",topThreePass.length)
    // console.log("BQC Pass URL-test:",topThreePass)
    console.log("Total of BQC Fail Url-test:",topThreeFail.length)
    console.log("BQC Fail Url-test:",topThreeFail)
    const word = '最高级'
    if(topThreeFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${topThreeFail} `)
    }
    //expect(topThreeFail.length).to.equal(0)
})

Then("'顶级'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",topFourPass.length)
    // console.log("BQC Pass URL-test:",topFourPass)
    console.log("Total of BQC Fail Url-test:",topFourFail.length)
    console.log("BQC Fail Url-test:",topFourFail)
    const word = '顶级'
    if(topFourFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${topFourFail} `)
    }
    //expect(topFourFail.length).to.equal(0)
})

Then("'顶尖'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",topFivePass.length)
    // console.log("BQC Pass URL-test:",topFivePass)
    console.log("Total of BQC Fail Url-test:",topFiveFail.length)
    console.log("BQC Fail Url-test:",topFiveFail)
    const word = '顶尖'
    if(topFiveFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${topFiveFail} `)
    }
    //expect(topFiveFail.length).to.equal(0)
})

Then("'最高档'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",topSixPass.length)
    // console.log("BQC Pass URL-test:",topSixPass)
    console.log("Total of BQC Fail Url-test:",topSixFail.length)
    console.log("BQC Fail Url-test:",topSixFail)
    const word = '最高档'
    if(topSixFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${topSixFail} `)
    }
    //expect(topSixFail.length).to.equal(0)
})

Then("'世界领先'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",topSevenPass.length)
    // console.log("BQC Pass URL-test:",topSevenPass)
    console.log("Total of BQC Fail Url-test:",topSevenFail.length)
    console.log("BQC Fail Url-test:",topSevenFail)
    const word = '世界领先'
    if(topSevenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${topSevenFail} `)
    }
    //expect(topSevenFail.length).to.equal(0)
})

Then("'国家示范'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",topEightPass.length)
    // console.log("BQC Pass URL-test:",topEightPass)
    console.log("Total of BQC Fail Url-test:",topEightFail.length)
    console.log("BQC Fail Url-test:",topEightFail)
    const word = '国家示范'
    if(topEightFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${topEightFail} `)
    }
    //expect(topEightFail.length).to.equal(0)
})

Then("'全国领先'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",topNinePass.length)
    // console.log("BQC Pass URL-test:",topNinePass)
    console.log("Total of BQC Fail Url-test:",topNineFail.length)
    console.log("BQC Fail Url-test:",topNineFail)
    const word = '全国领先'
    if(topNineFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${topNineFail} `)
    }
    //expect(topNineFail.length).to.equal(0)
})

Then("'行业顶尖'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",topTenPass.length)
    // console.log("BQC Pass URL-test:",topTenPass)
    console.log("Total of BQC Fail Url-test:",topTenFail.length)
    console.log("BQC Fail Url-test:",topTenFail)
    const word = '行业顶尖'
    if(topTenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${topTenFail} `)
    }
    //expect(topTenFail.length).to.equal(0)
})

Then("'行业领先'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",topElevenPass.length)
    // console.log("BQC Pass URL-test:",topElevenPass)
    console.log("Total of BQC Fail Url-test:",topElevenFail.length)
    console.log("BQC Fail Url-test:",topElevenFail)
    const word = '行业领先'
    if(topElevenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${topElevenFail} `)
    }
    //expect(topElevenFail.length).to.equal(0)
})

Then("'领衔'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",topTwelvePass.length)
    // console.log("BQC Pass URL-test:",topTwelvePass)
    console.log("Total of BQC Fail Url-test:",topTwelveFail.length)
    console.log("BQC Fail Url-test:",topTwelveFail)
    const word = '领衔'
    if(topTwelveFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${topTwelveFail} `)
    }
    //expect(topTwelveFail.length).to.equal(0)
})

Then("'问鼎'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",topThirteenPass.length)
    // console.log("BQC Pass URL-test:",topThirteenPass)
    console.log("Total of BQC Fail Url-test:",topThirteenFail.length)
    console.log("BQC Fail Url-test:",topThirteenFail)
    const word = '问鼎'
    if(topThirteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${topThirteenFail} `)
    }
    //expect(topThirteenFail.length).to.equal(0)
})

Then("'开创之举'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",topFourteenPass.length)
    // console.log("BQC Pass URL-test:",topFourteenPass)
    console.log("Total of BQC Fail Url-test:",topFourteenFail.length)
    console.log("BQC Fail Url-test:",topFourteenFail)
    const word = '开创之举'
    if(topFourteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${topFourteenFail} `)
    }
    //expect(topFourteenFail.length).to.equal(0)
})

Then("'第一'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",firstOnePass.length)
    // console.log("BQC Pass URL-test:",firstOnePass)
    console.log("Total of BQC Fail Url-test:",firstOneFail.length)
    console.log("BQC Fail Url-test:",firstOneFail)
    const word = '第一'
    if(firstOneFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${firstOneFail} `)
    }
    //expect(firstOneFail.length).to.equal(0)
})

Then("'唯一'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",firstTwoPass.length)
    // console.log("BQC Pass URL-test:",firstTwoPass)
    console.log("Total of BQC Fail Url-test:",firstTwoFail.length)
    console.log("BQC Fail Url-test:",firstTwoFail)
    const word = '唯一'
    if(firstTwoFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${firstTwoFail} `)
    }
    //expect(firstTwoFail.length).to.equal(0)
})

Then("'首个'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",firstThreePass.length)
    // console.log("BQC Pass URL-test:",firstThreePass)
    console.log("Total of BQC Fail Url-test:",firstThreeFail.length)
    console.log("BQC Fail Url-test:",firstThreeFail)
    const word = '首个'
    if(firstThreeFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${firstThreeFail} `)
    }
    //expect(firstThreeFail.length).to.equal(0)
})

Then("'推荐首选'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",firstFourPass.length)
    // console.log("BQC Pass URL-test:",firstFourPass)
    console.log("Total of BQC Fail Url-test:",firstFourFail.length)
    console.log("BQC Fail Url-test:",firstFourFail)
    const word = '推荐首选'
    if(firstFourFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${firstFourFail} `)
    }
    //expect(firstFourFail.length).to.equal(0)
})

Then("'首家'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",firstFivePass.length)
    // console.log("BQC Pass URL-test:",firstFivePass)
    console.log("Total of BQC Fail Url-test:",firstFiveFail.length)
    console.log("BQC Fail Url-test:",firstFiveFail)
    const word = '首家'
    if(firstFiveFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${firstFiveFail} `)
    }
    //expect(firstFiveFail.length).to.equal(0)
})

Then("'独家'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",firstSixPass.length)
    // console.log("BQC Pass URL-test:",firstSixPass)
    console.log("Total of BQC Fail Url-test:",firstSixFail.length)
    console.log("BQC Fail Url-test:",firstSixFail)
    const word = '独家'
    if(firstSixFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${firstSixFail} `)
    }
    //expect(firstSixFail.length).to.equal(0)
})

Then("'第一人'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",firstSevenPass.length)
    // console.log("BQC Pass URL-test:",firstSevenPass)
    console.log("Total of BQC Fail Url-test:",firstSevenFail.length)
    console.log("BQC Fail Url-test:",firstSevenFail)
    const word = '第一人'
    if(firstSevenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${firstSevenFail} `)
    }
    //expect(firstSevenFail.length).to.equal(0)
})

Then("'首席'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",firstEightPass.length)
    // console.log("BQC Pass URL-test:",firstEightPass)
    console.log("Total of BQC Fail Url-test:",firstEightFail.length)
    console.log("BQC Fail Url-test:",firstEightFail)
    const word = '首席'
    if(firstEightFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${firstEightFail} `)
    }
    //expect(firstEightFail.length).to.equal(0)
})

Then("'独一无二'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",firstNinePass.length)
    // console.log("BQC Pass URL-test:",firstNinePass)
    console.log("Total of BQC Fail Url-test:",firstNineFail.length)
    console.log("BQC Fail Url-test:",firstNineFail)
    const word = '独一无二'
    if(firstNineFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${firstNineFail} `)
    }
    //expect(firstNineFail.length).to.equal(0)
})

Then("'绝无仅有'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",firstTenPass.length)
    // console.log("BQC Pass URL-test:",firstTenPass)
    console.log("Total of BQC Fail Url-test:",firstTenFail.length)
    console.log("BQC Fail Url-test:",firstTenFail)
    const word = '绝无仅有'
    if(firstTenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${firstTenFail} `)
    }
    //expect(firstTenFail.length).to.equal(0)
})

Then("'前无古人'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",firstElevenPass.length)
    // console.log("BQC Pass URL-test:",firstElevenPass)
    console.log("Total of BQC Fail Url-test:",firstElevenFail.length)
    console.log("BQC Fail Url-test:",firstElevenFail)
    const word = '前无古人'
    if(firstElevenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${firstElevenFail} `)
    }
    //expect(firstElevenFail.length).to.equal(0)
})

Then("'100%'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",firstTwelvePass.length)
    // console.log("BQC Pass URL-test:",firstTwelvePass)
    console.log("Total of BQC Fail Url-test:",firstTwelveFail.length)
    console.log("BQC Fail Url-test:",firstTwelveFail)
    const word = '100%'
    if(firstTwelveFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${firstTwelveFail} `)
    }
    //expect(firstTwelveFail.length).to.equal(0)
})

Then("'百分百'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",firstThirteenPass.length)
    // console.log("BQC Pass URL-test:",firstThirteenPass)
    console.log("Total of BQC Fail Url-test:",firstThirteenFail.length)
    console.log("BQC Fail Url-test:",firstThirteenFail)
    const word = '百分百'
    if(firstThirteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${firstThirteenFail} `)
    }
    //expect(firstThirteenFail.length).to.equal(0)
})

Then("'史上'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",firstFourteenPass.length)
    // console.log("BQC Pass URL-test:",firstFourteenPass)
    console.log("Total of BQC Fail Url-test:",firstFourteenFail.length)
    console.log("BQC Fail Url-test:",firstFourteenFail)
    const word = '史上'
    if(firstFourteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${firstFourteenFail} `)
    }
    //expect(firstFourteenFail.length).to.equal(0)
})

Then("'非常一流'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",firstFifteenPass.length)
    // console.log("BQC Pass URL-test:",firstFifteenPass)
    console.log("Total of BQC Fail Url-test:",firstFifteenFail.length)
    console.log("BQC Fail Url-test:",firstFifteenFail)
    const word = '非常一流'
    if(firstFifteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${firstFifteenFail} `)
    }
    //expect(firstFifteenFail.length).to.equal(0)
})

Then("'金牌'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",goldFirstPass.length)
    // console.log("BQC Pass URL-test:",goldFirstPass)
    console.log("Total of BQC Fail Url-test:",goldFirstFail.length)
    console.log("BQC Fail Url-test:",goldFirstFail)
    const word = '金牌'
    if(goldFirstFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${goldFirstFail} `)
    }
    //expect(goldFirstFail.length).to.equal(0)
})

Then("'名牌'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",goldTwoPass.length)
    // console.log("BQC Pass URL-test:",goldTwoPass)
    console.log("Total of BQC Fail Url-test:",goldTwoFail.length)
    console.log("BQC Fail Url-test:",goldTwoFail)
    const word = '名牌'
    if(goldTwoFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${goldTwoFail} `)
    }
    //expect(goldTwoFail.length).to.equal(0)
})

Then("'王牌'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",goldThreePass.length)
    // console.log("BQC Pass URL-test:",goldThreePass)
    console.log("Total of BQC Fail Url-test:",goldThreeFail.length)
    console.log("BQC Fail Url-test:",goldThreeFail)
    const word = '王牌'
    if(goldThreeFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${goldThreeFail} `)
    }
    //expect(goldThreeFail.length).to.equal(0)
})

Then("'销量冠军'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",goldFourPass.length)
    // console.log("BQC Pass URL-test:",goldFourPass)
    console.log("Total of BQC Fail Url-test:",goldFourFail.length)
    console.log("BQC Fail Url-test:",goldFourFail)
    const word = '销量冠军'
    if(goldFourFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${goldFourFail} `)
    }
    //expect(goldFourFail.length).to.equal(0)
})

// Then("'第一'不可出現在網頁中",{timeout: 24 * 5000},async function(){
//     console.log("Total of BQC Pass URL-test:",goldFivePass.length)
//     // console.log("BQC Pass URL-test:",goldFivePass)
//     console.log("Total of BQC Fail Url-test:",goldFiveFail.length)
//     console.log("BQC Fail Url-test:",goldFiveFail)
//     const word = '第一'
//     if(goldFiveFail.length > 0 ){
//         throw new Error(` ${word}有出現在以下這些URL: ${goldFiveFail} `)
//     }
//     //expect(goldFiveFail.length).to.equal(0)
// })

Then("'NO.1'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",goldSixPass.length)
    // console.log("BQC Pass URL-test:",goldSixPass)
    console.log("Total of BQC Fail Url-test:",goldSixFail.length)
    console.log("BQC Fail Url-test:",goldSixFail)
    const word = 'NO.1'
    if(goldSixFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${goldSixFail} `)
    }
    //expect(goldSixFail.length).to.equal(0)
})

Then("'Top1'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",goldSevenPass.length)
    // console.log("BQC Pass URL-test:",goldSevenPass)
    console.log("Total of BQC Fail Url-test:",goldSevenFail.length)
    console.log("BQC Fail Url-test:",goldSevenFail)
    const word = 'Top1'
    if(goldSevenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${goldSevenFail} `)
    }
    //expect(goldSevenFail.length).to.equal(0)
})

Then("'领袖品牌'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",goldEightPass.length)
    // console.log("BQC Pass URL-test:",goldEightPass)
    console.log("Total of BQC Fail Url-test:",goldEightFail.length)
    console.log("BQC Fail Url-test:",goldEightFail)
    const word = '领袖品牌'
    if(goldEightFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${goldEightFail} `)
    }
    //expect(goldFirstFail.length).to.equal(0)
})

Then("'领导品牌'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",goldNinePass.length)
    // console.log("BQC Pass URL-test:",goldNinePass)
    console.log("Total of BQC Fail Url-test:",goldNineFail.length)
    console.log("BQC Fail Url-test:",goldNineFail)
    const word = '领导品牌'
    if(goldNineFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${goldNineFail} `)
    }
    //expect(goldNineFail.length).to.equal(0)
})

// Then("'顶级'不可出現在網頁中",{timeout: 24 * 5000},async function(){
//     console.log("Total of BQC Pass URL-test:",goldTenPass.length)
//     // console.log("BQC Pass URL-test:",goldTenPass)
//     console.log("Total of BQC Fail Url-test:",goldTenFail.length)
//     console.log("BQC Fail Url-test:",goldTenFail)
//     const word = '顶级'
//     if(goldTenFail.length > 0 ){
//         throw new Error(` ${word}有出現在以下這些URL: ${goldTenFail} `)
//     }
//     //expect(goldTenFail.length).to.equal(0)
// })

// Then("'顶尖'不可出現在網頁中",{timeout: 24 * 5000},async function(){
//     console.log("Total of BQC Pass URL-test:",goldElevenPass.length)
//     // console.log("BQC Pass URL-test:",goldElevenPass)
//     console.log("Total of BQC Fail Url-test:",goldElevenFail.length)
//     console.log("BQC Fail Url-test:",goldElevenFail)
//     const word = '顶尖'
//     if(goldElevenFail.length > 0 ){
//         throw new Error(` ${word}有出現在以下這些URL: ${goldElevenFail} `)
//     }
//     //expect(goldElevenFail.length).to.equal(0)
// })

Then("'泰斗'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",goldTwelvePass.length)
    // console.log("BQC Pass URL-test:",goldTwelvePass)
    console.log("Total of BQC Fail Url-test:",goldTwelveFail.length)
    console.log("BQC Fail Url-test:",goldTwelveFail)
    const word = '泰斗'
    if(goldTwelveFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${goldTwelveFail} `)
    }
    //expect(goldTwelveFail.length).to.equal(0)
})

Then("'金标'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",goldThirteenPass.length)
    // console.log("BQC Pass URL-test:",goldThirteenPass)
    console.log("Total of BQC Fail Url-test:",goldThirteenFail.length)
    console.log("BQC Fail Url-test:",goldThirteenFail)
    const word = '金标'
    if(goldThirteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${goldThirteenFail} `)
    }
    //expect(goldThirteenFail.length).to.equal(0)
})

Then("'驰名尖端'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",goldFourteenPass.length)
    // console.log("BQC Pass URL-test:",goldFourteenPass)
    console.log("Total of BQC Fail Url-test:",goldFourteenFail.length)
    console.log("BQC Fail Url-test:",goldFourteenFail)
    const word = '驰名尖端'
    if(goldFourteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${goldFourteenFail} `)
    }
    //expect(goldFourteenFail.length).to.equal(0)
})

Then("'最'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",mostFirstPass.length)
    // console.log("BQC Pass URL-test:",mostFirstPass)
    console.log("Total of BQC Fail Url-test:",mostFirstFail.length)
    console.log("BQC Fail Url-test:",mostFirstFail)
    const word = '最'
    if(mostFirstFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostFirstFail} `)
    }
    //expect(mostFirstFail.length).to.equal(0)
})

Then("'终极'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",mostTwoPass.length)
    // console.log("BQC Pass URL-test:",mostTwoPass)
    console.log("Total of BQC Fail Url-test:",mostTwoFail.length)
    console.log("BQC Fail Url-test:",mostTwoFail)
    const word = '终极'
    if(mostTwoFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostTwoFail} `)
    }
    //expect(mostTwoFail.length).to.equal(0)
})

Then("'极致'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",mostThreePass.length)
    // console.log("BQC Pass URL-test:",mostThreePass)
    console.log("Total of BQC Fail Url-test:",mostThreeFail.length)
    console.log("BQC Fail Url-test:",mostThreeFail)
    const word = '极致'
    if(mostThreeFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostThreeFail} `)
    }
    //expect(mostThreeFail.length).to.equal(0)
})

Then("'万能'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",mostFourPass.length)
    // console.log("BQC Pass URL-test:",mostFourPass)
    console.log("Total of BQC Fail Url-test:",mostFourFail.length)
    console.log("BQC Fail Url-test:",mostFourFail)
    const word = '万能'
    if(mostFourFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostFourFail} `)
    }
    //expect(mostFourFail.length).to.equal(0)
})

Then("'决对'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",mostFivePass.length)
    // console.log("BQC Pass URL-test:",mostFivePass)
    console.log("Total of BQC Fail Url-test:",mostFiveFail.length)
    console.log("BQC Fail Url-test:",mostFiveFail)
    const word = '决对'
    if(mostFiveFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostFiveFail} `)
    }
    //expect(mostFiveFail.length).to.equal(0)
})

Then("'彻底'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",mostSixPass.length)
    // console.log("BQC Pass URL-test:",mostSixPass)
    console.log("Total of BQC Fail Url-test:",mostSixFail.length)
    console.log("BQC Fail Url-test:",mostSixFail)
    const word = '彻底'
    if(mostSixFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostSixFail} `)
    }
    //expect(mostSixFail.length).to.equal(0)
})

Then("'完全'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",mostSevenPass.length)
    // console.log("BQC Pass URL-test:",mostSevenPass)
    console.log("Total of BQC Fail Url-test:",mostSevenFail.length)
    console.log("BQC Fail Url-test:",mostSevenFail)
    const word = '完全'
    if(mostSevenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostSevenFail} `)
    }
    //expect(mostSevenFail.length).to.equal(0)
})

Then("'极佳'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",mostEightPass.length)
    // console.log("BQC Pass URL-test:",mostEightPass)
    console.log("Total of BQC Fail Url-test:",mostEightFail.length)
    console.log("BQC Fail Url-test:",mostEightFail)
    const word = '极佳'
    if(mostEightFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostEightFail} `)
    }
    //expect(mostEightFail.length).to.equal(0)
})

Then("'全解决'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",mostNinePass.length)
    // console.log("BQC Pass URL-test:",mostNinePass)
    console.log("Total of BQC Fail Url-test:",mostNineFail.length)
    console.log("BQC Fail Url-test:",mostNineFail)
    const word = '全解决'
    if(mostNineFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostNineFail} `)
    }
    //expect(mostNineFail.length).to.equal(0)
})

Then("'全方位'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",mostTenPass.length)
    // console.log("BQC Pass URL-test:",mostTenPass)
    console.log("Total of BQC Fail Url-test:",mostTenFail.length)
    console.log("BQC Fail Url-test:",mostTenFail)
    const word = '全方位'
    if(mostTenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostTenFail} `)
    }
    //expect(mostTenFail.length).to.equal(0)
})

Then("'全面改善'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",mostElevenPass.length)
    // console.log("BQC Pass URL-test:",mostElevenPass)
    console.log("Total of BQC Fail Url-test:",mostElevenFail.length)
    console.log("BQC Fail Url-test:",mostElevenFail)
    const word = '全面改善'
    if(mostElevenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostElevenFail} `)
    }
    //expect(mostElevenFail.length).to.equal(0)
})

Then("'精确'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",mostTwelvePass.length)
    // console.log("BQC Pass URL-test:",mostTwelvePass)
    console.log("Total of BQC Fail Url-test:",mostTwelveFail.length)
    console.log("BQC Fail Url-test:",mostTwelveFail)
    const word = '精确'
    if(mostTwelveFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostTwelveFail} `)
    }
    //expect(mostTwelveFail.length).to.equal(0)
})

Then("'准确'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",mostThirteenPass.length)
    // console.log("BQC Pass URL-test:",mostThirteenPass)
    console.log("Total of BQC Fail Url-test:",mostThirteenFail.length)
    console.log("BQC Fail Url-test:",mostThirteenFail)
    const word = '准确'
    if(mostThirteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostThirteenFail} `)
    }
    //expect(mostThirteenFail.length).to.equal(0)
})

Then("'精准'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",mostFourteenPass.length)
    // console.log("BQC Pass URL-test:",mostFourteenPass)
    console.log("Total of BQC Fail Url-test:",mostFourteenFail.length)
    console.log("BQC Fail Url-test:",mostFourteenFail)
    const word = '精准'
    if(mostFourteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostFourteenFail} `)
    }
    //expect(mostFourteenFail.length).to.equal(0)
})

Then("'优秀'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",mostFifteenPass.length)
    // console.log("BQC Pass URL-test:",mostFifteenPass)
    console.log("Total of BQC Fail Url-test:",mostFifteenFail.length)
    console.log("BQC Fail Url-test:",mostFifteenFail)
    const word = '优秀'
    if(mostFifteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostFifteenFail} `)
    }
    //expect(mostFifteenFail.length).to.equal(0)
})

Then("'完美'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",mostSixteenPass.length)
    // console.log("BQC Pass URL-test:",mostSixteenPass)
    console.log("Total of BQC Fail Url-test:",mostSixteenFail.length)
    console.log("BQC Fail Url-test:",mostSixteenFail)
    const word = '完美'
    if(mostSixteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostSixteenFail} `)
    }
    //expect(mostSixteenFail.length).to.equal(0)
})

Then("'永久'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",mostSeventeenPass.length)
    // console.log("BQC Pass URL-test:",mostSeventeenPass)
    console.log("Total of BQC Fail Url-test:",mostSeventeenFail.length)
    console.log("BQC Fail Url-test:",mostSeventeenFail)
    const word = '永久'
    if(mostSeventeenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${mostSeventeenFail} `)
    }
    //expect(mostSeventeenFail.length).to.equal(0)
})

Then("'老字号'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",authorityOnePass.length)
    // console.log("BQC Pass URL-test:",authorityOnePass)
    console.log("Total of BQC Fail Url-test:",authorityOneFail.length)
    console.log("BQC Fail Url-test:",authorityOneFail)
    const word = '老字号'
    if(authorityOneFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${authorityOneFail} `)
    }
    //expect(authorityOneFail.length).to.equal(0)
})

Then("'中国驰名商标'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",authorityTwoPass.length)
    // console.log("BQC Pass URL-test:",authorityTwoPass)
    console.log("Total of BQC Fail Url-test:",authorityTwoFail.length)
    console.log("BQC Fail Url-test:",authorityTwoFail)
    const word = '中国驰名商标'
    if(authorityTwoFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${authorityTwoFail} `)
    }
    //expect(authorityTwoFail.length).to.equal(0)
})

Then("'特供'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",authorityThreePass.length)
    // console.log("BQC Pass URL-test:",authorityThreePass)
    console.log("Total of BQC Fail Url-test:",authorityThreeFail.length)
    console.log("BQC Fail Url-test:",authorityThreeFail)
    const word = '特供'
    if(authorityThreeFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${authorityThreeFail} `)
    }
    //expect(authorityThreeFail.length).to.equal(0)
})

Then("'专供'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",authorityFourPass.length)
    // console.log("BQC Pass URL-test:",authorityFourPass)
    console.log("Total of BQC Fail Url-test:",authorityFourFail.length)
    console.log("BQC Fail Url-test:",authorityFourFail)
    const word = '专供'
    if(authorityFourFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${authorityFourFail} `)
    }
    //expect(authorityFourFail.length).to.equal(0)
})

Then("'专家推荐'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",authorityFivePass.length)
    // console.log("BQC Pass URL-test:",authorityFivePass)
    console.log("Total of BQC Fail Url-test:",authorityFiveFail.length)
    console.log("BQC Fail Url-test:",authorityFiveFail)
    const word = '专家推荐'
    if(authorityFiveFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${authorityFiveFail} `)
    }
    //expect(authorityFiveFail.length).to.equal(0)
})

Then("'国家免检'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",authoritySixPass.length)
    // console.log("BQC Pass URL-test:",authoritySixPass)
    console.log("Total of BQC Fail Url-test:",authoritySixFail.length)
    console.log("BQC Fail Url-test:",authoritySixFail)
    const word = '国家免检'
    if(authoritySixFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${authoritySixFail} `)
    }
    //expect(authoritySixFail.length).to.equal(0)
})

Then("'质量免检'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",authoritySevenPass.length)
    // console.log("BQC Pass URL-test:",authoritySevenPass)
    console.log("Total of BQC Fail Url-test:",authoritySevenFail.length)
    console.log("BQC Fail Url-test:",authoritySevenFail)
    const word = '质量免检'
    if(authoritySevenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${authoritySevenFail} `)
    }
    //expect(authoritySevenFail.length).to.equal(0)
})

Then("'无需国家质量检测'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",authorityEightPass.length)
    // console.log("BQC Pass URL-test:",authorityEightPass)
    console.log("Total of BQC Fail Url-test:",authorityEightFail.length)
    console.log("BQC Fail Url-test:",authorityEightFail)
    const word = '无需国家质量检测'
    if(authorityEightFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${authorityEightFail} `)
    }
    //expect(authorityEightFail.length).to.equal(0)
})

Then("'免抽检'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",authorityNinePass.length)
    // console.log("BQC Pass URL-test:",authorityNinePass)
    console.log("Total of BQC Fail Url-test:",authorityNineFail.length)
    console.log("BQC Fail Url-test:",authorityNineFail)
    const word = '免抽检'
    if(authorityNineFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${authorityNineFail} `)
    }
    //expect(authorityNineFail.length).to.equal(0)
})

Then("'点击领奖'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",prizeOnePass.length)
    // console.log("BQC Pass URL-test:",prizeOnePass)
    console.log("Total of BQC Fail Url-test:",prizeOneFail.length)
    console.log("BQC Fail Url-test:",prizeOneFail)
    const word = '点击领奖'
    if(prizeOneFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeOneFail} `)
    }
    //expect(prizeOneFail.length).to.equal(0)
})

Then("'恭喜获奖'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",prizeTwoPass.length)
    // console.log("BQC Pass URL-test:",prizeTwoPass)
    console.log("Total of BQC Fail Url-test:",prizeTwoFail.length)
    console.log("BQC Fail Url-test:",prizeTwoFail)
    const word = '恭喜获奖'
    if(prizeTwoFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeTwoFail} `)
    }
    //expect(prizeTwoFail.length).to.equal(0)
})

Then("'全民免单'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",prizeThreePass.length)
    // console.log("BQC Pass URL-test:",prizeThreePass)
    console.log("Total of BQC Fail Url-test:",prizeThreeFail.length)
    console.log("BQC Fail Url-test:",prizeThreeFail)
    const word = '全民免单'
    if(prizeThreeFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeThreeFail} `)
    }
    //expect(prizeThreeFail.length).to.equal(0)
})

Then("'点击有惊喜'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",prizeFourPass.length)
    // console.log("BQC Pass URL-test:",prizeFourPass)
    console.log("Total of BQC Fail Url-test:",prizeFourFail.length)
    console.log("BQC Fail Url-test:",prizeFourFail)
    const word = '点击有惊喜'
    if(prizeFourFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeFourFail} `)
    }
    //expect(prizeFourFail.length).to.equal(0)
})

Then("'点击获取'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",prizeFivePass.length)
    // console.log("BQC Pass URL-test:",prizeFivePass)
    console.log("Total of BQC Fail Url-test:",prizeFiveFail.length)
    console.log("BQC Fail Url-test:",prizeFiveFail)
    const word = '点击获取'
    if(prizeFiveFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeFiveFail} `)
    }
    //expect(prizeFiveFail.length).to.equal(0)
})

Then("'点击转身'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",prizeSixPass.length)
    // console.log("BQC Pass URL-test:",prizeSixPass)
    console.log("Total of BQC Fail Url-test:",prizeSixFail.length)
    console.log("BQC Fail Url-test:",prizeSixFail)
    const word = '点击转身'
    if(prizeSixFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeSixFail} `)
    }
    //expect(prizeSixFail.length).to.equal(0)
})

Then("'点击试穿'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",prizeSevenPass.length)
    // console.log("BQC Pass URL-test:",prizeSevenPass)
    console.log("Total of BQC Fail Url-test:",prizeSevenFail.length)
    console.log("BQC Fail Url-test:",prizeSevenFail)
    const word = '点击试穿'
    if(prizeSevenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeSevenFail} `)
    }
    //expect(prizeSevenFail.length).to.equal(0)
})

Then("'点击翻身'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",prizeEightPass.length)
    // console.log("BQC Pass URL-test:",prizeEightPass)
    console.log("Total of BQC Fail Url-test:",prizeEightFail.length)
    console.log("BQC Fail Url-test:",prizeEightFail)
    const word = '点击翻身'
    if(prizeEightFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeEightFail} `)
    }
    //expect(prizeEightFail.length).to.equal(0)
})

Then("'领奖品'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",prizeNinePass.length)
    // console.log("BQC Pass URL-test:",prizeNinePass)
    console.log("Total of BQC Fail Url-test:",prizeNineFail.length)
    console.log("BQC Fail Url-test:",prizeNineFail)
    const word = '领奖品'
    if(prizeNineFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeNineFail} `)
    }
    //expect(prizeNineFail.length).to.equal(0)
})

Then("'秒杀'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",prizeTenPass.length)
    // console.log("BQC Pass URL-test:",prizeTenPass)
    console.log("Total of BQC Fail Url-test:",prizeTenFail.length)
    console.log("BQC Fail Url-test:",prizeTenFail)
    const word = '秒杀'
    if(prizeTenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeTenFail} `)
    }
    //expect(prizeTenFail.length).to.equal(0)
})

Then("'抢爆'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",prizeElevenPass.length)
    // console.log("BQC Pass URL-test:",prizeElevenPass)
    console.log("Total of BQC Fail Url-test:",prizeElevenFail.length)
    console.log("BQC Fail Url-test:",prizeElevenFail)
    const word = '抢爆'
    if(prizeElevenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeElevenFail} `)
    }
    //expect(prizeElevenFail.length).to.equal(0)
})

Then("'在不抢就没啦'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",prizeTwelvePass.length)
    // console.log("BQC Pass URL-test:",prizeTwelvePass)
    console.log("Total of BQC Fail Url-test:",prizeTwelveFail.length)
    console.log("BQC Fail Url-test:",prizeTwelveFail)
    const word = '在不抢就没啦'
    if(prizeTwelveFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeTwelveFail} `)
    }
    //expect(prizeTwelveFail.length).to.equal(0)
})

Then("'不会再便宜'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",prizeThirteenPass.length)
    // console.log("BQC Pass URL-test:",prizeThirteenPass)
    console.log("Total of BQC Fail Url-test:",prizeThirteenFail.length)
    console.log("BQC Fail Url-test:",prizeThirteenFail)
    const word = '不会再便宜'
    if(prizeThirteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeThirteenFail} `)
    }
    //expect(prizeThirteenFail.length).to.equal(0)
})

Then("'万人疯抢'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",prizeFourteenPass.length)
    // console.log("BQC Pass URL-test:",prizeFourteenPass)
    console.log("Total of BQC Fail Url-test:",prizeFourteenFail.length)
    console.log("BQC Fail Url-test:",prizeFourteenFail)
    const word = '万人疯抢'
    if(prizeFourteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeFourteenFail} `)
    }
    //expect(prizeFourteenFail.length).to.equal(0)
})

Then("'错过就没机会'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",prizeFifteenPass.length)
    // console.log("BQC Pass URL-test:",prizeFifteenPass)
    console.log("Total of BQC Fail Url-test:",prizeFifteenFail.length)
    console.log("BQC Fail Url-test:",prizeFifteenFail)
    const word = '错过就没机会'
    if(prizeFifteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeFifteenFail} `)
    }
    //expect(prizeFifteenFail.length).to.equal(0)
})

Then("'全民疯抢'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",prizeSixteenPass.length)
    // console.log("BQC Pass URL-test:",prizeSixteenPass)
    console.log("Total of BQC Fail Url-test:",prizeSixteenFail.length)
    console.log("BQC Fail Url-test:",prizeSixteenFail)
    const word = '全民疯抢'
    if(prizeSixteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeSixteenFail} `)
    }
    //expect(prizeSixteenFail.length).to.equal(0)
})

Then("'全民抢购'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",prizeSeventeenPass.length)
    // console.log("BQC Pass URL-test:",prizeSeventeenPass)
    console.log("Total of BQC Fail Url-test:",prizeSeventeenFail.length)
    console.log("BQC Fail Url-test:",prizeSeventeenFail)
    const word = '全民抢购'
    if(prizeSeventeenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeSeventeenFail} `)
    }
    //expect(prizeSeventeenFail.length).to.equal(0)
})

Then("'卖疯了'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",prizeEighteenPass.length)
    // console.log("BQC Pass URL-test:",prizeEighteenPass)
    console.log("Total of BQC Fail Url-test:",prizeEighteenFail.length)
    console.log("BQC Fail Url-test:",prizeEighteenFail)
    const word = '卖疯了'
    if(prizeEighteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeEighteenFail} `)
    }
    //expect(prizeEighteenFail.length).to.equal(0)
})

Then("'抢疯了'不可出現在網頁中",{timeout: 24 * 5000},async function(){
    console.log("Total of BQC Pass URL-test:",prizeNineteenPass.length)
    // console.log("BQC Pass URL-test:",prizeNineteenPass)
    console.log("Total of BQC Fail Url-test:",prizeNineteenFail.length)
    console.log("BQC Fail Url-test:",prizeNineteenFail)
    const word = '抢疯了'
    if(prizeNineteenFail.length > 0 ){
        throw new Error(` ${word}有出現在以下這些URL: ${prizeNineteenFail} `)
    }
    //expect(prizeNineteenFail.length).to.equal(0)
})

// //測試用
// Then("'灵感成真'不可出現在網頁中",{timeout: 24 * 5000},async function(){
//     console.log("Total of BQC Pass URL-test(test):",testPass.length)
//     //console.log("BQC Pass URL-test:",testPass)
//     console.log("Total of BQC Fail Url-test(test):",testFail.length)
//     console.log("BQC Fail Url-test(test):",testFail)
//     const word = '灵感成真'
//     if(testFail.length > 0 ){
//         throw new Error(` ${word}有出現在以下這些URL: ${testFail} `)
//     }
//     //expect(testFail.length).to.equal(0)
// })